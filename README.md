# Dark Pattern Learning Game

## Description
A learning game for / against dark patterns written in React.

## Installation

### Docker

1. Clone the repo
2. In the root folder create a folder called `.db` and a text file inside it called `password.txt`. In it store the password secret for your database.
3. The application requires `app.env`, `app.local.env` and `app.prod.env` to function. request them from admin or make your own app.env file. To migrate changes and access the datbase via prisma make sure to create and set the local env variables for the local terminal properly in `app.local.env`.
4. Run the following commands

```bash
npm install
```

5. Run the development docker container. Wait for next to start it might take some time on the first build.

```bash
docker compose up --build 
```

6. Load the local environment variables in the local terminal before using prisma. Migrate the database to latest changes

```bash
export $(grep -v '^#' app.local.env | xargs)
npx prisma migrate deploy
```

7. Go to localhost to open the application.


#### Running docker for dev

For building the docker container in non detached background mode.
Remember that the databases are available and accessible the container name running the database.

```bash
docker compose up --build 
# In detached/background mode
docker compose up -d --build 
```

### Working with prisma in development

To work with prisma studio and create the requisite migrations using prisma in your local terminal.
You need to first load in the environment variables. The database is available on localhost in the local terminal.

```bash
export $(grep -v '^#' app.local.env | xargs)
```

When the variables are set properly you should be able to view the database model in prisma studio without any errors.

```bash
npx prisma studio
```

After making changes to the database model in `prisma/schema.prisma` run the following command to make changes to the database schema and regenerate the prisma client.

```bash
npx prisma migrate dev
```

To unload the environment variables

```bash
unset $(grep -v '^#' app.local.env | sed -E 's/(.*)=.*/\1/' | xargs)
```

#### Running docker for production

For running docker in production with the optimized nextjs production build.
Remember that the databases are available and accessible the container name running the database.

```bash
docker compose -f "compose.prod.yml" up --build
# In detached/background mode 
docker compose -f "compose.prod.yml" up -d --build 

```

### Prisma in production 

Make sure that docker is running and the database server is running in production before running the migrate deploy command.
The production datbase schema will have to be initialized and synced to the latest prisma schema before the application can access it. To sync the database to the latest schema use the following command. Make sure to set the database url properly for the production database. This should ideally be run in CI pipeline or manually done to properly initialize the database.

```bash
npx prisma migrate deploy
```

Refer to the [prisma migration mental model](https://www.prisma.io/docs/orm/prisma-migrate/understanding-prisma-migrate/mental-model) to understand the workflow.

### Manual Install

1. Clone the repo
2. In `next.config.js`, change output to 'export'
3. npm install
4. npm install next@latest react@latest react-dom@latest
5. npm run dev or npm run build


## Content Creation

### Menu
Add new levels by expanding the "contracts" array in the "contracts-data.ts" file. Each "Contract" has "Subcontracts" (i.e., the actual levels) that should ideally feature a common theme. At least that is the idea.

### Level
For a new level, create a folder with a "page.tsx" file. This unlocks the url path paramter for the folder. Here, export a react function that returns some `<GameUI childeren={children} />`. Children is an array of any react components or loaded from HTML.

Levels are displayed with a width of 1600px.

### Dark Patterns

Dark patterns are marked as a HTML class with `darkPatternContainer`. Additionally, provide the category for the dark pattern as class as well (`dpNone`, `dpObstruction`, `dpSneaking`, `dpInterfaceInterference`, `dpForcedAction`, `dpSocialEngineering`). Elements can have multiple categories. In that case, any of them will be accepted as answer.

Elements, that belong together (i.e., parent and child or repeating siblings) can be linked together using the class `dpLinkXYZ` where XYZ is a unique identifier. All elements with this identifier are linked and if one is selected, all of them are selected.

Provide an explanation why something is a dark pattern or not using the `about` attribute. There are presets available in "dark-pattern-data.tsx".

Additionally, to ease analyzing the logs add a `custom-id` string with a (unique and) meaningful name to each `darkPatternContainer`. Logs will display this `custom-id` for selection and category.

Regular scoring is 100 points for finding a dark pattern and 100 points for correctly categorizing it. However, the latter can be overridden by setting `custom-score=` as HTML attribute. Elements with a score of 0 are excluded from the summary but can be found during gameplay (use this e.g., for ambivalent dark pattern elements). Linked elements can have different scores and the one of the selected element will be applied.

When hovering with the magic wand enabled, the background of elements is changed to red to indicate that they can be selected. This extends to children. However, elements can be excluded from highlighting by adding the `noDarkPatternHighlight` class to them.

By adding the `dpNoContainer` class to an element, all dark pattern containers inside this element and its children will be removed. This can for instance be used to render the background for an overlay.


### Pages

Levels support multiple pages (e.g., to support a alert overlay or similar). To display multiple pages, provide an array of children to the game scene instead of a single child.

Pages are locked and have to be unlocked during gameplay by finding the corresponding buttons (or other elements). Elements, that unlock another page have the `page-element` class. They also indicate what page they unlock via `custom-page=`. Note that this unlocks all pages up to the value provided.


### Debug Mode

On the contracts page, press 'd' to enable the debug menu. From there, you can generate user ids, initiate the post-test and export user logs.


## License
MIT
