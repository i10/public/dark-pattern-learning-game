/*
  Warnings:

  - Made the column `message` on table `Log` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `Log` MODIFY `message` VARCHAR(191) NOT NULL;
