-- migrate sql log table to remove unique userId constraint
ALTER TABLE `Log` DROP FOREIGN KEY `Log_userID_fkey`;
ALTER TABLE `Log` DROP INDEX `Log_userID_key`;
ALTER TABLE `Log` ADD INDEX `Log_userID_key` (`userID`);
ALTER TABLE `Log` ADD CONSTRAINT `Log_userID_fkey` FOREIGN KEY (`userID`) REFERENCES `User`(`userID`) ON DELETE RESTRICT ON UPDATE CASCADE;