-- AlterTable
ALTER TABLE `User` DROP COLUMN `timeOnline`,
    ADD COLUMN `occupation` VARCHAR(191) NOT NULL;
