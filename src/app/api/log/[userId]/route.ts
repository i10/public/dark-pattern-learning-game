import prisma from "@/app/prisma";


export async function GET(request: Request, { params: { userId } }: { params: { userId: string } }) {
    // get user based on userId
    const logs = await prisma.log.findMany({
        where: { userID: userId },
    });
    console.log(logs);
    return Response.json(logs);
}