import prisma from "@/app/prisma";
import { Prisma } from "@prisma/client";


type LogCreateBody = Prisma.Args<typeof prisma.log, 'create'>['data'];
// Create a new log
export async function POST(request: Request) {
    const data: LogCreateBody = await request.json();
    // create logs
    const log = await prisma.log.create({ data });
    console.log(log);
    return Response.json(log,{status:200, statusText:"OK"});
}