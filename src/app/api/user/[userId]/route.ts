import prisma from "@/app/prisma";


export async function GET(request: Request, { params: { userId } }: { params: { userId: string } }) {
    // get user based on userId
    
    const user = await prisma.user.findUnique({
        where: { userID: userId },
        include: { Logs: false },
    });
    console.log(user);
    return Response.json(user,{status:200, statusText:"OK"});
}


export async function PATCH(request: Request, { params: { userId } }: { params: { userId: string } }) {
    // update user
    const data = await request.json();
    const user = await prisma.user.update({
        where: { userID: userId },
        data,
    });
    console.log(user);
    return Response.json(user,{status:200, statusText:"OK"});
}
