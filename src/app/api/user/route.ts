import prisma from "@/app/prisma";
import { Prisma } from "@prisma/client";

type UserCreateBody = Prisma.Args<typeof prisma.user, 'create'>['data'];
export async function POST(request: Request) {
    // create user
    const data: UserCreateBody = await request.json();
    const user = await prisma.user.create({ data });
    console.log(user);
    return Response.json(user,{status:200, statusText:"OK"});
}
