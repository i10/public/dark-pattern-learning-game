export type Contract = {
  name: string;
  description: string;
  includeImage: boolean;
  level: number;
  path: string;
  subcontracts: SubContract[];
};

export type SubContract = {
  id: string;
  name: string;
  description: string | null;
  imageUrl: string;
  difficulty: number;
}



export const contracts: Contract[] = [
  {
    name: "Tutorial",
    description: "Get to know dark patterns!\nWhat are they?\nHow can you recognize them?",
    includeImage: false,
    level: 1,
    path: "tutorial",
    subcontracts: [
      {
        id: "19424c90-efea-4ed0-a741-ad833b104d72",
        name: "Getting to know dark patterns",
        description: null,
        imageUrl: "tutorial_1.png",
        difficulty: 0,
      },
      {
        id: "8e1f3930-77b5-41bd-8b31-447748325bb3",
        name: "Quiz: Test your knowlegde on dark patterns",
        description: "No magic wand, just rapid fire questions!",
        imageUrl: "tutorial_1.png",
        difficulty: 1,
      },
    ]
  },
  {
    name: "HoteLand",
    description: "Popular Hotel Booking Site \"HoteLand\" asked us for help to make their website more consumer friendly.",
    includeImage: true,
    level: 2,
    path: "levels/booking-site",
    subcontracts: [
      {
        id: "342743fd-37fe-40a0-acea-d1cba748d792",
        name: "HoteLand Search",
        description: null,
        imageUrl: "hoteland_1.jpg",
        difficulty: 2,
      },
      // {
      //   id: "840d6c07-7d12-4f80-9c0c-e5ebc6b95457",
      //   name: "HoteLand Search Design B",
      //   imageUrl: "hoteland_2.jpg",
      //   difficulty: 3,
      // },
      {
        id: "4abd10de-57f5-49ec-9575-3c873ed429ab",
        name: "HoteLand Booking",
        description: null,
        imageUrl: "hoteland_2.jpg",
        difficulty: 3,
      },
      {
        id: "916c4ab2-de4c-44ca-a68a-be1277e98da6",
        name: "HoteLand Newsletter Unsubscribe",
        description: "Try to unsubscribe from the HoteLand newsletter.",
        imageUrl: "hoteland_3.jpg",
        difficulty: 4,
      },
    ]
  },
  {
    name: "SmartShop24",
    description: "SmartShop24 recently redesigned their order process. They performed A/B testing to measure their revenue and found out that Design B performs better.\nCan you check this from a dark pattern perspective?",
    includeImage: true,
    level: 3,
    path: "levels/smartshop24",
    subcontracts: [
      {
        id: "6224f94c-01eb-4701-aac2-acccf6041415",
        name: "SmartShop24 Order Process Design A",
        description: null,
        imageUrl: "smartshop_1.jpg",
        difficulty: 1,
      },
      {
        id: "205621d3-dad6-434b-aee0-49d310938d5f",
        name: "SmartShop24 Order Process Design B",
        description: null,
        imageUrl: "smartshop_2.jpg",
        difficulty: 3,
      },
    ]
  },
  {
    name: "TicketDreams",
    description: "TicketDreams is the leading ticket shop for events in your area. They want to improve their ticket booking process. Can you help them?",
    includeImage: true,
    level: 4,
    path: "levels/ticketshop",
    subcontracts: [
      {
        id: "83104ca6-45f6-4b36-904c-feec9308ac11",
        name: "Ticket Booking Process",
        description: "Try to purchase one standard ticket for the event.",
        imageUrl: "ticketdreams_1.jpg",
        difficulty: 3,
      },
    ]
  }
]