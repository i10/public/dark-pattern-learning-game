"use client"

import { useState, useEffect, useCallback } from 'react';
import React from 'react';
import './contracts.css';
import { Contract, SubContract, contracts } from './contracts-data';
import { loadBool, storeBool, getHighscore, addLineBreaks, loadNumber, storeNumber, getCareerValue, Warning } from './utils';
import { generateNewUserID, exportCurrentUserData, logMessage, logMessageWithCompletion } from './logger';
import { WarningOverlay } from './game-ui';
import { Prisma } from '@prisma/client';


//===============================================================================================================================
//
// CONTRACTS PAGE (aka Level Selection)
//
//===============================================================================================================================

export default function ContractsPage() {
  const [warning, setWarning] = useState<Warning | null>(null); 
  const [debugModeEnabled, setDebugModeEnabled] = useState(false);


  useEffect(() => {
    if (sessionStorage.getItem('user_id') == null) {
      if (process.env.NODE_ENV == 'production') {
      // go to demographics if no user id is set
      //   window.location.href = "/study/demographics/"
      const newUserID = generateNewUserID();

      const user: Prisma.UserCreateInput = {
        userID: newUserID,
        email: `${newUserID}@example.com`,
        age: "-",
        gender: "-",
        educationBackground: "-",
        occupation: "-",
      };

      // make api call to create new user.
      fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      })
      .then((response) => response.json())
      .then((data) => {
        console.log('Success:', data);
      })
      .catch((error) => {
        console.error('Error:', error);

        // check if user already exists:
        fetch(`/api/user/${newUserID}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        })
        .then((response) => response.json())
        .then((data) => {
          console.log('Success:', data);
        })
      });


      } else {
        generateNewUserID();
      }
    } 
  }, []);


  const handleKeyPress = useCallback((event: KeyboardEvent) => {
    handleKeyboardEvent(event);
  }, []);

  useEffect(() => {
    window.addEventListener('keydown', handleKeyPress);
    return () => { window.removeEventListener('keydown', handleKeyPress); };
  }, [handleKeyPress]);




  function handleKeyboardEvent(e: KeyboardEvent) {
    switch (e.key) {
      case 'd': 
        if (process.env.NODE_ENV == 'development') {
          // allow debug menu in development builds
          setDebugModeEnabled(d => !d); 
          e.preventDefault(); 
        } 
        break;
      default: break;
    }
  }

  const handleUnfinishedStudy = () => {
    setWarning({
      title: "Please complete all levels",
      prompt: "You haven't completed all levels yet. You can continue to part 3 of the study after playing each level.",
      buttons: [
        { 
          title: "Keep playing",
          action: () => setWarning(null)
        }
      ],
      dismissAction: () => setWarning(null)
    })
  }






  return (
    <div className='main-wrapper'>
      <header className='main-header'>
        <div className='main-header-wrapper-2'> 
          <div className='header-logo' style={{cursor: 'inherit !important'}}>
            <h1>D P D F</h1>
            <h3>Dark Pattern Defense Force</h3>
          </div>

          
          {debugModeEnabled && <DebugMenu />}
          <UserAccountView incompleteAction={handleUnfinishedStudy} />
        </div>
      </header>

      <main className='contracts-main'>
        <h1>Available Contracts</h1>
        <div className='contracts-main-divider'></div>
        <ContractsList />

        {loadBool("tutorial_completed") && <CareerLevelView />}
      </main>

      {(warning) &&
      <div className='warning-overlay' onClick={warning.dismissAction}>
        <WarningOverlay warning={warning} />
      </div>
      }
    </div>
  )
}




//===============================================================================================================================
//
// CONTRACTS LIST
//
//===============================================================================================================================

function ContractsList() {
  const contractsList = contracts
    .filter((_, i) => (i == 0) || loadBool("tutorial_completed"))
    .map((contract) => <ContractView contract={contract} key={contract.name} />);

  return (
    <div className='contracts-main-list'>
      {contractsList}
    </div>
  );
}



function ContractView({ contract }: { contract: Contract }) {
  return (
    <div className="contract-item contract-item-active">
       <SubContractsList contract={contract} />
      
      <div className='contract-details'>
        <h1>{contract.name}</h1>
        <h3>{addLineBreaks(contract.description)}</h3>

        {(contract.includeImage) && <ContractImageView contract={contract} />}
      </div>
    </div>
  );
}


function ContractImageView({ contract }: { contract: Contract }) {
  const subContractImages = contract.subcontracts.filter(subcontract => (subcontract.imageUrl != "")).map(subcontract => {
   return './contract_previews/' + subcontract.imageUrl;
  });

  const imageStackView = subContractImages.map((image, index) => {  
    return <img src={image} key={index} className='contract-image' />
  });

  const singleImageView = subContractImages.map((image, index) => {  
    return <img src={image} key={index} className='contract-image-solo' />
  });

  return (
    <div className='contract-images'>
      {(subContractImages.length == 1) ? singleImageView : imageStackView}
    </div>
  );
}





function SubContractsList({ contract }: { contract: Contract }) {
  const subcontractsList = contract.subcontracts.map(subcontract =>
    <SubContractView path={contract.path} subcontract={subcontract} key={subcontract.id} />
  );

  return (
    <div className='sub-contracts-list-wrapper'>
      <h1>Available Tasks <small>({contract.subcontracts.length})</small></h1>
      <div className='sub-contracts-list'>
        {subcontractsList}
      </div>
    </div>
  );
}



function SubContractView({ path, subcontract }: { path: string, subcontract: SubContract }) {
  const difficultyView = [];
  for (let i = 0; i < subcontract.difficulty; i++) {
    const key = 'difficulty-' + i;
    difficultyView.push(<span className='sub-contract-difficulty-view-active' key={key}>⏺&#xFE0E;</span>)
  }
  for (let i = subcontract.difficulty; i < 5; i++) {
    const key = 'difficulty-' + i;
    difficultyView.push(<span key={key}>⏺&#xFE0E;</span>)
  }

  const handleClick = () => {
    logMessageWithCompletion(subcontract.id, "Started Level: " + subcontract.name, () => {
      // this causes issues with non-css-file styles not being applied
      // router.push('/' + path + '/' + subcontract.id);

      // not a very nice solution, but otherwise non-css-file styles are not applied properly.
      // may have something to do with client side rendering or pre-rendering.

      window.location.href = '/' + path + '/' + subcontract.id;
    });
  }
  
  const [highscore, setHighscore] = useState(() => getHighscore(subcontract.id));

  const imageClasses = (loadBool(subcontract.id + "-completed", false)) ? 'sub-contract-image sub-contract-image-completed' : 'sub-contract-image'


  return (
    <div className='sub-contract-item'>

      <h1>{(loadBool(subcontract.id + "-completed", false) ? '✓ ' : '')}{subcontract.name}</h1>

      {(subcontract.description != null) && <small className='sub-contract-description'>{subcontract.description}</small>}

      {(subcontract.imageUrl != "") && <img src={'./contract_previews/' + subcontract.imageUrl} className={imageClasses} />}

      {(subcontract.difficulty > 0) &&
      <table className='sub-contract-info-table'>
        <tbody>
          <tr>
            <td>Difficulty:</td>
            <td className='sub-contract-difficulty-view'>{difficultyView}</td>
          </tr>
          <tr>
            <td>Highscore:</td>
            <td>{highscore}</td>
          </tr>
        </tbody>
      </table>
      }

      <button onClick={() => handleClick()}>{(loadBool(subcontract.id + "-completed", false)) ? 'Play Again' : 'Play Level'}</button>
    </div>
  );
}




//===============================================================================================================================
//
// CAREER LADDER VIEW
//
//===============================================================================================================================

function CareerLevelView() {

  let careerValue = getCareerValue();

  return (
    <div className='contracts-career-container'>
      <h1>Career Level: <span>{careerValue} points</span></h1>
      <div className='contracts-main-divider'></div>

      <div className='contracts-career-ladder'>
        <div>DPDF Trainee</div>
        <div className={(careerValue >= 500) ? '' : 'pending'}>DPDF Assistant<span>Unlocks at 500 points</span></div>
        <div className={(careerValue >= 2500) ? '' : 'pending'}>DPDF Supervisor<span>Unlocks at 2500 points</span></div>
        <div className={(careerValue >= 5000) ? '' : 'pending'}>DPDF Junior Manager<span>Unlocks at 5000 points</span></div>
        <div className={(careerValue >= 6800) ? '' : 'pending'}>DPDF Senior Manager<span>Unlocks at 6800 points</span></div>
        <div className={(careerValue >= 8000) ? '' : 'pending'}>DPDF Director<span>Unlocks at 8000 points</span></div>
      </div>

      <small>You earn points the first time that you complete a contract.</small>
    </div>
  )
}



//===============================================================================================================================
//
// USER ACCOUNT MANAGEMENT
//
//===============================================================================================================================

// currently just used for prolific user feedback, later for user account management
function UserAccountView({ incompleteAction }: { incompleteAction: () => void }) {
  return (
    <div className='header-menu-right'>
      {/* <button onClick={() => { 
        const allLevelsComplete = loadBool("342743fd-37fe-40a0-acea-d1cba748d792-completed", false) &&
                                  loadBool("4abd10de-57f5-49ec-9575-3c873ed429ab-completed", false) &&
                                  loadBool("916c4ab2-de4c-44ca-a68a-be1277e98da6-completed", false) &&
                                  loadBool("6224f94c-01eb-4701-aac2-acccf6041415-completed", false) &&
                                  loadBool("205621d3-dad6-434b-aee0-49d310938d5f-completed", false) &&
                                  loadBool("83104ca6-45f6-4b36-904c-feec9308ac11-completed", false);

        if ((allLevelsComplete) || (process.env.NODE_ENV != 'production')) {
          window.location.href = "/study/posttest-game/"
        } else {
          // alert("You have to complete all levels to complete the study.") 
          incompleteAction();
        }
      }}>Complete Study ⏵</button> */}
    </div>
  )
}




//===============================================================================================================================
//
// DEBUG HELPER VIEW
//
//===============================================================================================================================

function DebugMenu() {
  const [menuItemsVisible, setMenuItemsVisible] = useState(false);

  const handleUserIDClick = () => {
    // clear session
    sessionStorage.clear();

    // fill in the new values
    generateNewUserID();

    // go to demographics
    window.location.href = "/study/demographics/"
  }


  return (
    <>
    <button className='debug-menu' onClick={() => setMenuItemsVisible(b => !b)} >⚙</button>
    <div className='debug-items'>
      <button onClick={handleUserIDClick}>New User ID</button>

      <button onClick={() => { 
        storeBool("tutorial_completed", true); 
        window.location.reload(); 
      }}>Unlock levels</button>
    </div>
    </>
  )
}

