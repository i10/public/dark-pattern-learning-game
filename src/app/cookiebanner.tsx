import './cookiebanner.css'
import * as explanations from './dark-pattern-data';



//===============================================================================================================================
//
// COOKIE BANNER CONTAINER
//
//===============================================================================================================================

export default function CookieBanner({ 
                        seed = 0, 
                        companyName = "Test LLC", 
                        backgroundColor = "rgb(220, 220, 220)", 
                        foregroundColor = "black",
                        buttonPrimaryColor = "#FAFBFC",
                        buttonTextColor = "#696969",
                        buttonAccentColor = "#F00",
                        buttonAccentHighlightColor = "#FFF",
                        buttonSecondaryColor = "#ccc"
                      }) {
  const bannerClassNames = (seed > 0) ? 'fullWidthCookieBanner darkPatternContainer dpInterfaceInterference dpLinkCookieBannerInterfaceInterference topTooltip' : 'fullWidthCookieBanner darkPatternContainer dpNone';              
  const buttonDivClassNames = (seed > 0) ? 'fullWidthCookieBannerButtons darkPatternContainer dpInterfaceInterference dpLinkCookieBannerInterfaceInterference topTooltip' : 'fullWidthCookieBannerButtons darkPatternContainer dpNone';
  const cookieBannerExplanation = (seed > 0) ? explanations.badCookieBannerExplanation : explanations.cleanCookieBannerExplanation;

  return (
    <div className={bannerClassNames} style={{
      backgroundColor: backgroundColor,
      color: foregroundColor
    }} about={cookieBannerExplanation} custom-score='100' custom-id='cookie-banner-container'>
      <div>
        <h3>Cookie Consent</h3>
        <div className='fullWidthCookieBannerBody'>
          <p className='darkPatternContainer dpNone' custom-id='cookie-banner-text'>
            By clicking "Accept All", you agree to the use of analytical cookies (used to gain insight on website usage and to improve our site and services) and tracking cookies (<span className='darkPatternContainer dpNone' custom-id='cookie-banner-partner-text'>both from {companyName} and other trusted partners</span>) that help decide which products to show you on and off our site, measure the audience visiting our websites, and enable you to like or share things directly on social media. You can also customize your preferences <u className='cookie-banner-customize darkPatternContainer dpNone' custom-id='cookie-banner-customize-link'>here</u>.
          </p>

          <div className={buttonDivClassNames} custom-score='200' about={cookieBannerExplanation} custom-id='cookie-banner-buttons'>
            {
              {
                0: <CookieBannerButtons1 primaryColor={buttonPrimaryColor} textColor={buttonTextColor} />,
                1: <CookieBannerButtons2 primaryColor={buttonPrimaryColor} textColor={buttonTextColor} />,
                2: <CookieBannerButtons3 accentColor={buttonAccentColor} accentHighlightColor={buttonAccentHighlightColor} secondaryColor={buttonSecondaryColor} />,
              }[seed%3]
            }
            
          </div>
        </div>
      </div>
    </div>
  )
}




//===============================================================================================================================
//
// COOKIE BANNER BUTTON VARIATIONS
//
//===============================================================================================================================

function CookieBannerButtons1({primaryColor, textColor}: { primaryColor: string, textColor: String }) {
  const buttonStyle = {
    backgroundColor: primaryColor + " !important", 
    color: textColor + " !important"
  };

  return (
    <>
      <button className='cookieButtonFirst' style={buttonStyle}>Accept All Cookies</button>
			<button className='cookieButtonSecond' style={buttonStyle}>Refuse Non-Essential Cookies</button>
    </>
  )
}



function CookieBannerButtons2({primaryColor, textColor}: { primaryColor: string, textColor: String }) {
  const buttonStyle = {
    backgroundColor: primaryColor + " !important", 
    color: textColor + " !important"
  };

  return (
    <>
      <button style={buttonStyle}>Accept All</button>
    </>
  )
}



function CookieBannerButtons3({ accentColor, accentHighlightColor, secondaryColor }: { accentColor: string, accentHighlightColor: string, secondaryColor: string }) {
  const primaryButtonStyle = {
    backgroundColor: accentColor + " !important", 
    color: accentHighlightColor + " !important"
  };

  const secondaryButtonStyle = {
    color: secondaryColor + " !important"
  };

  return (
    <>
      <button className='cookieButtonFirst' style={primaryButtonStyle}>Accept All</button>
			<button className='cookieButtonSecond cookieButtonSecondary' style={secondaryButtonStyle}>Refuse Non-Essential</button>
    </>
  )
}