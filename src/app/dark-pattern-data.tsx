export const obstructionExplanation = "This includes dark patterns that make it unnecessarily difficult to achieve a desired task (e.g., additional steps, preventig price comparisions, ...).";
export const sneakingExplanation = "This includes hiding information, prices or subscriptions or disclosing these information very late.";
export const interfaceInterferenceExplanation = "This includes everything where there is an inbalance in the user interface. It also includes bad preselections, trick questions or complex language.";
export const forcedActionExplanation = "This includes for instance forced registration, disclosing more personal information or pay-to-play.";
export const socialEngineeringExplanation = "This includes the dark patterns that exploit social interactions such as fear of missing out (e.g., with urgency or scarcity) or social proof (e.g., fake reviews or expert testimonials). It also includes confirmshaming.";


export const limitedStockExplanation = "While this can be true, many websites use fake information that exploit the fear of missing out to rush consumers into making faster purchase choices.";
export const disguisedAdExplanation = "This is a disguised ad. The ad is barely distinguishable from the content.";
export const hiddenCostExplanation = "There are hidden costs that are not disclosed from the beginning.";
export const previousPriceExplanation = "This is one of the milder dark patterns. Showing a previous price might just be there to inform users of a promotion or it might be there to nudge them into buying the item before it gets more expensive again.";
export const cleanCookieBannerExplanation = "Cookie banner are often used as an example for dark patterns. However, in this case this is a clean cookie banner that is conforming to the GDPR rules and thus not a dark pattern.";
export const badCookieBannerExplanation = "Cookie banner are often used as an example for dark patterns. In this case, it is true: There is a visual inbalance between UI elements. The preferred action (accepting all cookies) is featured more prominently.";
export const fakeCountdownUrgencyExplanation = "This countdown is just here to create a feeling of urgency and rushing a purchase decision. Often they are fake and reopening the page resets the countdown.";
export const interfaceInbalanceExplanation = "There is an inbalance between the items. The preferred action is featured more prominently.";
export const additionalStepExplanation = "Instead of just performing the action that the user wishes, the website imposes an additional step that the user has to perform to achieve their desired outcome.";
export const confusingLanguageExplanation = "It uses confusing language so that it is not clear for the user what action to perform.";
export const highDemandExplanation = "While this can be true, many websites use fake information that exploit the fear of missing out to rush consumers into making faster purchase choices.";
export const testimonialsExplanation = "Many websites use biased or made-up testimonials that the user interprets as genuine and, as a result of that, makes uninformed decisions.";
export const forcedActionCookieBannerExplanation = "The user is forced to interact with the cookie banner instead of just granting access to the site or defaulting to the privacy preserving choice.";