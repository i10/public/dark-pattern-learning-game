'use client'

import { useState, useEffect, useCallback } from 'react';
import React from 'react';
import './contracts.css';
import './game-ui.css';
import { DarkPatternCategory, ScoreResult, applyScoringHighlight, getScoringResult, getDarkPatternTitle, getDarkPatternTitles, getDPCategoriesFromElement } from './scoring';
import * as explanations from './dark-pattern-data';
import { applyLevelResult, getHighscore, storeBool, BonusPoints, Warning } from './utils';
import { logMessage, logMessageWithCompletion } from './logger';









//===============================================================================================================================
//
// MAIN GAME UI
//
//===============================================================================================================================

export default function GameUI({
  children,
  levelID
}: {
  children: React.ReactNode[],
  levelID: string
}) {
  const [currentPage, setCurrentPage] = useState(0);
  const [unlockedPageCount, setUnlockedPageCount] = useState(1); // this will update ones the player uncovers additional pages
  const [pageUnlockedInfo, setPageUnlockedInfo] = useState<number | null>(null);

  const [isFinished, setIsFinished] = useState(false);
  const [isMagicWandEnabled, setIsMagicWandEnabled] = useState(false);
  const [currentScore, setCurrentScore] = useState(0);

  const [scoreResult, setScoreResult] = useState<ScoreResult | null>(null);
  const [justification, setJustification] = useState<string>('');

  const [finalResult, setFinalResult] = useState<ScoreResult[] | null>(null);
  const [bonusPoints, setBonusPoints] = useState<BonusPoints[]>([]);

  const [warning, setWarning] = useState<Warning | null>(null);

  const [numberOfDarkPatterns, setNumberOfDarkPatterns] = useState(0);


  //============================================================================
  //
  // LAUNCH TASKS
  //
  //============================================================================

  // Code in here is executed ones after the page renders
  useEffect(() => {
    addNavigationClickHandlers();
    removeUnwantedDarkPatternContainers();

    updateDPElementAttributes();

    setNumberOfDarkPatterns(() => getDarkPatternCount());
  }, []);

  
  function removeUnwantedDarkPatternContainers() {

    const noDPContainers = document.getElementsByClassName('dpNoContainer');
    for (let i = 0; i < noDPContainers.length; i++) {
      const noDPContainer = noDPContainers[i];
      
      
      var elementsToRemoveDPFrom = noDPContainer.getElementsByClassName('darkPatternContainer');
      while (elementsToRemoveDPFrom.length > 0) {

        for (let j = 0; j < elementsToRemoveDPFrom.length; j++) {
          const elementToRemoveDPFrom = elementsToRemoveDPFrom[j];
          
          // remove any dark pattern references from the classes
          let classList = elementToRemoveDPFrom.classList;
          for (let k = classList.length - 1; k >= 0; k--) {
            const classToRemove = classList[k];
            if (classToRemove.startsWith('dp')) {
              elementToRemoveDPFrom.classList.remove(classToRemove);
            }
            
          }

          elementToRemoveDPFrom.classList.remove('darkPatternContainer')
        }

        elementsToRemoveDPFrom = noDPContainer.getElementsByClassName('darkPatternContainer');
      }
    }
  }



  function getDarkPatternCount() {
    var dpElements: Element[] = [];

    const darkPatternElements = document.getElementsByClassName('darkPatternContainer');

    for (let i=0; i < darkPatternElements.length; i++) {
      const darkPatternElement = darkPatternElements[i];
      if (!darkPatternElement.classList.contains('dpNone')) {

        // this lists all dp elements

        // don't add to list if:
        if (parseInt(darkPatternElement.attributes.getNamedItem('custom-score')?.value ?? "100") == 0) {
          // score is 0 (ambigious)
          continue;
        
        } else {

          const classes = darkPatternElement.classList;
          var linkClassName: string | null = null
          for (let i = 0; i < classes.length; i++) {
            const className = classes[i];
            if (className.startsWith("dpLink")) {
              linkClassName = className
            }
          }

          if (linkClassName) {
            // dp element is linked to another, don't add if already
            if (dpElements.filter(e => e.classList.contains(linkClassName!)).length > 0) {
              continue;
            } 
          }

          dpElements.push(darkPatternElement);
        }
      }
    }

    return dpElements.length;
  }


  function updateDPElementAttributes() {
    const darkPatternElements = document.getElementsByClassName('darkPatternContainer');

    for (let i=0; i < darkPatternElements.length; i++) {
      const darkPatternElement = darkPatternElements[i];
      const categories = getDPCategoriesFromElement(darkPatternElement);

      darkPatternElement.setAttribute('custom-dp', getDarkPatternTitles(categories));
    }
  }


  // Ask before leaving the page (if not finished)
  useEffect(() => {
    const handleWindowClose = (e: BeforeUnloadEvent) => {
      if (!isFinished) {
        e.preventDefault();
        return (e.returnValue = 'Are you sure you want to leave? Any progress will be lost!');
      }
    };

    // in production, warn before leaving the page
    if (process.env.NODE_ENV == 'production') {
      window.addEventListener('beforeunload', handleWindowClose);
      return () => { window.removeEventListener('beforeunload', handleWindowClose); }; 
    }
  }, [isFinished]);


  // disable context menu to "prevent" cheating
  useEffect(() => {
    const preventContextMenu = (e: Event) => {
      e.preventDefault();
    };

    // in production, disable context menu
    if (process.env.NODE_ENV == 'production') {
      document.addEventListener('contextmenu', preventContextMenu);
      return () => { document.removeEventListener('contextmenu', preventContextMenu); }; 
    }
  }, [isFinished]);




  //============================================================================
  //
  // MAGIC WAND ACTIONS
  //
  //============================================================================

  function handleMagicWandButtonClick() {
    // hide any previous selection
    clearDPSelection();

    // toggle magic wand
    if (isMagicWandEnabled) {
      deactivateDarkPatternMagicWand();
    } else {
      activateDarkPatternMagicWand();
    }
  }

  function activateDarkPatternMagicWand() {
    const dpContainers = document.getElementsByClassName('darkPatternContainer');
    for (let i = 0; i < dpContainers.length; i++) {
      const dpContainer = dpContainers[i];
      dpContainer.classList.add("dpContainerActive");
      dpContainer.addEventListener("click", (e) => handleDarkPatternClick(e, dpContainer));
    }

    document.getElementById('childContainer')?.classList.add('isInDarkPatternSearchMode')

    setIsMagicWandEnabled(true);
  }
  
  function deactivateDarkPatternMagicWand() {
    const dpContainers = document.getElementsByClassName('darkPatternContainer');
  
    for (let i = 0; i < dpContainers.length; i++) {
      const dpContainer = dpContainers[i];
      dpContainer.classList.remove("dpContainerActive");
      dpContainer.removeEventListener("click", (e) => handleDarkPatternClick(e, dpContainer));
    }

    document.getElementById('childContainer')?.classList.remove('isInDarkPatternSearchMode')

    setIsMagicWandEnabled(false);
  }




  //============================================================================
  //
  // DARK PATTERN CONTAINER INTERACTIONS
  //
  //============================================================================

  // this function is added to the click event listener on every dark pattern container
  function handleDarkPatternClick(e: Event, element: Element) {
    if (!element.classList.contains('dpContainerActive') || element.classList.contains('dpContainerSelected')) { return }
  
    // don't send event to parent or toggle button actions
    e.preventDefault();
    e.stopImmediatePropagation();

    logMessage(levelID, "Selected: Element<" + (element.attributes.getNamedItem('custom-id')?.value ?? element.className) + ">");
  
    // clear previous selection and select this one
    clearDPSelection()
    element.classList.add('dpContainerSelected')
  
    // show category buttons
    document.getElementById('gameUICategoriesList')?.classList.add('game-ui-dp-categories-active')
  }
  
  
  function clearDPSelection() {
    const selectedElements = document.getElementsByClassName('dpContainerSelected')
    for (let i = 0; i < selectedElements.length; i++) {
      selectedElements[i].classList.remove('dpContainerSelected')
    }
  }
  
  
  
  
  
  
  //============================================================================
  //
  // DARK PATTERN CATEGORIES BUTTONS
  //
  //============================================================================

  function handleDarkPatternCategorization(category: number) {
    const selectedElements = document.getElementsByClassName('dpContainerSelected');
    if (selectedElements.length <= 0) { return /* should never happen, but still... */ }
    const selectedElement = selectedElements[0];

    const selectedCategory: DarkPatternCategory = category

    handleDarkPatternScoring(selectedElement, selectedCategory)
  }


  function handleDarkPatternScoring(element: Element, selectedCategory: DarkPatternCategory): ScoreResult | null {
    
    // get the scoring result
    const scoreResult = getScoringResult(element, selectedCategory)
    if (!scoreResult) {  console.log(element); return null }

    logMessage(levelID, "Categorized: " + getDarkPatternTitle(selectedCategory) + "; Correct: " + getDarkPatternTitles(scoreResult.categories) + "; Element" + scoreResult.element + "; Score change: " +  JSON.stringify(scoreResult.scoreExplanation).replaceAll(",", ";"));

    // highlight the element accordingly
    applyScoringHighlight(element, scoreResult, setScoreResult);

    // update the score
    setCurrentScore(sc => sc + scoreResult.value)

    // show the explanation overlay
    if (!isFinished) {
      setScoreResult(scoreResult)
    }
  
    // hide the categories button ui again
    document.getElementById('gameUICategoriesList')?.classList.remove('game-ui-dp-categories-active');

    return scoreResult
  }




  function hideExplanationOverlay() {
    if (justification != "") {
      if (scoreResult) {
        logMessage(levelID, "User justification for Element" + scoreResult.element + ": " + justification);
      }
      setJustification('');
    }
    setScoreResult(null);
  }


  function hideFinalResultOverlay(backToMenu: boolean) {
    // update the global highscore for this level
    applyLevelResult(levelID, currentScore);

    logMessageWithCompletion(levelID, "Final score: " + currentScore, () => {
      if (backToMenu) {
        window.location.href = '/';
      } else {
        // hide overlay
        setFinalResult(null);
      }
    });
  }



  
  //============================================================================
  //
  // KEYBOARD INPUT
  //
  //============================================================================


  const handleKeyPress = useCallback((event: KeyboardEvent) => {
    handleKeyboardEvent(event);
  }, [ // register the states that should be available for keyboard input
    isMagicWandEnabled, 
    scoreResult, 
    unlockedPageCount
  ]);

  useEffect(() => {
    window.addEventListener('keydown', handleKeyPress);
    return () => { window.removeEventListener('keydown', handleKeyPress); };
  }, [handleKeyPress]);


  function handleKeyboardEvent(e: KeyboardEvent) {
    if (e.target instanceof Element) { 
      // if currently in a text field: don't capture keyboard
      const element = e.target as Element;
      if (element.tagName.toUpperCase() == "TEXTAREA") { return }
    }

    switch (e.key) {
      case "Escape": handleESCAction(); e.preventDefault(); break;
      case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': handleNumberInput(Number(e.key) - 1); e.preventDefault(); break;
      case 'm': handleMagicWandButtonClick(); e.preventDefault(); break;
      default: break;
    }
  }

  const handleESCAction = () => {
    if ((scoreResult != null) || (finalResult != null)) {
      // dismiss overlay
      setScoreResult(null);
      setFinalResult(null);

    } else if (isMagicWandEnabled) {
      // disable magic wand
      clearDPSelection();
      deactivateDarkPatternMagicWand();
    }
  };

  const handleNumberInput = (n: number) => {
    if (n < unlockedPageCount) {
      setCurrentPage(n);
    }
  };





  //============================================================================
  //
  // FINISH ACTION
  //
  //============================================================================

  function handleFinishAction() {
    if (!isFinished) {
      // check if all pages are unlocked
      if (unlockedPageCount < children.length) {
        setWarning({
          title: "Are you sure?",
          prompt: "You haven't unlocked all available pages yet. You can unlock pages by exploring the website.",
          buttons: [
            { 
              title: "Keep playing",
              action: () => setWarning(null)
            },
            { 
              title: "Finish the level",
              action: () => { setWarning(null); completeLevel() }
            }
          ],
          dismissAction: () => setWarning(null)
        })
      } else {
        completeLevel();
      }
    
    } else {
      // second time, it takes the user back to home
      window.location.href = '/';
    }
  }

  function completeLevel() {
    // first time it highlights the missed dark patterns
    logMessage(levelID, "Completed Level");
    storeBool(levelID + "-completed", true);
    setIsFinished(true);
  }




  useEffect(() => {
    if (!isFinished) { return }

    setUnlockedPageCount(children.length);
    
    var missedElementResults: ScoreResult[] = [];

    var missedDPElements = document.getElementsByClassName('darkPatternContainer');
    while (missedDPElements.length > 0) {
      // const darkPatternElements = document.getElementsByClassName('darkPatternContainer');

      for (let i=0; i < missedDPElements.length; i++) {
        const darkPatternElement = missedDPElements[i];
        
        if (!darkPatternElement.classList.contains('dpNone')) {
          // missed this element
          const missedResult = handleDarkPatternScoring(darkPatternElement, DarkPatternCategory.None);
          if (missedResult) { 
            missedElementResults.push(missedResult);
            logMessage(levelID, "Missed element: Element" + missedResult.element + "; " + getDarkPatternTitles(missedResult.categories));
          }
        }

        darkPatternElement.classList.remove('darkPatternContainer');
      }

      missedDPElements = document.getElementsByClassName('darkPatternContainer');
    }

    // currently no longer used
    const classesWithTooltips = ["dpPartlyCorrect", "dpIncorrect", "dpMissing"]
    var elementsWithErrors: Element[] = [];
    for (let i=0; i<classesWithTooltips.length; i++) {
      const currentClass = classesWithTooltips[i];

      const elementsWithTooltips = document.getElementsByClassName(currentClass);
      for (let j=0; j<elementsWithTooltips.length; j++) {
          // elementsWithTooltips[j].classList.add('tooltip');
          elementsWithTooltips[j].classList.add('dpExplanation');
          elementsWithErrors.push(elementsWithTooltips[j]);
      }
    }


    var bonusPoints: BonusPoints[] = [];
    // Bonus points at the end
    if (missedElementResults.filter(s => Math.abs(s.value) > 0).length <= 0) {
      // reward all dp found with 200 points (filter needed to exclude ambigious dp)
      setCurrentScore(sc => sc + 200);
      bonusPoints.push({ title: 'Found all dark patterns', value: 200 });

      if (elementsWithErrors.filter(e => { return (e.attributes.getNamedItem('custom-score')?.value ?? "100") != "0" }).length <= 0) {
        // if perfect game (i.e., no mistakes)
        setCurrentScore(sc => sc + 300);
        bonusPoints.push({ title: 'Perfect game', value: 300 });
      }
    }

    // show the final results screen
    setBonusPoints(bonusPoints);
    setFinalResult(missedElementResults);

  }, [
    isFinished,
    children.length,
    levelID
  ]);



  //============================================================================
  //
  // MULTI PAGE SUPPORT
  //
  //============================================================================

  function handlePageChange(page: number) {
    setCurrentPage(page);
  }

  useEffect(() => {
    let children = document.getElementById('childContainer')?.children
    if (children) {
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        if (i == currentPage) {
          child.classList.remove('game-ui-inactive-page');
          child.classList.add('game-ui-active-page');
        } else {
          child.classList.remove('game-ui-active-page');
          child.classList.add('game-ui-inactive-page');
        }
      }
    }

    logMessage(levelID, "Changed to page: " + (currentPage + 1));
  }, [currentPage, levelID]);



  function addNavigationClickHandlers() {
    let navigationElements = document.getElementsByClassName('page-element');
    for (let i = 0; i < navigationElements.length; i++) {
      const navigationElement = navigationElements[i];
      const unlocksPage = Number(navigationElement.getAttribute('custom-page')) ?? 0

      navigationElement.addEventListener('click', (e) => {
        unlockPage(e, navigationElement, unlocksPage);
      })
    }
  }


  function unlockPage(event: Event, element: Element, page: number) {
    if (!element.classList.contains('page-element')) { return }

    event.preventDefault();
    event.stopImmediatePropagation();

    element.removeEventListener('click', () => { unlockPage(event, element, page) })
    element.classList.remove('page-element');

    // remove all other navigation elements that unlock the same page
    let navigationElements = document.getElementsByClassName('page-element');
    for (let i = navigationElements.length - 1; i >= 0; i--) {
      const navigationElement = navigationElements[i];
      if ((Number(navigationElement.getAttribute('custom-page')) ?? 0) == page) {
        navigationElement.removeEventListener('click', () => { unlockPage(event, navigationElement, page) })
        navigationElement.classList.remove('page-element');
      }
    }

    setUnlockedPageCount(p => {
      if ((p < page) && (page <= children.length)) {
        logMessage(levelID, "Unlocked page: " + page);
        return page 
      } else {
        return p
      }
    })
    
    setPageUnlockedInfo(page);
  }


  function hidePageUnlockedOverlay(switchPage: boolean) {
    if (!pageUnlockedInfo) { return }
    if (switchPage) {
      setCurrentPage(pageUnlockedInfo - 1);
      
      // clear any selection on previous page and hide the categories button ui
      clearDPSelection();
      document.getElementById('gameUICategoriesList')?.classList.remove('game-ui-dp-categories-active');
      
      setPageUnlockedInfo(null);
    } else {
      // hide overlay
      setPageUnlockedInfo(null);
    }
  }


  useEffect(() => {
    updatePagerColors();
  }, [finalResult, currentPage, unlockedPageCount]);


  function updatePagerColors() {
    if (!isFinished) { return }

    const childViews = document.getElementById('childContainer')?.children;
    if (!childViews) { return }
    for (let i = 0; i < childViews.length; i++) {
      const childView = childViews[i];
      const pagerButton = document.getElementById('pager-button-' + i);
      if (!pagerButton) { continue }

      if (childView.getElementsByClassName('dpMissing').length > 0) {
        pagerButton.classList.add('dpClassifyIncorrect');
      } else {
        pagerButton.classList.add('dpClassifyCorrect');
      }
    } 1
  }




  //============================================================================
  //
  // UI
  //
  //============================================================================

  const includePager = children.length > 1

  return (
    <div className='main-wrapper'>
      <header className='main-header'>
        <div className='main-header-wrapper'> 
          <div className='header-logo' onClick={() => { window.location.href = '/'; }}>
            <h1>D P D F</h1>
            <h3>Dark Pattern Defense Force</h3>
          </div>
        </div>
      </header>

      <main className='game-ui-main'>
      <div className='game-ui-main-wrapper'>

        <div className='game-ui-level-container'>
          <div className='game-ui-container-size-box' id='childContainer'>
            {children}
          </div>
        </div>

        <div className='game-ui-sidebar'>
          
          <TimerView finished={isFinished} />

          <div className='game-ui-score'>
            Score: <div className={(currentScore >= 0) ? 'game-ui-score-positive' : 'game-ui-score-negative'}>{currentScore}</div>
          </div>

          {(!isFinished) &&
          <label className="darkPatternSelectorButton">
            <input type="checkbox" checked={isMagicWandEnabled} onChange={handleMagicWandButtonClick} id='magicWandCheckbox' />
            <span>🪄 Dark Pattern Magic Wand&trade;</span>
          </label>
          }

          {!isFinished && <DetailInfo isMagicWandEnabled={isMagicWandEnabled} />}

          {(isMagicWandEnabled &&
            <div className='game-ui-dp-categories' id='gameUICategoriesList'>
              <h3>Categories of Dark Patterns</h3>

              <button className='darkPatternCategoryButton' custom-category='Obstruction'            onClick={() => handleDarkPatternCategorization(DarkPatternCategory.Obstruction)}>Obstruction<CategoryButtonInfo description={explanations.obstructionExplanation} /></button>
              <button className='darkPatternCategoryButton' custom-category='Sneaking'               onClick={() => handleDarkPatternCategorization(DarkPatternCategory.Sneaking)}>Sneaking<CategoryButtonInfo description={explanations.sneakingExplanation} /></button>
              <button className='darkPatternCategoryButton' custom-category='Interface Interference' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.InterfaceInterference)}>Interface Interference<CategoryButtonInfo description={explanations.interfaceInterferenceExplanation} /></button>
              <button className='darkPatternCategoryButton' custom-category='Forced Action'          onClick={() => handleDarkPatternCategorization(DarkPatternCategory.ForcedAction)}>Forced Action<CategoryButtonInfo description={explanations.forcedActionExplanation} /></button>
              <button className='darkPatternCategoryButton' custom-category='Social Engineering'     onClick={() => handleDarkPatternCategorization(DarkPatternCategory.SocialEngineering)}>Social Engineering<CategoryButtonInfo description={explanations.socialEngineeringExplanation} /></button>
              <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.Unsure)}>I'm not sure</button>
            </div>)}

        </div>

        {includePager && <PageControl pageCount={children.length} unlockCount={unlockedPageCount} currentPage={currentPage} action={handlePageChange} /> }

        {!isMagicWandEnabled && <button className='finishedButton' onClick={() => handleFinishAction()}>{!isFinished ? "Finish" : "Back To Home"}</button>}


      </div>
      </main>
   
      {(scoreResult) &&
      <div className='game-ui-explanation-overlay' onClick={hideExplanationOverlay}>
        <ExplanationOverlay 
          scoreResult={scoreResult} 
          justification={justification} 
          setJustification={setJustification}
          dismissAction={hideExplanationOverlay} 
        />
      </div>
      }


      {(pageUnlockedInfo) &&
      <div className='game-ui-explanation-overlay' onClick={() => hidePageUnlockedOverlay(false)}>
        <PageUnlockedOverlay 
          page={pageUnlockedInfo}
          dismissAction={hidePageUnlockedOverlay} 
        />
      </div>
      }


      {(finalResult) &&
      <div className='game-ui-final-result-overlay' onClick={() => hideFinalResultOverlay(false)}>
        <FinalResultOverlay 
          finalResult={finalResult}
          bonusPoints={bonusPoints}
          finalScore={currentScore}
          highscore={getHighscore(levelID)}
          numberOfDarkPatterns={numberOfDarkPatterns}
          dismissAction={hideFinalResultOverlay} 
        />
      </div>
      }

    {(warning) &&
      <div className='game-ui-explanation-overlay' onClick={hideExplanationOverlay}>
        <WarningOverlay warning={warning} />
      </div>
      }


    </div>
  )
}




//===============================================================================================================================
//
// SIDEBAR HELPER VIEWS
//
//===============================================================================================================================

export function TimerView({ finished }: { finished: boolean }) {
  const timeWithPaddingZero = (time: number) => {
    return (time < 10) ? `0${time}` : `${time}`;
  };
  
  const timeFormatter = (time: number) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${timeWithPaddingZero(seconds)}`;
  };

  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (!finished) {
      timer = setTimeout(() => setSeconds(s => s + 1), 1000);
    }
    
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [seconds, finished]);

  return (
    <div className={(seconds >= 600) ? 'game-ui-timer stress-mode' : 'game-ui-timer'}>
      {timeFormatter(seconds)}
    </div>
  );
}



function DetailInfo({ isMagicWandEnabled }: { isMagicWandEnabled: boolean }) {
  if (isMagicWandEnabled) {
    return (<small>Select deceptive or manipulative elements on the website to the left.</small>)
  } else {
    return (<small>Activate the Dark Pattern Magic Wand&trade; and select deceptive or manipulative elements on the website to the left.</small>)
  }
}



export function CategoryButtonInfo({ description }: { description: string}) {
  return (
    <span 
      className='tooltip rightTooltip' 
      data-text={description} 
      onClick={(e) => e.stopPropagation()} 
      /*onMouseEnter={(e) => logMessage(e.currentTarget.parentElement?.attributes.getNamedItem("custom-category")?.value ?? "", "Help overlay shown")}
      onMouseLeave={(e) => logMessage(e.currentTarget.parentElement?.attributes.getNamedItem("custom-category")?.value ?? "", "Help overlay dismissed")}*/
    >ℹ&#xFE0E;</span>
  )
}




//===============================================================================================================================
//
// MULTIPAGE CONTROL
//
//===============================================================================================================================

export function PageControl({ pageCount, unlockCount, currentPage, action }: { pageCount: number, unlockCount: number, currentPage: number, action: (page: number) => void  }) {
  const buttons = [];
  for (let i = 0; i < pageCount; i++) {
    var classes = 'game-ui-pager-button';
    if (i == currentPage) {
      classes = 'game-ui-pager-button game-ui-pager-button-active'
    } else if (i >= unlockCount) {
      classes = 'game-ui-pager-button game-ui-pager-button-disabled'
    }

    const key = 'pager-button-' + i;
    buttons.push(<button className={classes} key={key} id={key} onClick={() => action(i)} disabled={i >= unlockCount}>{(i >= unlockCount) ? '🔒' : i+1}</button>)
  }

  return (
  <div className='game-ui-pager'>
    {(pageCount > 1) && 
    <div className='game-ui-pager-wrapper'>
      {buttons}
    </div>
    }
  </div>
  );
}




//===============================================================================================================================
//
// OVERLAYS
//
//===============================================================================================================================

export function ExplanationOverlay({ scoreResult, justification, setJustification, dismissAction }: { scoreResult: ScoreResult, justification: string, setJustification: (value: React.SetStateAction<string>) => void, dismissAction: () => void }) {
  const scoreItems = scoreResult.scoreExplanation.map(scoreChange => {
    if (scoreChange.change == 0) {
      return (<tr key={JSON.stringify(scoreChange)}><td>{scoreChange.title}:</td><td className='overlay-score'>± 0</td></tr>)
    } else {
      return (<tr key={JSON.stringify(scoreChange)}><td>{scoreChange.title}:</td><td className={(scoreChange.change > 0) ? 'overlay-score-positive' : 'overlay-score-negative'}>{(scoreChange.change > 0) ? '+' : '-'} {Math.abs(scoreChange.change)}</td></tr>)
    }
  });

  return (
    <div className='game-ui-explanation-overlay-inner' onClick={(e) => e.stopPropagation()}>
      <button className='game-ui-explanation-close' onClick={() => dismissAction()}>⛌</button>
      <h1>{scoreResult.title}</h1>
      
      <div className="game-ui-explanation-overlay-description">{scoreResult.explanation}</div>
      <table className='score-change-table'>
        <thead><tr><th colSpan={2}>Score:</th></tr></thead>
        <tbody>
        {scoreItems}
        </tbody>
      </table>

      {(scoreResult.newClassName != 'dpCorrect') &&
        <div className='game-ui-explanation-overlay-justification'>
          <h3>Explain why you thought otherwise?</h3>
          <textarea name='justification' className='game-ui-explanation-overlay-justification-text' id='game-ui-explanation-overlay-justification-text' placeholder='Optional. Help us understand your thought process.' defaultValue={justification} onChange={e => setJustification(e.target.value) } />
        </div>
      }

      <button className='overlay-button overlay-button-last' onClick={() => dismissAction()}>Continue</button>
    </div>
  );
}



export function FinalResultOverlay({ finalResult, bonusPoints, finalScore, highscore, numberOfDarkPatterns, dismissAction }: { finalResult: ScoreResult[], bonusPoints: BonusPoints[], finalScore: number, highscore: number, numberOfDarkPatterns: number, dismissAction: (toMenu: boolean) => void }) {
  const filteredList = finalResult.filter(s => Math.abs(s.value) > 0) 
  const missedItems = filteredList.map((missedElement, index) =>
    <tr key={index}><td>{getDarkPatternTitles(missedElement.categories)}:</td><td className='overlay-score-negative'>{missedElement.value}</td></tr>
    );

  const bonusPointItems = bonusPoints.map((b, index) =>
    <tr key={index}><td>{b.title}:</td><td className='overlay-score-positive'>{b.value}</td></tr>
    );

  const finalScoreClass = finalScore >= 0 ? 'game-ui-explanation-overlay-finalscore positive' : 'game-ui-explanation-overlay-finalscore negative';


  function captionString() {
    if (filteredList.length <= 0) {
      return "Congratulations!"
    } else if (filteredList.length <= 3) {
      return "That was okay!"
    } else {
      return "There is room for improvement!"
    }
  }


  return (
    <div className='game-ui-explanation-overlay-inner result-overlay-inner' onClick={(e) => e.stopPropagation()}>
      <h1>{captionString()}</h1>
      <div className="game-ui-explanation-overlay-description">{(filteredList.length <= 0) ? "You have found all dark patterns (" + numberOfDarkPatterns + "/" + numberOfDarkPatterns + ") on the website. Good job!" : "You found " + (numberOfDarkPatterns - filteredList.length) + " of " + numberOfDarkPatterns + " dark patterns on this website!"}</div>
      
      <table className='score-change-table'>
        {(filteredList.length > 0) && <>
          <thead><tr><th colSpan={2}>You missed:</th></tr></thead>
          <tbody>
          {missedItems}
          </tbody>
          </>
        }

        {(bonusPoints.length > 0) && <>
          <thead><tr><th colSpan={2}>Bonus points:</th></tr></thead>
          <tbody>
          {bonusPointItems}
          </tbody>
          </>
        }

        <thead><tr><th colSpan={2}>Your final score is:</th></tr></thead>
        <tbody>
          <tr>
            <td colSpan={2} className={finalScoreClass}>{finalScore}</td>
          </tr>
        </tbody>

        <thead><tr><th colSpan={2}>The current high score is:</th></tr></thead>
        <tbody>
          <tr>
            <td colSpan={2} className='game-ui-explanation-overlay-finalscore'>{Math.max(highscore, finalScore)}</td>
          </tr>
        </tbody>
      </table>

      {(filteredList.length > 0) &&
        <>
          <div className="game-ui-explanation-overlay-description" style={{marginTop: 20}}>You can either find out what you did wrong or try again!</div>
          <button className='overlay-button' onClick={() => dismissAction(false)}>Show me my mistakes!</button>
        </>
      }

      <button className='overlay-button overlay-button-last' onClick={(e) => dismissAction(true) }>Back To Home</button>

    </div>
  );
}



export function PageUnlockedOverlay({ page, dismissAction }: { page: number, dismissAction: (switchPage: boolean) => void }) {
  return (
    <div className='game-ui-explanation-overlay-inner game-ui-page-unlocked-overlay' onClick={(e) => e.stopPropagation()}>
      <button className='game-ui-explanation-close' onClick={() => dismissAction(false)}>⛌</button>
      <h1 style={{fontSize: '40px'}}>🔓</h1>
      
      <h3>You have unlocked page {page}.</h3>
      <button className='overlay-button' onClick={() => dismissAction(false)}>Continue on this page</button>
      <button className='overlay-button overlay-button-last' onClick={() => dismissAction(true)}>Go to page {page}</button>
    </div>
  );
}


export function WarningOverlay({ warning }: { warning: Warning }) {
  const buttonViews = warning.buttons.map((b, i) => {
    const classes = (i == warning.buttons.length - 1) ? 'overlay-button overlay-button-last' : 'overlay-button'
    return <button className={classes} onClick={b.action} key={i}>{b.title}</button>
  });

  return (
    <div className='game-ui-explanation-overlay-inner game-ui-page-unlocked-overlay' onClick={(e) => e.stopPropagation()}>
      <button className='game-ui-explanation-close' onClick={() => warning.dismissAction()}>⛌</button>
      <h1 style={{fontSize: '34px'}}>{warning.title}</h1>
      <h3>{warning.prompt}</h3>
      {buttonViews}
    </div>
  );
}



