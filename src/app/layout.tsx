import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { EnvProvider } from '@/env/provider'
import './globals.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Dark Pattern Learning Game',
  description: 'Learn about dark patterns in a playful way.',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <EnvProvider>{children}</EnvProvider>
      </body>
    </html>
  )
}
