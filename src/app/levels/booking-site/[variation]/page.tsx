import BookingSiteListingsGame from '../bookingsite-listings';
import BookingSiteDetailsGame from '../bookingsite-details';
import BookingSiteUnsubscribeGame from '../bookingsite-unsubscribe';

export default function Home({ params }: { params: { variation: string } }) {
  switch (params.variation) {
    case "342743fd-37fe-40a0-acea-d1cba748d792":
      return (<BookingSiteListingsGame variation={0} levelID={params.variation} />)

    // case "840d6c07-7d12-4f80-9c0c-e5ebc6b95457":
    //   return (<BookingSiteListingsGame variation={2} levelID={params.variation} />)

    case "4abd10de-57f5-49ec-9575-3c873ed429ab":
      return (<BookingSiteDetailsGame variation={1} levelID={params.variation} />)

    case "916c4ab2-de4c-44ca-a68a-be1277e98da6":
      return (<BookingSiteUnsubscribeGame variation={0} levelID={params.variation} />)

    default:
      break;
  }
}


export function generateStaticParams() {
  return [
    { variation: '342743fd-37fe-40a0-acea-d1cba748d792' }, 
    { variation: '4abd10de-57f5-49ec-9575-3c873ed429ab' },
    { variation: '916c4ab2-de4c-44ca-a68a-be1277e98da6' }
  ]
}