export type Listing = {
  name: string;
  imageUrl: string;
  rating: string;
  ratingWord: string;
  stars: number;
  price: string;
  previousPrice: string | null;
  reviews: string;
  roomName: string;
  roomFeatures: number;
  includeScarcity: number;
  includeSneaking: boolean;
  disguisedAd: boolean;
};

export const listings: Listing[] = [
  {
    name: 'The Royal Resort',
    imageUrl: 'hotelroom_2.jpg',
    rating: '9.2',
    ratingWord: 'Wonderful',
    stars: 5,
    price: '319',
    previousPrice: null,
    reviews: '578',
    roomName: 'King Room',
    roomFeatures: 3,
    includeScarcity: 1,
    includeSneaking: false,
    disguisedAd: false
  },
  {
    name: 'King\'s Cottage',
    imageUrl: 'hotelroom_1.jpg',
    rating: '8.7',
    ratingWord: 'Very Good',
    stars: 4,
    price: '229',
    previousPrice: null,
    reviews: '1641',
    roomName: 'Twin-Bed Room',
    roomFeatures: 2,
    includeScarcity: 0,
    includeSneaking: false,
    disguisedAd: false
  },
  {
    name: 'TRZ Rooms',
    imageUrl: 'hotelroom_3.jpg',
    rating: '7.0',
    ratingWord: 'Good',
    stars: 2,
    price: '75',
    previousPrice: '120',
    reviews: '2957',
    roomName: 'Double Room',
    roomFeatures: 2,
    includeScarcity: 0,
    includeSneaking: true,
    disguisedAd: false
  },
  {
    name: 'Parkside Hotel',
    imageUrl: 'hotelroom_4.jpg',
    rating: '7.5',
    ratingWord: 'Good',
    stars: 3,
    price: '159',
    previousPrice: null,
    reviews: '873',
    roomName: 'Economy Double Room',
    roomFeatures: 3,
    includeScarcity: 0,
    includeSneaking: false,
    disguisedAd: false
  },
  {
    name: 'Glenos Beach Resort',
    imageUrl: 'hotelroom_6.jpg',
    rating: '9.4',
    ratingWord: 'Wonderful',
    stars: 5,
    price: '299',
    previousPrice: null,
    reviews: '269',
    roomName: 'Comfort Double Room',
    roomFeatures: 1,
    includeScarcity: 0,
    includeSneaking: false,
    disguisedAd: true
  },
  {
    name: 'Bayview Hotel',
    imageUrl: 'hotelroom_5.jpg',
    rating: '8.1',
    ratingWord: 'Very Good',
    stars: 4,
    price: '229',
    previousPrice: '279',
    reviews: '1468',
    roomName: 'Double Room with Balcony',
    roomFeatures: 0,
    includeScarcity: 3,
    includeSneaking: false,
    disguisedAd: false
  },
  {
    name: 'Riverland Motel',
    imageUrl: 'hotelroom_7.jpg',
    rating: '7.3',
    ratingWord: 'Good',
    stars: 3,
    price: '60',
    previousPrice: null,
    reviews: '1156',
    roomName: 'Double Room Basic',
    roomFeatures: 1,
    includeScarcity: 0,
    includeSneaking: true,
    disguisedAd: false
  },
  {
    name: 'Cabin at the Beach',
    imageUrl: 'hotelroom_8.jpg',
    rating: '7.9',
    ratingWord: 'Good',
    stars: 2,
    price: '110',
    previousPrice: null,
    reviews: '2151',
    roomName: 'Standard Room',
    roomFeatures: 0,
    includeScarcity: 0,
    includeSneaking: false,
    disguisedAd: false
  },
]





export type RoomInfo = {
  name: string;
  features: React.JSX.Element;
  details: RoomInfoDetails[];
};

export type RoomInfoDetails = {
  occupancy: string;
  previousPrice: string | null;
  price: string;
  breakfastIncluded: boolean;
  refundable: boolean;
  prepay: boolean;
  includeUrgency: boolean;
  available: number;
  preselection: number;
}


export const roomList: RoomInfo[] = [
  {
    name: "King Room",
    features: <small>
                <span className='booking-detail-room-bed'>1 king bed 🛏️</span>
                <br/>
                Landmark View, 20m<sup>2</sup>,<br/>Flatscreen TV,<br/>Air conditioning,<br/>Coffee machine<br/>Sound proof, Free WiFi
              </small>,
    details: [
      {
        occupancy: "👤👤",
        previousPrice: null,
        price: "€ 319",
        breakfastIncluded: true,
        refundable: false,
        prepay: true,
        includeUrgency: true,
        available: 5,
        preselection: 1
      },
      {
        occupancy: "👤👤",
        previousPrice: null,
        price: "€ 273",
        breakfastIncluded: false,
        refundable: true,
        prepay: true,
        includeUrgency: true,
        available: 5,
        preselection: 0
      }
    ]
  },
  {
    name: "Earl Room",
    features: <small>
                <span className='booking-detail-room-bed'>1 full bed 🛏️</span>
                <br/>
                City View, 10m<sup>2</sup>,<br/>Flatscreen TV,<br/>Air conditioning,<br/>Free WiFi
              </small>,
    details: [
      {
        occupancy: "👤👤",
        previousPrice: "€ 199",
        price: "€ 179",
        breakfastIncluded: true,
        refundable: true,
        prepay: false,
        includeUrgency: false,
        available: 5,
        preselection: 0
      },
      {
        occupancy: "👤👤",
        previousPrice: null,
        price: "€ 159",
        breakfastIncluded: true,
        refundable: false,
        prepay: false,
        includeUrgency: false,
        available: 5,
        preselection: 0
      },
      {
        occupancy: "👤👤",
        previousPrice: null,
        price: "€ 113",
        breakfastIncluded: false,
        refundable: false,
        prepay: false,
        includeUrgency: false,
        available: 5,
        preselection: 0
      }
    ]
  }
]