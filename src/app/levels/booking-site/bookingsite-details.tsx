'use client'

import { useState } from 'react';
import React from 'react';
import './bookingsite.css';
import CookieBanner from '../../cookiebanner';
import GameUI from '../../game-ui';
import * as explanations from '../../dark-pattern-data';
import { roomList } from './bookingsite-data';


const pricePreselectionSneakingDescription = 'The preselection is for the most expensive room. It furthermore sneaks in the optional breakfast at worse conditions.';



//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function BookingSiteDetailsGame({ variation, levelID }: { variation: number, levelID: string }) {
  return (
    <GameUI children={ [
      <BookingSiteDetailsWithOverlay variation={2} key='1' />,
      <BookingSiteDetails variation={1} key='2' />
    ] } levelID={levelID} />
  )
}




//===============================================================================================================================
//
// BOOKING DETAIL VIEW WITH NAGGING OVERLAY
//
//===============================================================================================================================

export function BookingSiteDetailsWithOverlay({ variation }: { variation: number }) {
  return (
    <div>
      {/* Background view without dp annotations */}
      <BookingSiteDetails variation={variation} />

      <div className='booking-detail-save-now-overlay'>
        <div className='booking-detail-save-now-overlay-inner darkPatternContainer dpForcedAction' about='There is no way for the user to permanently disable this alert.' custom-id='booking-details-nagging-overlay'>
          <button className='booking-detail-close-nagging page-element' custom-page='2'>⤬</button>
          <h1>Join now and save big!</h1>
          <p>Join the free HoteLand community by creating an account and save up to 15% on top with every booking.</p>
          <div className='booking-detail-save-now-btns darkPatternContainer dpInterfaceInterference' about={explanations.interfaceInbalanceExplanation} custom-id='booking-details-nagging-overlay-buttons'>
            <button className='booking-detail-save-now-btn-1 page-element' custom-page='2'>Join now!</button>
            <button className='booking-detail-save-now-btn-2 page-element' custom-page='2'>Remind me later</button>
          </div>
        </div>
      </div>
    </div>
  )
}




//===============================================================================================================================
//
// BOOKING DETAIL VIEW
//
//===============================================================================================================================

export function BookingSiteDetails({ variation }: { variation: number }) {
  const [isBookingLoved, setIsBookingLoved] = useState(false);

  // remove dp annotations for variation 2 (used as background "image")
  const mainClasses = (variation == 2) ? 'booking-site-main-wrapper dpNoContainer' : 'booking-site-main-wrapper';

  return (
    <div className={mainClasses}>
      <header className='booking-header booking-header-small'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-details-logo'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-details-signin'>
          <button className='noDarkPatternHighlight'>Register</button>
          <button className='noDarkPatternHighlight'>Sign In</button>
        </div>
      </header>

      <main className='booking-main' id="booking-main">
        <img src='/bookingsite_assets/hotelroom_2.jpg' className='booking-details-main-image' />
        
        <div className='booking-detail-headline'>
          <Menu reviewCount='421' />

          <div className='booking-detail-subheadline'>
            <div className='booking-detail-starrating'>★★★★★</div>
            
            <div className='booking-detail-preferred-partner darkPatternContainer dpSocialEngineering' about='The thumbs up icon suggests that this is a very good property, but only upon reading the tooltip will it become clear that this is in fact a paid endorsement.' custom-id='booking-details-thumbs-up-tooltip'>
                <span className='tooltip rightTooltip' data-text='This property is part of our Preferred Partner Program. It pays us a higher commission if you make a booking.'>
                  <span>👍🏻</span>
                </span>
              </div>
            
            <button className='reserve-button darkPatternContainer dpSneaking dpInterfaceInterference' about={pricePreselectionSneakingDescription} custom-score={0} custom-id='booking-details-top-booking-button-sneaking'>Book room<br/><small>319€</small></button>
            <button className='love-button' onClick={() => setIsBookingLoved(l => !l)}>{isBookingLoved ? '♥' : '♡'}</button>
          </div>

          <div className='booking-detail-hotel-name darkPatternContainer dpNone' custom-id='booking-details-location-info'>
            <h1>The Royal Resort</h1>
            <h4>📍 Birdcage Walk, London, SW1E, England<br/><small>Excellent location, less than 100m to city center</small></h4>
          </div>

          <div className='booking-detail-more-images darkPatternContainer dpNone' custom-id='booking-details-image-gallery'>
            <div>
              <img src='/bookingsite_assets/hotelroom_1.jpg' />
              <img src='/bookingsite_assets/hotelroom_6.jpg' />
              <img src='/bookingsite_assets/hotelroom_4.jpg' className='with-overlay' data-text='+ 17 photos' />
            </div>
          </div>
        </div>

        <div className='booking-detail-clear'></div>

        <div className='booking-detail-highlights darkPatternContainer dpNone' custom-id='booking-details-hotel-feature-list'>
          <div>☕️ Breakfast</div>
          <div>🅿️ Private Parking</div>
          <div>🛜 Free WiFi</div>
          <div>🏓 Fitness Center</div>
          <div>🚭 Non-smoking Rooms</div>
        </div>

        
        <div className='booking-detail-property-highlights darkPatternContainer dpNone' custom-id='booking-details-hotel-highlights-sidenote'>
          <h2>Property Highlights</h2>
          <h3>Perfect Location</h3>
          <p>Located in the center of London with a beautiful landmark view. Rated with an excellent <b>9.7</b> location score by our guests.</p>

          <h3>Perfect for a weekend stay</h3>
          <p>Couples in particular enjoy staying here for a romantic getaway.</p>

        </div>

        <div className='booking-detail-property-description darkPatternContainer dpNone' custom-id='booking-details-hotel-description'>
          <p>"The Royal Resort" is a luxury 5-star hotel located in the heart of London, surrounded by parks and in the vicinity of a lively theatre scene. It is in walking distance of many attractions and easily accessible by public transport.</p>
          <p>The King Rooms feature elegant, contemporary decor, decadent Italian marble bathrooms with complimentary toiletries and luxury king-sized beds with Egyptian cotton sheets.</p>
          <p>The more affordable Earl Rooms offer a more modern style while still providing all the amenties our guests would expect.</p>

        </div>

        <br className='clearfix'/>

        <div className='booking-detail-price-table'>
          <h2>Availability</h2>

          <DateRoomSelection />

          <div className='booking-detail-price-table-inner darkPatternContainer dpSneaking dpInterfaceInterference dpLinkPriceTablePreselection' about={pricePreselectionSneakingDescription} custom-id='booking-details-table'>
            <PriceTable />
          </div>
        </div>



        <div className='booking-detail-urgency-info darkPatternContainer dpSocialEngineering' about={explanations.limitedStockExplanation} custom-id='booking-details-x-hotels-unavailable-bottom-scarcity'>
          ⏰ <b>Limited supply for your dates:</b> 18 similar hotels are already unavailable on our site
        </div>
        
      </main>

      <CookieBanner 
          seed={2}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}





//===============================================================================================================================
//
// HELPER VIEWS
//
//===============================================================================================================================

function Menu({ reviewCount }: { reviewCount: string }) {
  const menuItemData = ['Overview', 'Info & prices', 'Facilites', 'The fine print', 'Reviews (' + reviewCount + ')']
  const menuItems = menuItemData.map(title =>
      <button onClick={() => { document.getElementById("price-table")?.scrollIntoView(true); }} key={title}>{title}</button>
    )

    return (
      <div className='booking-details-menu darkPatternContainer dpNone' custom-id='booking-details-menu'>
        {menuItems}
      </div>
    );
}



function DateRoomSelection({ dates = "Fr, Jul 5 - So, Jul 7", 
                             rooms = "2 adults, 0 children, 1 room" }) {
  return (
    <div className='booking-searchbar booking-detail-searchbar darkPatternContainer dpNone' custom-id='booking-details-search-bar'>
      <div>
        <span>📅 {dates}</span>
        <span>⛌</span>
      </div>

      <div>
        <span>🙍🏼‍♂️ {rooms}</span>
        <span>⛌</span>
      </div>

      <button>Search</button>
    </div>
  );
}



function PriceTable() {
  const rows = roomList.map((roomInfo, i) => {
    return roomInfo.details.map((detail, j) => {
      const choiceClasses = (detail.includeUrgency) ? 'booking-detail-room-choices darkPatternContainer dpSocialEngineering dpLinkBookingChoicesUrgency' : 'booking-detail-room-choices darkPatternContainer dpNone'

      return (
        <tr key={"pricerow" + i + "-" + j}>
          {(j == 0) && <td rowSpan={roomInfo.details.length} className='darkPatternContainer dpNone' custom-id='booking-details-table-room-features'><b>{roomInfo.name}</b><br />{roomInfo.features}</td>}

          <td valign='top' className='darkPatternContainer dpNone' custom-id='booking-details-table-occupancy'><small className='booking-detail-silhoutte noDarkPatternHighlight'>{detail.occupancy}</small></td>
          <td valign='top' className='darkPatternContainer dpNone' custom-id='booking-details-table-fees-included'><b>{detail.price}</b><br /><small>Includes taxes and fees</small></td>
          
          <td valign='top' className={choiceClasses} about={detail.includeUrgency ? explanations.limitedStockExplanation : ''} custom-id={detail.includeUrgency ? 'booking-details-table-x-rooms-left-scarcity-container' : 'booking-details-table-feature-list-clean'}>
            {(detail.breakfastIncluded) ?
                 (<span className='bold green darkPatternContainer dpNone' custom-id='booking-details-table-breakfast-included'>☕️ Breakfast included in the price</span>) :
                 (<span className='bold darkPatternContainer dpNone' custom-id='booking-details-table-breakfast-extra'>Breakfast for just € 23 per night</span>)
            }

            {(detail.refundable) ?
                 (<span className='bold green darkPatternContainer dpNone' custom-id='booking-details-table-refund-policy-0'>✓ Free cancellation</span>) :
                 (<span className='bold darkPatternContainer dpNone' custom-id='booking-details-table-refund-policy-1'>Non-refundable</span>)
            }

            {(detail.prepay) ?
                 (<span className='bold green darkPatternContainer dpNone' custom-id='booking-details-table-payment-style-0'>✓ No pre-payment needed</span>) :
                 (<span className='darkPatternContainer dpNone' custom-id='booking-details-table-payment-style-1'>Pay in advance</span>)
            }

            {(detail.includeUrgency) && <span className='booking-detail-room-choices-urgency darkPatternContainer dpSocialEngineering dpLinkBookingChoicesUrgency bold red' about={explanations.limitedStockExplanation} custom-id='booking-details-table-x-rooms-left-scarcity'>Only {detail.available} left on our site</span>}
            
          </td>
          
          <td className={(detail.preselection > 0) ? 'darkPatternContainer dpInterfaceInterference dpSneaking dpLinkPriceTablePreselection' : 'darkPatternContainer dpNone'} about={pricePreselectionSneakingDescription} custom-score={200} custom-id={detail.preselection > 0 ? 'booking-details-table-room-preselection-sneaking' : 'booking-details-table-room-preselection-none'}><RoomCountPicker count={(detail.includeUrgency) ? detail.available : 10} selected={detail.preselection} /></td>

          {((i == 0) && (j == 0)) && <td rowSpan={roomList.length * 10} className='booking-detail-table-booking-button darkPatternContainer dpNone' valign='top' custom-id='booking-details-table-confirm-button'><button className='noDarkPatternHighlight'>Book Room</button><br/><small>Confirmation is immediate</small></td>}
        </tr>
      )
    })
  })

  return (
    <table id="price-table" className='noDarkPatternHighlight'>
      <thead>
        <tr>
          <th className='noDarkPatternHighlight'>Room Type</th>
          <th className='noDarkPatternHighlight booking-detail-price-table-guests'>Number of guests</th>
          <th className='noDarkPatternHighlight'>Today's price</th>
          <th className='noDarkPatternHighlight'>Your Choices</th>
          <th className='noDarkPatternHighlight booking-detail-price-table-rooms'>Select Rooms</th>
          <th className='noDarkPatternHighlight'></th>
        </tr>
      </thead>

      <tbody>
        {rows}
      </tbody>
    </table>
  );
}



function RoomCountPicker({ count = 5, selected = 0 }) {
  const entries = [];
  for (let i = 0; i < count + 1; i++) {
    entries.push(<option value={i} key={i}>{i}</option>)
  }

  return (
    <select className='booking-detail-room-count-select' name="booking-detail-room-count" defaultValue={selected}>
      {entries}
    </select>
  );
}