'use client'

import './bookingsite.css';
import CookieBanner from '../../cookiebanner';
import { Listing, listings } from './bookingsite-data';
import GameUI from '../../game-ui';
import * as explanations from '../../dark-pattern-data';



//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function BookingSiteListingsGame({ variation, levelID }: { variation: number, levelID: string }) {
  return (
    <GameUI children={ [<BookingSiteListings variation={variation} key='1' />] } levelID={levelID} />
  )
}




//===============================================================================================================================
//
// BOOKING LISTINGS SEARCH VIEW
//
//===============================================================================================================================

export function BookingSiteListings({ variation }: { variation: number }) {
  return (
    <div className='booking-site-main-wrapper'>
      <header className='booking-header'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-listing-header-logo'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-listing-header-signin'>
          <button className='noDarkPatternHighlight'>Register</button>
          <button className='noDarkPatternHighlight'>Sign In</button>
        </div>


        <div className='booking-searchbar darkPatternContainer dpNone' custom-id='booking-listing-header-searchbar'>
          <SearchBar location='London' 
                     dates='We, Jul 10 - Mo, Jul 22' 
                     rooms='2 adults, 0 children, 1 room' />
        </div>
      </header>

      <main className='booking-main'>

        <Sidebar />

        <Listings scarcityPercentage={96} />
        
      </main>

      <CookieBanner 
          seed={variation}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}



//===============================================================================================================================
//
// SIDEBAR (FILTER CHECKBOXES)
//
//===============================================================================================================================

function Sidebar() {
  return (
    <div className='booking-sidebar'>

      <div className='darkPatternContainer dpNone' custom-id='booking-listing-sidebar-budget'>
        <h3>Your Budget (per night)</h3>

        <p>€ 10 – € 500+</p>

        <span className='booking-fakeslider'>
          <span className='booking-fakeslider-line'></span>
          <span className='booking-fakeslider-thumb1'></span>
          <span className='booking-fakeslider-thumb2'></span>
        </span>
      </div>

      <div>

        <FilterSection name='Popular Filters' 
                        filters={[
                        { title: 'Breakfast included', isChecked: true },
                        { title: 'Parking', isChecked: false },
                        { title: 'Spa', isChecked: false },
                        { title: 'Free WiFi', isChecked: false },
                        { title: 'Pet Friendly', isChecked: false },
                      ]} />

        <span className="booking-sidebar-divider" />

        <FilterSection name='Rating' 
                        filters={[
                        { title: 'Wonderful: (9+)', isChecked: false },
                        { title: 'Very Good: (8+)', isChecked: false },
                        { title: 'Good: (7+)', isChecked: false },
                        { title: 'Pleasant: (6+)', isChecked: false },
                      ]} />

        <span className="booking-sidebar-divider" />

        <FilterSection name='Meals' 
                        filters={[
                        { title: 'All-inclusive', isChecked: false },
                        { title: 'Breakfast & dinner included', isChecked: false },
                        { title: 'Breakfast included', isChecked: false },
                      ]} />

        <span className="booking-sidebar-divider" />

        <FilterSection name='Facilities' 
                        filters={[
                        { title: 'Parking', isChecked: false },
                        { title: 'Free WiFi', isChecked: false },
                        { title: 'Restaurant', isChecked: false },
                        { title: 'Pet friendly', isChecked: false },
                        { title: 'Room service', isChecked: false },
                        { title: '24-hour front desk', isChecked: false },
                        { title: 'Fitness center', isChecked: false },
                        { title: 'Non-smoking rooms', isChecked: false },
                        { title: 'Airport shuttle', isChecked: false },
                        { title: 'Family rooms', isChecked: false },
                        { title: 'Spa', isChecked: false },
                        { title: 'Electric vehicle charging station', isChecked: false },
                        { title: 'Swimming pool', isChecked: false },
                      ]} />

        <span className="booking-sidebar-divider" />

        <FilterSection name='Cancellation Policy'
                        filters={[
                          { title: 'Free cancellation', isChecked: false },
                          { title: 'Book without credit card', isChecked: false },
                        ]} />

        <span className="booking-sidebar-divider" />

        <FilterSection name='Travel Sustainable'
                        filters={[
                          { title: 'Level 3+', isChecked: false },
                          { title: 'Level 3 and higher', isChecked: false },
                          { title: 'Level 2 and higher', isChecked: false },
                          { title: 'Level 1 and higher', isChecked: false },
                        ]} />
      </div>

    </div>
  );
}



function FilterSection({ name = "Filter", filters = [{ title: "Parking", isChecked: false }] }) {
  const listItems = filters.map(filter =>
    <label className="booking-checkbox" key={filter.title}>
      {filter.title}
      <input type="checkbox" defaultChecked={filter.isChecked} />
      <span className="booking-checkmark"></span>
    </label>
  );

  return (
    <div className='darkPatternContainer dpNone' custom-id='booking-listing-sidebar-filter-group'>
      <h3>{name}</h3>
      {listItems}
    </div>
  );
}


//===============================================================================================================================
//
// MAIN VIEW (LISTINGS)
//
//===============================================================================================================================


function Listings({ scarcityPercentage = 96 }: { scarcityPercentage: number }) {
  const listItems = listings.map(listing =>
    <ListingView listing={listing} key={listing.name} />
  );

  return (
    <div className='booking-listings '>
      <div className='booking-listing-sort darkPatternContainer dpSocialEngineering' custom-score='0' about='This is a little bit more ambigious. There is certainly some social engineering, suggesting that the first couple of hotels are preferable by some undisclosed measure.' custom-id='booking-listing-our-top-picks-selector'>
        ⮃ Sort by: Our Top Picks ⏷
      </div>

      <div className='booking-listing-notavailable darkPatternContainer dpSocialEngineering' about={explanations.limitedStockExplanation} custom-id='booking-listing-places-unavailable-scarcity-info'>
        {scarcityPercentage}% of places to stay are unavailable for your dates.
        <button>⛌</button>
      </div>

      {listItems}
    </div>
  );
}



function ListingView({ listing }: {listing: Listing}) {
  const stars = [];
  for (let i = 0; i < listing.stars; i++) {
    stars.push(<span key={i}>★</span>)
  }

  const listingDP = listing.disguisedAd ? 'booking-listing darkPatternContainer dpSneaking dpLinkListingDisguisedAd' : 'booking-listing darkPatternContainer dpNone';
  const listingCustomID = listing.disguisedAd ? 'booking-listing-container-disguised-ad' : 'booking-listing-container'
  
  return (
    <div className={listingDP} about={listing.disguisedAd ? explanations.disguisedAdExplanation : ""} custom-score={listing.disguisedAd ? '200' : '100'} custom-id={listingCustomID}>
      <div className='booking-listing-image-container'>
        <img src={'/bookingsite_assets/' + listing.imageUrl} className='booking-listing-image' />
        <button className='noDarkPatternHighlight'>♡</button>
      </div>
      
      <div className='booking-listing-main'>
        <div className='booking-listing-header'>
          <span>
            <h1>{listing.name}</h1> 
            {listing.disguisedAd && <span className='booking-listing-header-ad darkPatternContainer dpSneaking dpLinkListingDisguisedAd' about={explanations.disguisedAdExplanation} custom-score='200' custom-id='booking-listing-disguised-ad-label'>Sponsored</span>}
          </span>
          <div className='booking-listing-stars darkPatternContainer dpNone' custom-id='booking-listing-star-rating'>{stars}</div>
        
          <div className='booking-listing-rating-container darkPatternContainer dpNone' custom-id='booking-listing-user-rating-container'>
            <div className='booking-listing-rating darkPatternContainer dpNone' custom-id='booking-listing-user-rating-count'>
              <span>{listing.ratingWord}</span>
              <span>{listing.reviews} reviews</span>
            </div>

            <div className='booking-listing-rating-box darkPatternContainer dpNone' custom-id='booking-listing-user-rating-number'>
              <span>{listing.rating}</span>
            </div>
          </div>
        </div>

        <div className='booking-listing-detail'>
          <ListingDetail roomName={listing.roomName} features={listing.roomFeatures} includeScarcity={listing.includeScarcity} />

          <div style={{flexGrow: '5'}}></div>
          
          <PriceDetail price={listing.price} previousPrice={listing.previousPrice} includeSneaking={listing.includeSneaking} />
          
        </div>
      </div>
    </div>
  );
}



function ListingDetail({ roomName = "King Room", features = 3, includeScarcity = 0 }) {
  const possibleFeatures = [];
  switch (features) {
    case 1:
      possibleFeatures.push(<span className='booking-listing-detail-feature' key='canc'>✓ Free cancellation</span>)
      break
    case 2:
      possibleFeatures.push(<span className='booking-listing-detail-feature' key='prep'>✓ No prepayment needed</span>)
      break;
    case 3:
      possibleFeatures.push(<span className='booking-listing-detail-feature' key='canc'>✓ Free cancellation</span>)
      possibleFeatures.push(<span className='booking-listing-detail-feature' key='prep'>✓ No prepayment needed</span>)
      break
    default:
      break;
  }

  return (
    <div className='booking-listing-detail-features darkPatternContainer dpNone' custom-id='booking-listing-room-features-container'>
      <span className='booking-listing-detail-roominfo'>{roomName}</span>

      <span className='booking-listing-detail-feature'>✓ Breakfast included</span>
      {possibleFeatures}
      
      {(includeScarcity > 0) && <span className='booking-listing-detail-scarcity darkPatternContainer dpSocialEngineering dpLinkListingScarcity' about={explanations.limitedStockExplanation} custom-id='booking-listing-x-rooms-left-scarcity'>Only {includeScarcity} room left at this price on our site</span>      }
    </div>
  );
}



function PriceDetail({ price = '42', previousPrice, includeSneaking = false }: { price: String, previousPrice: String | null, includeSneaking: boolean }) {
  return (
    <div className='booking-listing-detail-price darkPatternContainer dpNone' custom-id='booking-listing-price-container'>
      <span className='booking-listing-detail-price-detail darkPatternContainer dpNone' custom-id='booking-listing-price-info-per-night'>per night, 2 adults</span>
      
      {previousPrice != null ? (
        <span className='booking-listing-detail-price-value darkPatternContainer dpSocialEngineering dpLinkListingPreviousPrice' about={explanations.previousPriceExplanation} custom-id='booking-listing-previous-price'><span className='booking-listing-detail-price-previous' custom-id='booking-listing-previous-price'>€ {previousPrice}</span>&nbsp;&nbsp;€ {price}</span>
      ) : (
        <span className='booking-listing-detail-price-value darkPatternContainer dpNone' custom-id='booking-listing-price'>€ {price}</span>
      )
      }
      
      {includeSneaking ? (
        <span className='booking-listing-detail-price-detail darkPatternContainer dpSneaking dpLinkListingSneakingCharges' about={explanations.hiddenCostExplanation} custom-id='booking-listing-additional-charges-sneaking'>Additional charges apply</span>
      ) : (
        <span className='booking-listing-detail-price-detail darkPatternContainer dpNone' custom-id='booking-listing-includes-fees'>Includes taxes and fees</span>
      )
      }

      <button className='darkPatternContainer dpNone' custom-id='booking-listing-availability-button'>See availability&nbsp;&nbsp;⟩</button>
    </div>
  );
}



//===============================================================================================================================
//
// HELPER VIEWS
//
//===============================================================================================================================

function SearchBar({ location = "New York City", 
                     dates = "We, Jul 10 - Mo, Jul 22", 
                     rooms = "2 adults, 0 children, 1 room" }) {
  return (
    <>
      <div>
        <span>🛏️ {location}</span>
        <span>⛌</span>
      </div>

      <div>
        <span>📅 {dates}</span>
        <span>⛌</span>
      </div>

      <div>
        <span>🙍🏼‍♂️ {rooms}</span>
        <span>⛌</span>
      </div>

      <button>Search</button>
    </>
  );
}