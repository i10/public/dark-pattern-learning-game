'use client'

import { useState } from 'react';
import React from 'react';
import './bookingsite.css';
import '../../game-ui.css';
import CookieBanner from '../../cookiebanner';
import GameUI from '../../game-ui';
import * as explanations from '../../dark-pattern-data';



//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function BookingSiteUnsubscribeGame({ variation, levelID }: { variation: number, levelID: string }) {
  return (
    <GameUI children={ [
      <BookingSiteUnsubscribePage1 variation={variation} key='1' />,
      <BookingSiteUnsubscribePage2 variation={variation} key='2' />,
      <BookingSiteUnsubscribePage3 variation={variation} key='3' />,
      <BookingSiteUnsubscribePage4 variation={variation} key='4' />
    ] } levelID={levelID} />
  )
}




//===============================================================================================================================
//
// BOOKING NEWSLETTER UNSUBSCRIBE PAGE 1
//
//===============================================================================================================================

export function BookingSiteUnsubscribePage1({ variation }: { variation: number }) {
  return (
    <div className='booking-site-main-wrapper'>
      <header className='booking-header booking-header-small'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-unsubscribe-logo'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-unsubscribe-signout'>
          <button className='noDarkPatternHighlight'><span className='booking-unsubscribe-account-image'>👤</span> My Account</button>
        </div>
      </header>

      <main className='booking-main'>
        <div className='booking-unsubscribe-wrapper'>
          <h3>Unsubscribe from our newsletter.</h3>

          <div className='booking-unsubscribe-form darkPatternContainer dpObstruction' about={explanations.additionalStepExplanation} custom-id='booking-unsubscribe-page1-obstruction'>
            <p>Do you instead want to update the frequency that you hear from us?</p>

            <form>
              <label>
                <input type='radio' name='unsubscribe-frequency' defaultChecked />
                &nbsp;Every other day
              </label><br />
              <label>
                <input type='radio' name='unsubscribe-frequency' />
                &nbsp;Twice a week
              </label><br />
              <label>
                <input type='radio' name='unsubscribe-frequency' />
                &nbsp;One time per week
              </label><br />
            </form>

            <div className='booking-unsubscribe-button-wrapper darkPatternContainer dpInterfaceInterference' about={explanations.interfaceInbalanceExplanation} custom-id='booking-unsubscribe-page1-buttons'>
              <button className='booking-unsubscribe-button-update'>Update my preferences</button>
              <button className='booking-unsubscribe-button-continue page-element' custom-page='2'>Unsubscribe</button>
            </div>

          </div>
        </div>
      </main>

      <CookieBanner 
          seed={variation}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}




//===============================================================================================================================
//
// BOOKING NEWSLETTER UNSUBSCRIBE PAGE 2
//
//===============================================================================================================================

export function BookingSiteUnsubscribePage2({ variation }: { variation: number }) {
  return (
    <div className='booking-site-main-wrapper'>
      <header className='booking-header booking-header-small'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-unsubscribe-logo2'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-unsubscribe-signout2'>
          <button className='noDarkPatternHighlight'><span className='booking-unsubscribe-account-image'>👤</span> My Account</button>
        </div>
      </header>

      <main className='booking-main'>
        <div className='booking-unsubscribe-wrapper'>
          <h3>Unsubscribe from our newsletter.</h3>

          <div className='booking-unsubscribe-form darkPatternContainer dpObstruction' about={explanations.additionalStepExplanation} custom-id='booking-unsubscribe-page2-obstruction'>
            <p className='booking-unsubscribe-categories darkPatternContainer dpInterfaceInterference' about={explanations.confusingLanguageExplanation} custom-id='booking-unsubscribe-page2-trick-question'>Please unselect the types of newsletter that you do not want to unsubscribe from.</p>

            <form className='booking-unsubscribe-form-inner darkPatternContainer dpInterfaceInterference' about='From the design, it is unclear if the checkboxes are selected or not.' custom-id='booking-unsubscribe-page2-checkboxes'>  
              <label className="booking-checkbox-evil">
                General Marketing
                <input type="checkbox" defaultChecked />
                <span className="booking-checkmark-evil"></span>
              </label>

              <label className="booking-checkbox-evil">
                Special Promotions
                <input type="checkbox" defaultChecked />
                <span className="booking-checkmark-evil"></span>
              </label>

              <label className="booking-checkbox-evil">
                Exclusive Discounts
                <input type="checkbox" defaultChecked />
                <span className="booking-checkmark-evil"></span>
              </label>

              <label className="booking-checkbox-evil">
                Travel Blogs
                <input type="checkbox" defaultChecked />
                <span className="booking-checkmark-evil"></span>
              </label>

              <label className="booking-checkbox-evil">
                Travel Tips
                <input type="checkbox" defaultChecked />
                <span className="booking-checkmark-evil"></span>
              </label>
            </form>

            <div className='booking-unsubscribe-button-wrapper'>
              <button className='booking-unsubscribe-button-confirm page-element' custom-page='3'>Confirm my choices</button>
            </div>
          </div>
       
        </div>
      </main>

      <CookieBanner 
          seed={variation}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}




//===============================================================================================================================
//
// BOOKING NEWSLETTER UNSUBSCRIBE PAGE 3
//
//===============================================================================================================================

export function BookingSiteUnsubscribePage3({ variation }: { variation: number }) {
  return (
    <div className='booking-site-main-wrapper'>
      <header className='booking-header booking-header-small'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-unsubscribe-logo3'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-unsubscribe-signout3'>
          <button className='noDarkPatternHighlight'><span className='booking-unsubscribe-account-image'>👤</span> My Account</button>
        </div>
      </header>

      <main className='booking-main'>
        <div className='booking-unsubscribe-wrapper'>
        
        <h3>Unsubscribe from our newsletter.</h3>

        <div className='booking-unsubscribe-form darkPatternContainer dpObstruction' about={explanations.additionalStepExplanation} custom-id='booking-unsubscribe-page3-obstruction'>
          <p>Please confirm your email address to unsubscribe from our newsletter.</p>

          <form>
            <input type='text' placeholder='E-Mail' className='booking-unsubscribe-email-textfield' />
          </form>

          <button className='booking-unsubscribe-button-email page-element' custom-page='4'>Unsubscribe</button>
        </div>

        </div>
      </main>

      <CookieBanner 
          seed={variation}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}




//===============================================================================================================================
//
// BOOKING NEWSLETTER UNSUBSCRIBE PAGE 4
//
//===============================================================================================================================

export function BookingSiteUnsubscribePage4({ variation }: { variation: number }) {
  return (
    <div className='booking-site-main-wrapper'>
      <header className='booking-header booking-header-small'>
        <h1 className='darkPatternContainer dpNone' custom-id='booking-unsubscribe-logo4'>HoteLand</h1>

        <div className='booking-header-account darkPatternContainer dpNone' custom-id='booking-unsubscribe-signout4'>
          <button className='noDarkPatternHighlight'><span className='booking-unsubscribe-account-image'>👤</span> My Account</button>
        </div>
      </header>

      <main className='booking-main'>
        <div className='booking-unsubscribe-wrapper'>
        
        <h3 className='booking-unsubscribe-final-confirmshaming darkPatternContainer dpSocialEngineering' custom-id='booking-unsubscribe-page4-confirmshaming' custom-score={0} about='The website uses emotional language in an attempt to persuate the user to change their mind.'>We are so sorry you want to leave us!</h3>

        <div className='booking-unsubscribe-form'>
          <p className='booking-unsubscribe-email-p darkPatternContainer dpObstruction' about={explanations.additionalStepExplanation} custom-id='booking-unsubscribe-page4-obstruction'>Success. We have send you an email with instructions how to unsubscribe from our newsletter.</p>

          <button className='booking-unsubscribe-button-email'>Go to Home</button>
        </div>

        </div>
      </main>

      <CookieBanner 
          seed={variation}
          companyName='HoteLand LLC' 
          backgroundColor="rgb(220, 220, 220)" 
          foregroundColor='black' 
          buttonPrimaryColor='#ccc'
          buttonTextColor='#111'
          buttonAccentColor='rgb(10, 100, 27)'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />  
    </div>
  )
}