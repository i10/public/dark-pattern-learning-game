import SmartShop24Game from "../smartshop24";

export default function Home({ params }: { params: { variation: string } }) {
  switch (params.variation) {
    case "6224f94c-01eb-4701-aac2-acccf6041415":
      return (<SmartShop24Game variation={0} levelID={params.variation} />)

    case "205621d3-dad6-434b-aee0-49d310938d5f":
      return (<SmartShop24Game variation={1} levelID={params.variation} />)

    default:
      break;
  }
}

export function generateStaticParams() {
  return [
    { variation: '6224f94c-01eb-4701-aac2-acccf6041415' }, 
    { variation: '205621d3-dad6-434b-aee0-49d310938d5f' }
  ]
}