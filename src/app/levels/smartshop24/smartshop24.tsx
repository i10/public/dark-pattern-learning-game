'use client'

import { useState, useEffect } from 'react';
import React from 'react';
import './smartshop24.css';
import CookieBanner from '../../cookiebanner';
import GameUI from '../../game-ui';
import * as explanations from '../../dark-pattern-data';



//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function SmartShop24Game({ variation, levelID }: { variation: number, levelID: string }) {
  return (
    <GameUI children={ [
      <SmartShop24 variation={variation} key='1' />
    ] } levelID={levelID} />
  )
}




//===============================================================================================================================
//
// SMARTSHOP MAIN SITE
//
//===============================================================================================================================

export function SmartShop24({ variation }: { variation: number }) {
  const [selectedColor, setSelectedColor] = useState((variation == 0) ? 0 : 1);
  const [selectedStorage, setSelectedStorage] = useState((variation == 0) ? 0 : 2);

  return (
    <div className='shop24-site-main-wrapper'>
      <header className='shop24-header'>
        <h1 className='darkPatternContainer dpNone' custom-id='shop24-logo'>SmartShop24</h1>
      </header>

      <div className='shop24-menu'>
        <ul>
          <li><a href="#" className='active'>Smartphones</a></li>
          <li><a href="#">Smart Watches</a></li>
          <li><a href="#">Tablets</a></li>
          <li><a href="#">Headphones & Audio</a></li>
          <li><a href="#">Miscellaneous</a></li>
        </ul>
      </div>

      <main className='shop24-main'>

        {(variation == 1) && <div className='shop24-urgency-offer darkPatternContainer dpSocialEngineering bottomTooltip' about={explanations.fakeCountdownUrgencyExplanation} custom-id='shop24-promo-countdown-urgency'>Get an additional <b>10%</b> off your next purchase with promo code "ILoveTech". Only valid for the next: <div className='shop24-countdown'><Countdown /></div></div>}

        <div className='shop24-image'>
          <img src='/shop_assets/smartphone.webp' />
        </div>

        <Shop24PriceInfo colorSelection={selectedColor} storageSelection={selectedStorage} />

        <div className='shop24-main-info'>
          <h1 className='darkPatternContainer dpNone' custom-id='shop24-product-title'>Samyang P30 Pro</h1>

          <div className='shop24-quick-infos darkPatternContainer dpNone' custom-id='shop24-delivery-info'>
            <div>📅 Delivery: 3-5 days</div>
          </div>

          <Shop24ColorSelection preselection={(variation == 0) ? 0 : 1} 
                                selection={selectedColor} 
                                buttonAction={setSelectedColor} 
          />

          <Shop24StorageSelection preselection={(variation == 0) ? 0 : 2} 
                                  selection={selectedStorage} 
                                  buttonAction={setSelectedStorage} 
          />


          <Shop24TradeIn />

          <Shop24Insurance usesDP={(variation == 0) ? false : true} />

        </div>

        <br className='clearfix' />

      </main>

      <CookieBanner 
          seed={(variation == 0) ? 0 : 2}
          companyName='SmartShop24' 
          backgroundColor="#ddd" 
          foregroundColor='black' 
          buttonPrimaryColor='#333'
          buttonTextColor='#fff'
          buttonAccentColor='#333'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333'
        />   
    </div>
  )
}




//===============================================================================================================================
//
// SMARTSHOP PRICE VIEW
//
//===============================================================================================================================

function Shop24PriceInfo({ colorSelection, storageSelection }: { colorSelection: number, storageSelection: number }) {
  function getCurrentPrice() {
    var basePrice = 599;
    
    // premium color price
    if (colorSelection == 1) {
      basePrice += 120
    }

    basePrice += (storageSelection * 160)

    return basePrice
  }

  return (
    <div className='shop24-current-price darkPatternContainer dpNone' custom-id='shop24-price-info'>
      <p>Your price:</p>
      <h1>{getCurrentPrice()}€</h1>
      <small>Includes VAT</small>

      <br />
      <button className='noDarkPatternHighlight'>Add to Bag</button>
    </div>
  )
}




//===============================================================================================================================
//
// SMARTSHOP OPTION SELECTORS
//
//===============================================================================================================================

function Shop24ColorSelection({  preselection, selection, buttonAction }: { preselection: number, selection: number, buttonAction: (selection: number) => void }) {

  const colorOptions = ['gold', 'diamond', 'gray', 'pink']
  const colorList = colorOptions.map((val, index) => {
    const classList = (index == selection) ? 'shop24-colorchoice noDarkPatternHighlight selected ' + val : 'shop24-colorchoice noDarkPatternHighlight ' + val;
    return <button className={classList} onClick={() => buttonAction(index)} key={index}></button>
  });
  
  const colorString = ["Gold", "Premium Diamond", "Mechanic Gray", "Pink"][selection];

  const containerClasses = (preselection == 1) ? 'shop24-color-selection darkPatternContainer dpInterfaceInterference' : 'shop24-color-selection darkPatternContainer dpNone';
  const containerAbout = (preselection == 1) ? 'The default preselection (Premium Diamond) is the most expensive choice.' : '';
  
  
  return (
    <div className={containerClasses} about={containerAbout} custom-id={(preselection == 1) ? 'shop24-color-option-preselection' : 'shop24-color-option-clean'}>
      <h2>Color</h2>
      <p>Selected: <span className='color-selection-name'>{colorString}</span></p>
      {colorList}
    </div>
  )
}



function Shop24StorageSelection({  preselection, selection, buttonAction }: { preselection: number, selection: number, buttonAction: (selection: number) => void }) {

  const storageOptions = ['256 GB', '512 GB', '1 TB']
  const storageList = storageOptions.map((val, index) => {
    const classList = (index == selection) ? 'shop24-sizechoice selected' : 'shop24-sizechoice';
    return <button className={classList} onClick={() => buttonAction(index)} key={index}>{val}</button>
  });
  
  const containerClasses = (preselection != 0) ? 'shop24-size-selection darkPatternContainer dpInterfaceInterference' : 'shop24-size-selection darkPatternContainer dpNone';
  const containerAbout = (preselection != 0) ? 'The default preselection (1 TB) is the most expensive choice.' : '';

  return (
    <div className={containerClasses} about={containerAbout} custom-id={(preselection != 0) ? 'shop24-storage-option-preselection' : 'shop24-storage-option-clean'}>
      <h2>Storage</h2>
      {storageList}
    </div>
  )
}



function Shop24TradeIn() {
  const [selectedTradeIn, setSelectedTradein] = useState(0);

  return (
    <div className='shop24-tradein darkPatternContainer dpNone' custom-id='shop24-tradein'>
      <h2>Trade-in</h2>
      <button className={(selectedTradeIn == 0) ? 'selected' : ''} onClick={() => setSelectedTradein(0)}>No trade-in</button>
      <button className={(selectedTradeIn == 1) ? 'selected' : ''} onClick={() => setSelectedTradein(1)}>Select a phone to trade in<br/><small>Save up to 120€</small></button>
      <br className='clearfix' />
    </div>
  )
}



function Shop24Insurance({ usesDP = false }) {
  const [selectedInsurance, setSelectedInsurance] = useState(0);

  const explanationString = "It combines a bad default with a subscription that the user might miss. The bad default makes it interface interference. Because it is preselected, this makes it a hidden subscription and thus sneaking."

  if (usesDP) {
    return (
      <div className='shop24-insurance darkPatternContainer dpInterfaceInterference dpSneaking dpLinkSneakingInsurance topTooltip' about={explanationString} custom-id='shop24-insurance-container-sneaking'>
        <h2>Insurance</h2>
        <button className={(selectedInsurance == 0) ? 'topTooltip darkPatternContainer dpInterfaceInterference dpSneaking dpLinkSneakingInsurance insurance-sneaked-subscription selected' : 'topTooltip darkPatternContainer dpSneaking dpLinkSneakingInsurance insurance-sneaked-subscription'} about={explanationString} custom-id='shop24-insurance-button-sneaking' onClick={() => setSelectedInsurance(0)}>Premium Protection<br/><small>For as low as 6,99€ per month</small></button>
        <button className={(selectedInsurance == 1) ? 'topTooltip darkPatternContainer dpSocialEngineering insurance-confirmshaming selected' : 'topTooltip darkPatternContainer dpSocialEngineering insurance-confirmshaming'} about='It uses emotional language to shame the user for not picking the premium protection.' custom-id='shop24-insurance-button2-confirmshaming' onClick={() => setSelectedInsurance(1)}>No additional protection<br/><small>I don't care about my new phone!</small></button>
        <br className='clearfix' />
      </div>
    )

  } else {
    return (
      <div className='shop24-insurance darkPatternContainer dpNone' custom-id='shop24-insurance-container-clean'>
        <h2>Insurance</h2>
        <button className={(selectedInsurance == 0) ? 'insurance-no-confirmshaming selected' : 'insurance-no-confirmshaming'} onClick={() => setSelectedInsurance(0)}>No additional protection</button>
        <button className={(selectedInsurance == 1) ? 'selected' : ''} onClick={() => setSelectedInsurance(1)}>Premium Protection<br/><small>For 6,99€ per month</small></button>
        <br className='clearfix' />
      </div>
    )
  }
}




//===============================================================================================================================
//
// HELPER VIEWS
//
//===============================================================================================================================

function Countdown() {
  const timeWithPaddingZero = (time: number) => {
    return (time < 10) ? `0${time}` : `${time}`;
  };
  
  const timeFormatter = (time: number) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${timeWithPaddingZero(minutes)}:${timeWithPaddingZero(seconds)}`;
  };

  const [seconds, setSeconds] = useState(3599);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (seconds > 0) {
      timer = setTimeout(() => setSeconds(s => s - 1), 1000);
    }
    
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [seconds]);

  return (
      <>{timeFormatter(seconds)}</>
  );
}