import TicketshopGame from '../ticketshop';


export default function Home({ params }: { params: { variation: string } }) {
  switch (params.variation) {
    case "83104ca6-45f6-4b36-904c-feec9308ac11":
      return (<TicketshopGame variation={0} levelID={params.variation} />)

    default:
      break;
  }
}


export function generateStaticParams() {
  return [
    { variation: '83104ca6-45f6-4b36-904c-feec9308ac11' }
  ]
}