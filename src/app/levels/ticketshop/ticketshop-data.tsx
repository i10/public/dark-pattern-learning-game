export type EventListing = {
    id: number;
    name: string;
    day: number;
    month: string;
    weekdayTime: string;
    fullDate: string;
    place: string;
    tickettype: string;
    price: string;
    previousPrice: string | null;
    interfaceInterference: boolean; //true = highlighted, false = greyed out
};

export const events: EventListing[] = [
    {
        id: 1,
        name: "SummerDance - The Festival",
        day: 24,
        month: "August",
        weekdayTime: "Sat 8:30 PM",
        fullDate: "Saturday, August 24th, 8:30 PM",
        place: "Chicago • Grant Park",
        tickettype: "Super Fan Package",
        price: "146.99",
        previousPrice: null,
        interfaceInterference: true
    },
    {
        id: 2,
        name: "SummerDance - The Festival",
        day: 24,
        month: "August",
        weekdayTime: "Sat 8:30 PM",
        fullDate: "Saturday, August 24th, 8:30 PM",
        place: "Chicago • Grant Park",
        tickettype: "VIP Ticket",
        price: "94.99",
        previousPrice: "109.99",
        interfaceInterference: false
    },
    {
        id: 3,
        name: "SummerDance - The Festival",
        day: 24,
        month: "August",
        weekdayTime: "Sat 8:30 PM",
        fullDate: "Saturday, August 24th, 8:30 PM",
        place: "Chicago • Grant Park",
        tickettype: "Standard Ticket",
        price: "64.99",
        previousPrice: "89.99",
        interfaceInterference: false
    },
]