'use client'

import { useState, useEffect } from 'react';
import React from 'react';
import './ticketshop.css';
import GameUI from '../../game-ui';
import * as explanations from '../../dark-pattern-data';
import { EventListing, events} from './ticketshop-data';


//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function TicketshopGame({ variation, levelID }: { variation: number, levelID: string }) {
  const [selectedTicket, setSelectedTicket] = useState<EventListing>(events[0]);
  const [selectedAmount, setSelectedAmount] = useState<number>(2);
  const [selectedDelivery, setSelectedDelivery] = useState<number>(0);

    return (
      <GameUI children={[
        <TicketshopSelectTicketWithCookieBanner variation={2} key='1' />,

        <TicketshopSelectTicket 
          variation={1} 
          key='2' 
          selectedTicket={selectedTicket} 
          setSelectedTicket={setSelectedTicket}
          amount={selectedAmount}
          setAmount={setSelectedAmount} />,

        <TicketshopDelivery 
          variation={variation} 
          key='3' 
          selectedTicket={selectedTicket} 
          selectedDelivery={selectedDelivery} 
          setSelectedDelivery={setSelectedDelivery}
          amount={selectedAmount} />,

        <TicketshopBasket 
          variation={variation} 
          key='4' 
          selectedTicket={selectedTicket} 
          selectedDelivery={selectedDelivery}
          amount={selectedAmount}
          setAmount={setSelectedAmount} />
      ] } levelID={levelID} />
    )
}



//===============================================================================================================================
//
// TICKETSHOP SELECT TICKET WITH COOKIE BANNER - PAGE 1
//
//===============================================================================================================================

function TicketshopSelectTicketWithCookieBanner({variation}: {variation: number}) {
  return (
    <div>
    {/* Background view without dp annotations */}
    <div className='ticket-main-blur'>
      <TicketshopSelectTicket variation={variation} selectedTicket={events[0]} setSelectedTicket={() => {}} amount={1} setAmount={() => {}} />
    </div>

    <FullScreenCookieBanner />
  </div>
  )
}

function FullScreenCookieBanner() {
  return (
    <div className='ticketshop-cookie-banner'>
      <div className='ticketshop-cookie-banner-inner darkPatternContainer dpForcedAction dpLinkFullscreenCookieBannerForcedActionKSFJ182' about={explanations.forcedActionCookieBannerExplanation} custom-id='ticketshop-fullscreen-cookie-banner'>
        <h1>TicketDreams</h1>
        <p>We, TicketDreams, are part of the TicketDreams family of brands.</p><p>When you use our sites and apps, we use cookies to:</p>
        <ul>
          <li>provide our sites and apps to you</li>
          <li>authenticate users, apply security measures, and prevent spam and abuse, and</li>
          <li>measure your use of our sites and apps</li>
        </ul>

        <hr />

        <p>If you click '<b>Accept all</b>', we and <span className='ticketshop-cookie-banner-link'>our partners</span>, including 238 who are part of the IAB Transparency & Consent Framework, will also store and/or access information on a device (in other words, use cookies) and use precise geolocation data and other personal data such as IP address and browsing and search data, for personalised advertising and content, advertising and content measurement, and audience research and services development.</p>
        <p>If you do not want us and our partners to use cookies and personal data for these additional purposes, click '<b>Reject all</b>'.</p>
        <p>If you would like to customise your choices, click '<b>Manage privacy settings</b>'.</p>
        <p>You can change your choices at any time by clicking on the 'Privacy & cookie settings' or 'Privacy dashboard' links on our sites and apps. Find out more about how we use your personal data in our <span className='ticketshop-cookie-banner-link'>privacy policy</span> and <span className='ticketshop-cookie-banner-link'>cookie policy</span>.</p>

        <div className='ticketshop-cookie-banner-button-row darkPatternContainer dpForcedAction dpLinkFullscreenCookieBannerForcedActionKSFJ182' about={explanations.forcedActionCookieBannerExplanation} custom-id='ticketshop-fullscreen-cookie-banner-buttons'>
          <button className='ticketshop-cookie-banner-button page-element' custom-page='2'>Accept all</button>
          <button className='ticketshop-cookie-banner-button page-element' custom-page='2'>Reject all</button>
          <button className='ticketshop-cookie-banner-button page-element' custom-page='2'>Manage privacy settings</button>
        </div>
      </div>
    </div>
  )
}


//===============================================================================================================================
//
// TICKETSHOP SELECT TICKET - PAGE 1
//
//===============================================================================================================================

export function TicketshopSelectTicket({ variation, selectedTicket, setSelectedTicket, amount, setAmount }: { variation: number, selectedTicket: EventListing, setSelectedTicket: React.Dispatch<React.SetStateAction<EventListing>>, amount: number, setAmount: React.Dispatch<React.SetStateAction<number>>}) {
  const mainClasses = (variation == 2) ? 'ticketshop-main-wrapper dpNoContainer' : 'ticketshop-main-wrapper';


    return (
      <div className={mainClasses}>
        <header className='ticketshop-header'>
          <h1 className='darkPatternContainer dpNone' custom-id='ticketshop-logo'>TicketDreams</h1>

          <div className='ticketshop-header-searchbar darkPatternContainer dpNone' custom-id='ticketshop-searchbar'>
            <button className='noDarkPatternHighlight'>🔎 Search for artist and events</button>
          </div>
        </header>

        <main className='ticketshop-main'>
          <div className='ticketshop-top-of-the-page'>
            <div className='ticketshop-picture-teaser'>
              <img src='/ticketshop_assets/festival.jpg' />
              <div className='ticketshop-headline-teaser'>
                <h1>Get your tickets for the event of the year!</h1>
              </div>
            </div>
            <h1 className='ticketshop-header-high-demand darkPatternContainer dpSocialEngineering' about={explanations.highDemandExplanation} custom-id='ticketshop-high-demand'>
              Very popular! 1.562 people viewed this in the last 24 hours
            </h1>
            <div className='ticketshop-header-countdown-urgency darkPatternContainer dpSocialEngineering' about={explanations.fakeCountdownUrgencyExplanation} custom-id='ticketshop-fake-countdown'>
              <TicketShopCountdown />
            </div>
          </div>
          
          <div className='ticketshop-body'>
            <Testimonials />

            <div className='ticketshop-amount-selection darkPatternContainer dpInterfaceInterference' about='The website preselects two tickets as a default.' custom-id='ticketshop-amount-two-preselection'>
              <h4>Number of Tickets:</h4>
              <AmountPicker variation={0} amount={amount} setAmount={setAmount} min={1} max={10} />
            </div>

            <EventListings selectedTicket={selectedTicket} setSelectedTicket={setSelectedTicket} />
          </div>
        </main>
      </div>
    )
  }



//===============================================================================================================================
//
// Testimonials
//
//===============================================================================================================================

function Testimonials() {
  return (
    <div className='ticketshop-testimonials-container'>
      <div className ='ticketshop-testimonials darkPatternContainer dpSocialEngineering dpLinkTestimonials' about={explanations.testimonialsExplanation} custom-id='ticketshop-testimonials-group'>
        
        <div>
          
          <SingleTestiomonial NumberStars = '5'
                                description = '"The atmosphere is incredible. Worth every penny!"'
                                author = '- Ben, will visit again' />
          <SingleTestiomonial NumberStars = '5'
                                description = '"The best show I ever witnessed!"'
                                author = '- Emily, concert enthuiast' />
          <SingleTestiomonial NumberStars = '5'
                                description = '"I will still remember this night in 20 years!"'
                                author = '- Taylor, last years visitor' />
        </div>
      
      </div>
    </div>
  );
}

function SingleTestiomonial({ NumberStars = "5", description = "xyz", author = "abc" }) {
  const stars = [];
  for (let i = 0; i < Number(NumberStars); i++) {
    stars.push(<span key={i}>★</span>)
  }

  return (
    <div className='ticketshop-testimonials-single-group darkPatternContainer dpSocialEngineering dpLinkTestimonials' about={explanations.testimonialsExplanation} custom-id='ticketshop-single-testimonial'>
      <div className='ticketshop-testimonials-stars'>{stars}</div>
      <h2>{description}</h2>
      <h4>{author}</h4>
    </div>
  );
}


//===============================================================================================================================
//
// EVENT LISING
//
//===============================================================================================================================

function EventListings({ selectedTicket, setSelectedTicket }: { selectedTicket: EventListing, setSelectedTicket: React.Dispatch<React.SetStateAction<EventListing>> }) {
  const listItems = events.map(event =>
    <Event event={event} key={event.name} selectedTicket={selectedTicket} setSelectedTicket={setSelectedTicket} />
  );

  return (
    <div className='ticketshop-eventListings'>
      {listItems}
    </div>
  );
}



function Event({ event, selectedTicket, setSelectedTicket }: { event: EventListing,  selectedTicket: EventListing, setSelectedTicket: React.Dispatch<React.SetStateAction<EventListing>> }) {

  const previousPrice = (event.previousPrice != null) ? 'ticketshop-event-price darkPatternContainer dpSocialEngineering dpLinkPreviousPriceXC64728' : 'ticketshop-event-price darkPatternContainer dpNone';
  const previousPriceID = (event.previousPrice != null) ? 'ticketshop-event-container-previousprice' : 'ticketshop-event-container-price'

  const buttonClassList = event.interfaceInterference ? 'ticketshop-event-buyTicket highlighted darkPatternContainer dpInterfaceInterference dpLinkPurchaseButtonInterfaceInterferenceZGK928' : 'ticketshop-event-buyTicket darkPatternContainer dpInterfaceInterference dpLinkPurchaseButtonInterfaceInterferenceZGK928';
  const buttonCustomID = event.interfaceInterference ? 'ticketshop-event-buyTicket-visualInterference-highlighted' : 'ticketshop-event-buyTicket-visualInterference-lowlighted'


  return (
    <div className="ticketshop-event darkPatternContainer dpNone" custom-id="ticketshop-event-container">
      <table className='ticketshop-event-listing-table'>
        <tbody>
          <tr>
            <td>
              <div className="ticketshop-event-date darkPatternContainer dpNone" custom-id="ticketshop-event-container-date">
                <h1>{event.day}</h1>
                <h2>{event.month}</h2>
                <h3>{event.weekdayTime}</h3>
              </div>
            </td>

            <td>
              <div className="ticketshop-event-infos darkPatternContainer dpNone" custom-id="ticketshop-event-container-title">
                <div className="ticketshop-event-infos-2">
                  <h1>{event.name}</h1>
                  <h2>{event.place}</h2>
                  <h3>{event.tickettype}</h3>
                </div>
              </div>
            </td>

            <td>
              <div className={previousPrice} about={(event.previousPrice != null) ? explanations.previousPriceExplanation : ""} custom-id={previousPriceID}>
                {(event.previousPrice != null) ? (
                  <div className='ticketshop-event-price-2'>
                    <h1> $ {event.price} </h1>
                    <h3> $ {event.previousPrice} </h3>
                  </div>
                ) : (
                  <div className='ticketshop-event-price-2'>
                    <h1> $ {event.price} </h1>
                  </div>
                )
                }
              </div>
            </td>

            <td>
              <div className='ticketshop-event-buy-wrapper'>
                <div className={buttonClassList} about={explanations.interfaceInbalanceExplanation} custom-id={buttonCustomID}>
                  <button className='ticketshop-event-buyTicket-button page-element' custom-page='3' onClick={() => { setSelectedTicket(event) }}>Buy Ticket(s)</button>
                </div> 
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}


function TicketShopCountdown() {

  const timeFormatter = (time: number) => {
    const hours = Math.floor(time / 3600);
    const minutes = Math.floor((time - (hours * 3600)) / 60);
    const seconds = time % 60;
    return `Megafan Sale ends in: ${hours} hours ${minutes} minutes ${seconds} seconds`;
  };

  const [seconds, setSeconds] = useState(15694);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (seconds > 0) {
      timer = setTimeout(() => setSeconds(s => s - 1), 1000);
    }
    
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [seconds]);

  return (
      <>{timeFormatter(seconds)}</>
  );
}


function AmountPicker({ variation, amount, setAmount, min = 1, max = 10 }: { variation: number, amount: number, setAmount: React.Dispatch<React.SetStateAction<number>>, min: number, max: number}) {
  const divClassName = variation == 0 ? 'ticketshop-amount-picker' : 'ticketshop-amount-picker-small';

  return (
    <div className={divClassName}>
      <button onClick={() => setAmount(Math.max(amount - 1, min))}>-</button>
      <div className='ticketshop-amount-display'>{amount}</div>
      <button onClick={() => setAmount(Math.min(amount + 1, max))}>+</button>
    </div>
  );

}





//===============================================================================================================================
//
// TICKETSHOP SELECT DELIVERY - PAGE 2
//
//===============================================================================================================================

export function TicketshopDelivery({ variation, selectedTicket, selectedDelivery, setSelectedDelivery, amount }: { variation: number, selectedTicket: EventListing, selectedDelivery: number, setSelectedDelivery: React.Dispatch<React.SetStateAction<number>>, amount: number}) {
  return (
    <div className='ticketshop-main-wrapper'>
        <header className='ticketshop-header'>
          <h1 className='darkPatternContainer dpNone' custom-id='ticketshop-logo'>TicketDreams</h1>
        </header>

        <main className='ticketshop-main'>
          <div className='ticketshop-top-of-the-page'>
            <div className='ticketshop-picture-teaser'>
              <img src='/ticketshop_assets/festival.jpg' />
              <div className='ticketshop-headline-teaser'>
                <h1>Get your tickets for the event of the year!</h1>
              </div>
            </div>
          </div>

          <div className='ticket-selection-wrapper darkPatternContainer dpNone' custom-id='ticketshop-delivery-stop-basket'>
            <h3>Your ticket selection:</h3>
            <hr />
            <table>
              <tbody>
                <tr>
                  <td>
                    <p>{amount} ｘ {selectedTicket?.name}</p>
                    <p><small>{selectedTicket.tickettype}</small></p>
                    <p><small>{selectedTicket.fullDate}</small></p>
                  </td>
                  <td>
                    <b>{amount} ｘ ${selectedTicket.price}</b>
                  </td>
                </tr>
              </tbody>
            </table>
            
          </div>
          
          <div className='ticket-delivery-method-wrapper'>
            <p>Please select your preferred delivery method:</p>

            <div className='ticket-delivery-method-wrapper-inner darkPatternContainer dpInterfaceInterference' about='The default selection is for the most expensive delivery option.' custom-id='ticketshop-delivery-preselection'>

              <div>
                <input type='radio' id='delivery-method-1' name='delivery-method' value='delivery-method-1' defaultChecked={selectedDelivery == 0} onChange={() => {setSelectedDelivery(0)}} />
                <label htmlFor='delivery-method-1'>
                  Express Delivery
                  <br />
                  <small>Delivered by tomorrow.</small>
                </label>

                <span>$6.40</span>
              </div>

              <div>
                <input type='radio' id='delivery-method-2' name='delivery-method' value='delivery-method-2' defaultChecked={selectedDelivery == 1} onChange={() => {setSelectedDelivery(1)}} />
                <label htmlFor='delivery-method-2'>
                  Standard Delivery
                  <br />
                  <small className='darkPatternContainer dpSocialEngineering' custom-id='ticket-delivery-option-subtitle-fomo' about='This pushes the user to choose the more expensive delivery option to not miss out on the event with (presumably) artificial fear.'>Tickets may not arrive in time.</small>
                </label>

                <span>$3.20</span>
              </div>

              <div>
                <input type='radio' id='delivery-method-3' name='delivery-method' value='delivery-method-3' defaultChecked={selectedDelivery == 2} onChange={() => {setSelectedDelivery(2)}} />
                <label htmlFor='delivery-method-3'>
                  Mobile Ticket
                  <br />
                  <small>Just use your mobile phone at the venue.</small>
                </label>

                <span>$1.00</span>
              </div>
            </div>
          </div>

          <div className='clearfix'></div>
         
          <div className='ticket-delivery-button-navigation'>
            <button className='ticketshop-default-button' onClick={() => { }}>Back to ticket selection</button>
            <button className='ticketshop-default-button page-element' custom-page='4' onClick={() => { }}>Continue to checkout</button>
          </div> 

        </main>
      </div>
  );
}




//===============================================================================================================================
//
// TICKETSHOP BASKET - PAGE 3
//
//===============================================================================================================================

export function TicketshopBasket({ variation, selectedTicket, selectedDelivery, amount, setAmount }: { variation: number, selectedTicket: EventListing, selectedDelivery: number, amount: number, setAmount: React.Dispatch<React.SetStateAction<number>>}) {
  const [selectedInsurance, setSelectedInsurance] = useState<number>(1);

  const deliveryPrice = (selectedDelivery == 0) ? "$6.40" : (selectedDelivery == 1) ? "$3.20" : "$1.00";
  const deliveryDescription = (selectedDelivery == 0) ? "Express Delivery" : (selectedDelivery == 1) ? "Standard Delivery" : "Mobile Ticket";

  const totalPrice = (Number(selectedTicket.price) * amount) + Number(deliveryPrice.replace('$', '')) + (selectedInsurance * 7.99);
  
  return (
    <div className='ticketshop-main-wrapper'>
        <header className='ticketshop-header'>
          <h1 className='darkPatternContainer dpNone' custom-id='ticketshop-logo'>TicketDreams</h1>
        </header>

        <main className='ticketshop-main'>
          <div className='ticketshop-top-of-the-page'>
            <div className='ticketshop-picture-teaser'>
              <img src='/ticketshop_assets/festival.jpg' />
              <div className='ticketshop-headline-teaser'>
                <h1>Get your tickets for the event of the year!</h1>
              </div>
            </div>
          </div>
          
          <HiddenFinePrint />
          
          <div className='ticket-basket-wrapper'>
            <p>Confirm your order:</p>

            <div className='ticket-basket-wrapper-inner darkPatternContainer dpSneaking dpLinkSneakIntoBasket8293SC' about='This sneaks the insurance into the basket in the final step so users might miss it.' custom-id='ticketshop-checkout-basket'>

              <TicketShopBasketItem description={
              <div>
                <p>{amount} ｘ Ticket:<br/>{selectedTicket.name}<br/><small>{selectedTicket.tickettype}</small></p>
              </div>
              } price={amount + " ｘ $" + selectedTicket.price}
                amount={amount} setAmount={setAmount}
                amountPickerMin={1} amountPickerMax={10} 
                linkClassName={null} />

              <TicketShopBasketItem description={
              <div>
                <p>Ticket Insurance<br /><small>You are protected in case that the event does not happen.</small></p>
              </div>
              } price="$7.99" 
                amount={selectedInsurance} setAmount={setSelectedInsurance}
                amountPickerMin={0} amountPickerMax={1}
                linkClassName={"dpLinkSneakIntoBasket8293SC"} />

              <TicketShopBasketItem description={
              <div>
                <p>Delivery Fee<br /><small>{deliveryDescription}</small></p>
              </div>
              } price={deliveryPrice} 
                amount={0} setAmount={() => {}}
                amountPickerMin={0} amountPickerMax={0}
                linkClassName={null} />
              
              <hr />

              <TicketShopBasketItem description={
              <div>
              <p>Total</p>
                </div>
              } price={"$" + totalPrice.toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2})}
                amount={0} setAmount={() => {}}
                amountPickerMin={0} amountPickerMax={0}
                linkClassName={null} />

            </div>
          </div>

          <div className='clearfix'></div>

         
          <div className='ticket-delivery-button-navigation'>
            <button className='ticketshop-default-button'>Back to delivery selection</button>
            <button className='ticketshop-default-button' onClick={() => { }}>Place ticket order</button>
          </div> 

        </main>
      </div>
  );
}

function TicketShopBasketItem({ description, price, amount, setAmount, amountPickerMin, amountPickerMax, linkClassName }: { description: React.ReactElement, price: string, amount: number, setAmount: React.Dispatch<React.SetStateAction<number>>,  amountPickerMin: number, amountPickerMax: number, linkClassName: string | null }) {
  const showPicker = amountPickerMax > 0;

  const outerClassName = (linkClassName == null) ? 'ticket-basket-item darkPatternContainer dpNone' : 'ticket-basket-item darkPatternContainer dpSneaking ' + linkClassName;
  const outerAboutText = (linkClassName == null) ? '' : 'This sneaks the insurance into the basket in the final step so users might miss it.';
  const outerCustomID = (linkClassName == null) ? 'ticketshop-checkout-basket-item' : 'ticketshop-checkout-basket-item-sneaking';

  return (
    <div className={outerClassName} about={outerAboutText} custom-id={outerCustomID}>
      <div className='ticket-basket-item-price'>
        {price}
        {showPicker && <AmountPicker variation={1} amount={amount} setAmount={setAmount} min={amountPickerMin} max={amountPickerMax} />}
      </div>
      {description}
    </div>
  )
}


function HiddenFinePrint() {
  const [isExpanded, setIsExpanded] = useState<boolean>(false);

  const containerClass = isExpanded ? 'ticket-basket-fineprint expanded' : 'ticket-basket-fineprint';
  
  return (
    <div className='ticket-basket-wrapper ticket-basket-fineprint-wrapper'>
      <p>The fine print:</p>

      <div className={containerClass} id='ticket-basket-fineprint' onClick={() => { setIsExpanded(!isExpanded); document.getElementById("ticket-basket-fineprint")?.scrollTo(0,0); }}>
        <div className='ticket-basket-fineprint-inner darkPatternContainer dpSneaking' custom-id='ticketshop-hidden-fineprint'>

          <span className='ticket-hidden-indicator'>{isExpanded ? "◀" : "▼"}</span>
          <p>All ticket sales are final.</p>
          <p>By clicking the button below, you agree to our terms and conditions. You will be charged the total amount immediately. No refunds or exchanges are possible.</p>
          <p>If the event is canceled or postponed, you are eligible to a partial or full refund at the discretion of the venue.</p>
          <p>All tickets are personalised and you are forbidden from reselling them. Any action to the contrary will result in the invalidation of the ticket.</p>
        
          <div className='ticket-hidden-newsletter darkPatternContainer dpSneaking dpInterfaceInterference' about='This hides the newsletter subscription from the user how has to expand and scroll the fineprint to unselect it. Furthermore, it preselects the newsletter subscription as a bad default.' custom-id='ticketshop-hidden-newsletter-checkbox'>
            <input type="checkbox" id="ticket-newsletter" name="ticket-newsletter" defaultChecked onClick={(e) => {e.stopPropagation(); }} />
            <label htmlFor="ticket-newsletter" onClick={(e) => {e.stopPropagation(); }}>I want to subscribe to the newsletter.</label>
          </div>

        </div>
      </div>
    </div>
  )
}