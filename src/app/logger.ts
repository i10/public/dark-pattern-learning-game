import { Prisma } from "@prisma/client";



// log a message to the console and the database (production only)
export async function logMessage(level: string, msg: string) {
  const userID = sessionStorage.getItem('user_id') ?? generateNewUserID();

  const timestamp = Date.now();
  const logMessage = userID + "," + level + "," + msg;

  // console logging
  console.log(timestamp + " - " + logMessage);
  
  // only log to database in production
  if (process.env.NODE_ENV != 'production') { return }


  const log: Prisma.LogCreateInput = {
    user: { connect: { userID: userID }},
    level,
    message: msg,
  }
  // send to database
  const response = await fetch('/api/log', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(log),
  });
  const json = await response.json();
  if (json?.error) {
    console.log(json.error);
  }
}

// log a message to the console and the databas (production only) and call a completion handler
export function logMessageWithCompletion(level: string, msg: string, completionHandler: () => void) {
  logMessage(level, msg).then(() => {
    completionHandler();
  });
}








export function generateNewUserID() {
  // Note: this does not guarantee a unique id, but for the scope of a user study, it should suffice
  const newUserID = "user_" + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  console.log(newUserID);
  sessionStorage.setItem('user_id', newUserID);
  return newUserID;
}



export function exportCurrentUserData() {
  const userID = sessionStorage.getItem('user_id');
  if (!userID) { return }

  var csvRows: string[] = [];

  // parse the session storage for log messages
  for (let i = 0; i < sessionStorage.length; i++) {
    const key = sessionStorage.key(i);
    if ((key) && (key != 'user_id')) {
      const msg = sessionStorage.getItem(key);
      if (msg?.includes(userID)) {
        // log for this user
        csvRows.push(key + "," + msg)
      }
    }
  }

  if (csvRows.length <= 0) { return }
  csvRows.sort();
  csvRows.splice(0, 0, "Timestamp,Noise,UserID,Level,Message");

  // create the csv data
  const csvData = csvRows.join('\n');
  const blob = new Blob([csvData], { type: 'text/csv' });

  // download
  const url = window.URL.createObjectURL(blob) 
  const a = document.createElement('a') 
  a.setAttribute('href', url) 
  a.setAttribute('download', userID + '.csv');
  a.click() 
}