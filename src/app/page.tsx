import ContractsPage from './contracts';
import dynamic from 'next/dynamic';

const DynamicContracts = dynamic(() => import('./contracts'), {
  ssr: false, // disable server side rendering for js session storage code
})

export default function Home() {
  return (
    <DynamicContracts />
  )
}