'use client'

import React from 'react';
import './renderer.css';
import { toJpeg } from 'html-to-image';
import { SmartShop24 } from '../levels/smartshop24/smartshop24';
import { BookingSiteListings } from '../levels/booking-site/bookingsite-listings';
import BookingSiteDetailsGame, { BookingSiteDetailsWithOverlay } from '../levels/booking-site/bookingsite-details';
import { BookingSiteUnsubscribePage1 } from '../levels/booking-site/bookingsite-unsubscribe';
import TutorialClassifyGame from '../tutorial/tutorial-classify';
import { TicketshopSelectTicket } from '../levels/ticketshop/ticketshop';
import { events } from '../levels/ticketshop/ticketshop-data';


export default function PageRenderer() {

  function renderHTMLElement(id: string) {
    const element = document.getElementById(id);
    if (!element) { return }
    
    toJpeg(element, { cacheBust: false })
      .then((dataUrl) => {
        const link = document.createElement("a");
        link.download = id + ".jpg";
        link.href = dataUrl;
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div className='renderer-wrapper'>
      <div className='renderer-wrapper-inner'>

        <p>Note: The download function does not work properly with Safari.</p>

        <h1>SmartShop24</h1>

        <div className='renderer-container' id="smartshop_1">
          <SmartShop24 variation={0} />
        </div>
        <button onClick={() => { renderHTMLElement("smartshop_1") }}>Download</button>

        <div className='renderer-container' id="smartshop_2">
          <SmartShop24 variation={1} />
        </div>
        <button onClick={() => { renderHTMLElement("smartshop_2") }}>Download</button>



        <h1>HoteLand</h1>

        <div className='renderer-container' id="hoteland_1">
          <BookingSiteListings variation={0} />
        </div>
        <button onClick={() => { renderHTMLElement("hoteland_1") }}>Download</button>

        <div className='renderer-container' id="hoteland_2">
          <BookingSiteDetailsWithOverlay variation={1} />
        </div>
        <button onClick={() => { renderHTMLElement("hoteland_2") }}>Download</button>

        <div className='renderer-container' id="hoteland_3">
          <BookingSiteUnsubscribePage1 variation={0} />
        </div>
        <button onClick={() => { renderHTMLElement("hoteland_3") }}>Download</button>


        <h1>TicketDreams</h1>

        <div className='renderer-container' id="ticketdreams_1">
          <TicketshopSelectTicket variation={0} selectedTicket={events[0]} setSelectedTicket={()=>{}} amount={2} setAmount={()=>{}}/>
        </div>
        <button onClick={() => { renderHTMLElement("ticketdreams_1") }}>Download</button>


        <h1>Game UI</h1>

        <div className='renderer-container-big' id="gameui">
          <BookingSiteDetailsGame variation={1} levelID='renderer' />
        </div>
        <button onClick={() => { renderHTMLElement("gameui") }}>Download</button>

        <div className='renderer-container-big' id="gameui2">
          <TutorialClassifyGame levelID='8e1f3930-77b5-41bd-8b31-447748325bb3' />
        </div>
        <button onClick={() => { renderHTMLElement("gameui2") }}>Download</button>

      </div>
    </div>
  )
}