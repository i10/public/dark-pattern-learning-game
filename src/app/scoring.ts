export enum DarkPatternCategory {
  Obstruction = 0,
  Sneaking = 1,
  InterfaceInterference = 2,
  ForcedAction = 3,
  SocialEngineering = 4,
  None = 5,
  Unsure = 6,
  UnsureClassify = 7
}

export type ScoreResult = {
  element: string;
  title: string;
  categories: DarkPatternCategory[];
  value: number;
  scoreExplanation: ScoreChange[];
  explanation: string;
  newClassName: string;
}

export type ClassifyScoreResult = {
  value: number;
  newClassName: string;
}

export type ScoreChange = {
  title: string;
  change: number;
};


export function getScoringResult(element: Element, answer: DarkPatternCategory): ScoreResult | null {
  const elementName = "<" + (element.attributes.getNamedItem('custom-id')?.value ?? element.className) + ">";

  const correctCategories = getDPCategoriesFromElement(element);
  if (correctCategories.length <= 0) { return null }
  
  const elementValueString = element.attributes.getNamedItem('custom-score')?.value ?? "100"
  const elementValue = parseInt(elementValueString) ?? 100

  const bonusExplanationStr = (elementValue <= 0) ? " (but ambigious or very mild)" : ""

  const explanationString = element.attributes.getNamedItem('about')?.value ?? ''
  const explanation = "This is " + getDarkPatternTitles(correctCategories) + ".\n" +  explanationString;

  const darkPatternFoundValue = 100;


  if (answer == DarkPatternCategory.Unsure) {
    // marked as dp, but not sure which, two cases:

    if (correctCategories.includes(DarkPatternCategory.None)) {
      // it is actually no dark pattern, so unsure is incorrect
      return {
        element: elementName,
        title: "Incorrect!",
        categories: [DarkPatternCategory.None],
        value: -Math.floor(elementValue/2),
        scoreExplanation: [
          {
            title: "False Positive",
            change: -Math.floor(elementValue/2),
          }
        ],
        explanation: "This is actually no dark pattern. " + explanationString,
        newClassName: "dpIncorrect"
      }
    
    } else {
      // it is a dark pattern, so unsure is correct

      return {
        element: elementName,
        title: "This is a dark pattern!",
        categories: correctCategories,
        value: darkPatternFoundValue - Math.floor(elementValue/2),
        scoreExplanation: [
          {
            title: "Found a Dark Pattern",
            change: darkPatternFoundValue,
          },
          {
            title: "No Category" + bonusExplanationStr,
            change: -Math.floor(elementValue/2),
          }
        ],
        explanation: explanation,
        newClassName: "dpPartlyCorrect"
      }
    } 


  } else if ((answer == DarkPatternCategory.None) && (!correctCategories.includes(DarkPatternCategory.None))) {
    // failed to mark a dp

    return {
      element: elementName,
      title: "Missed Dark Pattern!",
      categories: correctCategories,
      value: -elementValue,
      scoreExplanation: [
        {
          title: "False Negative",
          change: -elementValue,
        }
      ],
      explanation: explanation,
      newClassName: "dpMissing"
    }


  } else if (correctCategories.includes(DarkPatternCategory.None)) {
    // false positive

    return {
      element: elementName,
      title: "Incorrect!",
      categories: [DarkPatternCategory.None],
      value: -elementValue,
      scoreExplanation: [
        {
          title: "False Positive",
          change: -Math.floor(elementValue/2),
        },
        {
          title: "Wrong Category" + bonusExplanationStr,
          change: -elementValue/2,
        }
      ],
      explanation: "This is actually no dark pattern. " + explanationString,
      newClassName: "dpIncorrect"
    }

  } else {
    // correctly marked something as a dark pattern

    if (!correctCategories.includes(answer)) {
      // but wrong category

      return {
        element: elementName,
        title: "Incorrect!",
        categories: correctCategories,
        value: darkPatternFoundValue - elementValue,
        scoreExplanation: [
          {
            title: "Found a Dark Pattern",
            change: darkPatternFoundValue,
          },
          {
            title: "Wrong Category" + bonusExplanationStr,
            change: -elementValue,
          }
        ],
        explanation: explanation,
        newClassName: "dpIncorrect"
      }

    } else {
      // correct category

      return {
        element: elementName,
        title: "Correct!",
        categories: correctCategories,
        value: elementValue + darkPatternFoundValue,
        scoreExplanation: [
          {
            title: "Found a Dark Pattern",
            change: darkPatternFoundValue,
          },
          {
            title: "Correct Category" + bonusExplanationStr,
            change: elementValue,
          }
        ],
        explanation: explanation,
        newClassName: "dpCorrect"
      }
    }
  }
}





export function getClassifyScoringResult(element: Element, answer: DarkPatternCategory): ClassifyScoreResult | null {

  const correctCategories = getDPCategoriesFromElement(element);

  const elementValueString = element.attributes.getNamedItem('custom-score')?.value ?? "100"
  const elementValue = parseInt(elementValueString) ?? 100

  if (answer == DarkPatternCategory.UnsureClassify) {
    // not sure
    return {
      value: -Math.floor(elementValue/2),
      newClassName: "dpClassifyPartlyCorrect"
    }
  
  } else if (correctCategories.includes(answer)) {
    // correct answer
    return {
      value: elementValue,
      newClassName: "dpClassifyCorrect"
    }

  } else {
    // incorrect answer
    return {
      value: -elementValue,
      newClassName: "dpClassifyIncorrect"
    }
  }
}





export function applyScoringHighlight(element: Element, result: ScoreResult, clickCallback: (value: React.SetStateAction<ScoreResult | null>) => void) {
  const category = getDPCategoryClassNameFromElement(element);
  const newClassName = result.newClassName;

  // same elemments can be linked together bei common dpLinkXYZ identifier
  const classes = element.classList;
  var linkClassName: string | null = null
  for (let i = 0; i < classes.length; i++) {
    const className = classes[i];
    if (className.startsWith("dpLink")) {
      linkClassName = className
    }
  }

  
  if (linkClassName != null) {
    // element is linked other elements: highlight all
    element.classList.remove('dpContainerSelected');
    
    const otherLinkedElements = document.getElementsByClassName(linkClassName);
    for (let i = 0; i < otherLinkedElements.length; i++) {
      const otherLinkedElement = otherLinkedElements[i];
      
      otherLinkedElement.classList.add(newClassName, 'noDarkPatternHighlight');
      otherLinkedElement.classList.remove('dpContainerActive', 'darkPatternContainer');
      otherLinkedElement.setAttribute('data-text', result.explanation);
      addScoreResultClickHandler(otherLinkedElement, result, clickCallback);
    }

  } else {
    // highlight the element
    element.classList.add(newClassName, 'noDarkPatternHighlight');
    element.setAttribute('data-text', result.explanation);
    addScoreResultClickHandler(element, result, clickCallback);
    
    element.classList.remove('dpContainerSelected', 'dpContainerActive', 'darkPatternContainer');
  }
}



// adds a click event handler to a incorrectly classified element for explanation and user justification
function addScoreResultClickHandler(element: Element, result: ScoreResult, clickCallback: (value: React.SetStateAction<ScoreResult | null>) => void) {
  if (result.newClassName != "dpCorrect") { 
    element.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopImmediatePropagation();
      clickCallback(result)
    })
  }
}



export function applyClassifyScoringHighlight(element: Element, result: ClassifyScoreResult) {
  element.classList.add(result.newClassName)
}





function getDPCategoryFromElement(element: Element): DarkPatternCategory {
  const classList = element.classList
  if (classList.contains('dpObstruction')) { return DarkPatternCategory.Obstruction }
  else if (classList.contains('dpSneaking')) { return DarkPatternCategory.Sneaking }
  else if (classList.contains('dpInterfaceInterference')) { return DarkPatternCategory.InterfaceInterference }
  else if (classList.contains('dpForcedAction')) { return DarkPatternCategory.ForcedAction }
  else if (classList.contains('dpSocialEngineering')) { return DarkPatternCategory.SocialEngineering }
  else { return DarkPatternCategory.None }
}

export function getDPCategoriesFromElement(element: Element): DarkPatternCategory[] {
  var result = [];

  const classList = element.classList;
  if (classList.contains('dpObstruction')) { result.push(DarkPatternCategory.Obstruction) }
  if (classList.contains('dpSneaking')) { result.push(DarkPatternCategory.Sneaking) }
  if (classList.contains('dpInterfaceInterference')) { result.push(DarkPatternCategory.InterfaceInterference) }
  if (classList.contains('dpForcedAction')) { result.push(DarkPatternCategory.ForcedAction) }
  if (classList.contains('dpSocialEngineering')) { result.push(DarkPatternCategory.SocialEngineering) }

  return (result.length > 0) ? result : [DarkPatternCategory.None]
}



export function getDarkPatternTitle(category: DarkPatternCategory): string {
  switch (category) {
    case DarkPatternCategory.Obstruction: return "Obstruction"
    case DarkPatternCategory.Sneaking: return "Sneaking"
    case DarkPatternCategory.InterfaceInterference: return "Interface Interference"
    case DarkPatternCategory.ForcedAction: return "Forced Action"
    case DarkPatternCategory.SocialEngineering: return "Social Engineering"
    case DarkPatternCategory.None: return "No Dark Pattern"
    case DarkPatternCategory.Unsure: return "Unsure"
    case DarkPatternCategory.UnsureClassify: return "Unsure"
    default: return "None"
  }
}

export function getDarkPatternTitles(categories: DarkPatternCategory[]): string {
  return categories.map(c => getDarkPatternTitle(c)).join(" and ")
}


export function getDarkPatternClassName(category: DarkPatternCategory): string {
  switch (category) {
    case DarkPatternCategory.Obstruction: return "dpObstruction"
    case DarkPatternCategory.Sneaking: return "dpSneaking"
    case DarkPatternCategory.InterfaceInterference: return "dpInterfaceInterference"
    case DarkPatternCategory.ForcedAction: return "dpForcedAction"
    case DarkPatternCategory.SocialEngineering: return "dpSocialEngineering"
    default: return "dpNone"
  }
}


function getDPCategoryClassNameFromElement(element: Element): string {
  const classList = element.classList;
  if (classList.contains('dpNagging')) { return 'dpForcedAction' }
  else if (classList.contains('dpObstruction')) { return 'dpObstruction' }
  else if (classList.contains('dpSneaking')) { return 'dpSneaking' }
  else if (classList.contains('dpInterfaceInterference')) { return 'dpInterfaceInterference' }
  else if (classList.contains('dpForcedAction')) { return 'dpForcedAction' }
  else if (classList.contains('dpSocialEngineering')) { return 'dpSocialEngineering' }
  else { return 'dpNone' }
}