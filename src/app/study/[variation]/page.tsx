import { PreTestIntro, PreTest, PreTestOutro, PostTestQuestionnaireTutorial, PostTestQuestionnaireGame, PostTestIntro, PostTest, PostTestOutro } from "../pre-post-test";
import { posttestImages, pretestImages } from "../preposttest-data";
import PostTestGameExperienceQuestionnaire from "../questionnaire";
import ProlificUserInfo, { ProlificFinalPage } from "../prolific";


export default function Home({ params }: { params: { variation: string } }) {
  switch (params.variation) {
    case "demographics":
      return (<ProlificUserInfo />)


    case "pretest-intro":
      return (<PreTestIntro />)

    case "pretest-1":
      return (<PreTest testImage={pretestImages[0]} nextURL="/study/pretest-2/" />)

    case "pretest-2":
      return (<PreTest testImage={pretestImages[1]} nextURL="/study/pretest-3/" />)

    case "pretest-3":
      return (<PreTest testImage={pretestImages[2]} nextURL="/study/pretest-4/" />)

    case "pretest-4":
      return (<PreTest testImage={pretestImages[3]} nextURL="/study/pretest-5/" />)

    case "pretest-5":
      return (<PreTest testImage={pretestImages[4]} nextURL="/study/pretest-6/" />)

    case "pretest-6":
      return (<PreTest testImage={pretestImages[5]} nextURL="/study/pretest-7/" />)

    case "pretest-7":
      return (<PreTest testImage={pretestImages[6]} nextURL="/study/pretest-8/" />)

    case "pretest-8":
      return (<PreTest testImage={pretestImages[7]} nextURL="/study/pretest-9/" />)

    case "pretest-9":
      return (<PreTest testImage={pretestImages[8]} nextURL="/study/pretest-10/" />)

    case "pretest-10":
      return (<PreTest testImage={pretestImages[9]} nextURL="/study/pretest-outro/" />)
      
    case "pretest-outro":
      return (<PreTestOutro />)


    case "posttest-game":
      return (<PostTestGameExperienceQuestionnaire />)

    case "posttest-intro":
      return (<PostTestIntro />)

    case "posttest-1":
      return (<PostTest testImage={posttestImages[0]} nextURL="/study/posttest-2/" />)

    case "posttest-2":
      return (<PostTest testImage={posttestImages[1]} nextURL="/study/posttest-3/" />)

    case "posttest-3":
      return (<PostTest testImage={posttestImages[2]} nextURL="/study/posttest-4/" />)

    case "posttest-4":
      return (<PostTest testImage={posttestImages[3]} nextURL="/study/posttest-5/" />)

    case "posttest-5":
      return (<PostTest testImage={posttestImages[4]} nextURL="/study/posttest-6/" />)

    case "posttest-6":
      return (<PostTest testImage={posttestImages[5]} nextURL="/study/posttest-7/" />)

    case "posttest-7":
      return (<PostTest testImage={posttestImages[6]} nextURL="/study/posttest-8/" />)

    case "posttest-8":
      return (<PostTest testImage={posttestImages[7]} nextURL="/study/posttest-9/" />)

    case "posttest-9":
      return (<PostTest testImage={posttestImages[8]} nextURL="/study/posttest-10/" />)

    case "posttest-10":
      return (<PostTest testImage={posttestImages[9]} nextURL="/study/posttest-outro/" />)

    case "posttest-outro":
      return (<ProlificFinalPage />)

    default:
      break;
  }
}

export function generateStaticParams() {
  return [
    { variation: 'demographics' },

    { variation: 'pretest-intro' },
    { variation: 'pretest-1' },
    { variation: 'pretest-2' },
    { variation: 'pretest-3' },
    { variation: 'pretest-4' },
    { variation: 'pretest-5' },
    { variation: 'pretest-6' },
    { variation: 'pretest-7' },
    { variation: 'pretest-8' },
    { variation: 'pretest-9' },
    { variation: 'pretest-10' },
    { variation: 'pretest-outro' },

    { variation: 'posttest-game' },
    { variation: 'posttest-intro' },
    { variation: 'posttest-1' },
    { variation: 'posttest-2' },
    { variation: 'posttest-3' },
    { variation: 'posttest-4' },
    { variation: 'posttest-5' },
    { variation: 'posttest-6' },
    { variation: 'posttest-7' },
    { variation: 'posttest-8' },
    { variation: 'posttest-9' },
    { variation: 'posttest-10' },
    { variation: 'posttest-outro' },
  ]
}