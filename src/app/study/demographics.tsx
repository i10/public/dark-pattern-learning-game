'use client'

import React from 'react';
import './study.css';
import { generateNewUserID, logMessage } from '../logger';
import { Prisma, User } from '@prisma/client';


export default function UserInfo() {

  // create new user in database and redirect to pretest-intro
  function submitAction(formData: FormData) {
    
    const newUserID = generateNewUserID();
    const email = formData.get("email")?.toString() ?? `${newUserID}@example.com`;
    const age = formData.get("age")?.toString() ?? "-";
    const gender = formData.get("gender")?.toString() ?? "-";
    const highestdegree = formData.get("highestdegree")?.toString() ?? "-";
    const occupation = formData.get("occupation")?.toString() ?? "-";
    
    // only create database user in production
    if (process.env.NODE_ENV != 'production') { window.location.href = "/study/pretest-intro/"; return }

    const user: Prisma.UserCreateInput = {
      userID: newUserID,
      email: (email == "") ? `${newUserID}@example.com` : email,
      age: age,
      gender: gender,
      educationBackground: highestdegree,
      occupation: occupation,
    };

    // make api call to create new user.
    fetch('/api/user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
    .then((response) => response.json())
    .then((data) => {
      console.log('Success:', data);
    })
    .then(() => {
      window.location.href = "/study/pretest-intro/";
    })
    .catch((error) => {
      console.error('Error:', error);
    });
  }

  

  return (
    <div className='user-wrapper'>
      <div className='user-wrapper-inner'>

        <h1>Demographics</h1>
        
        <form action={submitAction}>

          <label>
            Age:
            <input type='text' placeholder='20' name='age' required={true} />
          </label>

          <label>
            Gender:
            <input type='text' placeholder='M/F/D' name='gender' required={true} />
          </label>

          <label>
            Current Occupation / Field of Study:
            <input type='text' placeholder='CS, Economics, ...' name='occupation' required={true} />
          </label>

          <label>
            Last achieved academic degree:
            <input type='text' placeholder="High school, Bachelor's, ..." name='highestdegree' required={true} />
          </label>

          <label style={{marginTop: '40px'}}>
            Email address (optional):
            <input type='email' placeholder='your@email.com' name='email' />
          </label>

          <button type="submit">Continue</button>

        </form>
      </div>
    </div>
  )
}