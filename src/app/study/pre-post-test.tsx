'use client'

import { useEffect } from 'react';
import React from 'react';
import './study.css';
import { exportCurrentUserData, logMessage, logMessageWithCompletion } from '../logger';
import { PrePostTestImage, pretestImages } from './preposttest-data';


//===============================================================================================================================
//
// PRE TEST
//
//===============================================================================================================================

export function PreTestIntro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Part 1</h1>
        <br />
        <p>You will now be presented with a series of images of websites.</p>
        <p>Each website will only visible for a short period of time.</p>
        <p>Please look at the website and answer the following question about it afterwards:</p>
        <br />
        <p><i>Was there anything manipulative on the website?</i></p>
        <br />
        <button onClick={() => window.location.href = "/study/pretest-1/"}>Continue</button>
      </div>
    </div>
  )
}



export function PreTest({ testImage, nextURL }: { testImage: PrePostTestImage, nextURL: string }) {

  function submitAction(formData: FormData) {
    logMessageWithCompletion("pretest-" + testImage.id, "Manipulative: " + formData.get(testImage.id + '-radio') + "; Reason: " + (formData.get(testImage.id + '-text') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"), () => {
      window.location.href = nextURL;
    });
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <form action={submitAction}>

          <Test testImage={testImage} />

          <button className='preposttest-button' type="submit" id='submit-btn'>Continue</button>

        </form>
      </div>
    </div>
  )
}



export function PreTestOutro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Part 1 complete</h1>
        <br />
        <p>In part 2, you will learn more about manipulative designs by playing a game.</p>
        <p>Please play all the levels to continue in the study.</p>
        <br />
        <button onClick={() => window.location.href = "/"}>Continue to game</button>
      </div>
    </div>
  )
}




//===============================================================================================================================
//
// POST GAME QUESTIONNAIRES
//
//===============================================================================================================================

export function PostTestQuestionnaireTutorial() {

  function submitAction(formData: FormData) {
    logMessage("posttest-tutorial", "Answers: " + formData.get('post-test-tutorial-1') + ";" 
                                                + formData.get('post-test-tutorial-2') + ";" 
                                                + formData.get('post-test-tutorial-3') + ";");

    logMessage("posttest-tutorial", "F1: " + (formData.get('post-test-tutorial-more-info') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    logMessage("posttest-tutorial", "F2: " + (formData.get('post-test-tutorial-less-info') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    logMessage("posttest-tutorial", "F3: " + (formData.get('post-test-tutorial-comments') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    
    window.location.href = '/study/posttest-game/';
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Post-Test</h1>

        <br />
        <h2>Tutorial</h2>
        
        <br />
        <p>Please answer the following questions on a scale from -2 (totally disagree) to 2 (totally agree):</p>
        

        <form action={submitAction}>

          <table id="price-table" className='noDarkPatternHighlight'>
            <thead>
              <tr>
                <th></th>
                <th>-2<br/><small>totally disagree</small></th>
                <th>-1<br/><small>disagree</small></th>
                <th>0<br/><small>neutral</small></th>
                <th>1<br/><small>agree</small></th>
                <th>2<br/><small>totally agree</small></th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>The tutorial explained everything I needed to play the game.</td>
                <td><input type='radio' name='post-test-tutorial-1' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-1' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-1' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-1' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-1' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The information in the tutorial helped me understand the concepts of dark patterns.</td>
                <td><input type='radio' name='post-test-tutorial-2' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-2' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-2' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-2' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-2' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The amount of information in the tutorial was sufficient.</td>
                <td><input type='radio' name='post-test-tutorial-3' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-3' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-3' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-3' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-tutorial-3' value='2' required={true} /></td>
              </tr>
            </tbody>
          </table>

          <br /><br />
          <p>Where would you have wanted more information?</p>
          <textarea name='post-test-tutorial-more-info' />

          <br /><br />
          <p>Where would you have wanted less information?</p>
          <textarea name='post-test-tutorial-less-info' />

          <br /><br />
          <p>Further comments</p>
          <textarea name='post-test-tutorial-comments' />


          <button type="submit" id='submit-btn'>Continue</button>
        </form>
      </div>
    </div>
  )
}



export function PostTestQuestionnaireGame() {

  function submitAction(formData: FormData) {
    logMessage("posttest-game", "Answers: " + formData.get('post-test-game-1') + ";"
                                            + formData.get('post-test-game-2') + ";"
                                            + formData.get('post-test-game-3') + ";"
                                            + formData.get('post-test-game-4') + ";"
                                            + formData.get('post-test-game-5') + ";"
                                            + formData.get('post-test-game-6') + ";"
                                            + formData.get('post-test-game-7') + ";"
                                            + formData.get('post-test-game-8') + ";"
                                            + formData.get('post-test-game-9') + ";"
                                            + formData.get('post-test-game-10') + ";"
                                            + formData.get('post-test-game-11') + ";"
                                            + formData.get('post-test-game-12') + ";");

    logMessage("posttest-game", "F1: " + (formData.get('post-test-game-strengths') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    logMessage("posttest-game", "F2: " + (formData.get('post-test-game-weaknesses') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    logMessage("posttest-game", "F3: " + (formData.get('post-test-game-improvements') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    logMessage("posttest-game", "F4: " + (formData.get('post-test-game-comments') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    
    window.location.href = '/study/posttest-intro/';
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Post-Test</h1>

        <br />
        <h2>Game</h2>
        
        <br />
        <p>Please answer the following questions on a scale from -2 (totally disagree) to 2 (totally agree):</p>
        

        <form action={submitAction}>

          <table id="price-table" className='noDarkPatternHighlight'>
            <thead>
              <tr>
                <th></th>
                <th>-2<br/><small>totally disagree</small></th>
                <th>-1<br/><small>disagree</small></th>
                <th>0<br/><small>neutral</small></th>
                <th>1<br/><small>agree</small></th>
                <th>2<br/><small>totally agree</small></th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>The game had a clear goal</td>
                <td><input type='radio' name='post-test-game-1' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-1' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-1' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-1' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-1' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The game was intuitive</td>
                <td><input type='radio' name='post-test-game-2' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-2' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-2' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-2' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-2' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The difficulty / challenge was too hard</td>
                <td><input type='radio' name='post-test-game-3' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-3' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-3' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-3' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-3' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>I enjoyed playing the game</td>
                <td><input type='radio' name='post-test-game-4' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-4' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-4' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-4' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-4' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The game was engaging</td>
                <td><input type='radio' name='post-test-game-5' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-5' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-5' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-5' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-5' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The tasks were varying / interesting</td>
                <td><input type='radio' name='post-test-game-6' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-6' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-6' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-6' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-6' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>I was frustrated whilst playing the game</td>
                <td><input type='radio' name='post-test-game-7' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-7' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-7' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-7' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-7' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>I was frustrated after completing the game</td>
                <td><input type='radio' name='post-test-game-8' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-8' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-8' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-8' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-8' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>The game is suitable to learn about dark patterns</td>
                <td><input type='radio' name='post-test-game-9' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-9' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-9' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-9' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-9' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>Playing the game increased my knowledge about dark patterns</td>
                <td><input type='radio' name='post-test-game-10' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-10' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-10' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-10' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-10' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>Playing the game increased my confidence in detecting dark patterns</td>
                <td><input type='radio' name='post-test-game-11' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-11' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-11' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-11' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-11' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>Playing the game increased my confidence in handling dark patterns</td>
                <td><input type='radio' name='post-test-game-12' value='-2' required={true} /></td>
                <td><input type='radio' name='post-test-game-12' value='-1' required={true} /></td>
                <td><input type='radio' name='post-test-game-12' value='0' required={true} /></td>
                <td><input type='radio' name='post-test-game-12' value='1' required={true} /></td>
                <td><input type='radio' name='post-test-game-12' value='2' required={true} /></td>
              </tr>
            </tbody>
          </table>

          <br /><br />
          <p>What are the strengths of this game?</p>
          <textarea name='post-test-game-strengths' />

          <br /><br />
          <p>What are the weaknesses of this game?</p>
          <textarea name='post-test-game-weaknesses' />

          <br /><br />
          <p>Suggestions for improvements</p>
          <textarea name='post-test-game-improvements' />

          <br /><br />
          <p>Further comments</p>
          <textarea name='post-test-game-comments' />


          <button type="submit" id='submit-btn'>Continue</button>
        </form>
      </div>
    </div>
  )
}




//===============================================================================================================================
//
// POST TEST
//
//===============================================================================================================================

export function PostTestIntro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Part 3</h1>
        
        <br />
        <p>Just like at the beginning of the study, you will now be presented with a series of images of websites.</p>
        <p>Each website will only visible for a short period of time.</p>
        <p>Please look at the website and answer the following question about it afterwards:</p>
        <br />
        <p><i>Was there anything manipulative on the website?</i></p>
        <br />
        <button onClick={() => window.location.href = "/study/posttest-1/"}>Continue</button>
      </div>
    </div>
  )
}



export function PostTest({ testImage, nextURL }: { testImage: PrePostTestImage, nextURL: string }) {

  function submitAction(formData: FormData) {
    logMessageWithCompletion("posttest-" + testImage.id, "Manipulative: " + formData.get(testImage.id + '-radio') + "; Reason: " + (formData.get(testImage.id + '-text') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"), () => {
      window.location.href = nextURL;
    });
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <form action={submitAction}>

          <Test testImage={testImage} />

          <button className='preposttest-button' type="submit" id='submit-btn'>Continue</button>

        </form>
      </div>
    </div>
  )
}



export function PostTestOutro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Thank you for your participation in the study!</h1>
        
        <br /><br /><br /><br />
        <button onClick={() => exportCurrentUserData()} style={{marginRight: '20px'}}>Export Study Data</button>
        <button onClick={() => window.location.href = "/"}>Home</button>
      </div>
    </div>
  )
}




//===============================================================================================================================
//
// TEST HELPER VIEW
//
//===============================================================================================================================

function Test({ testImage }: { testImage: PrePostTestImage }) {

  useEffect(() => {
    let timer: NodeJS.Timeout;
    timer = setTimeout(() => updateUI(), testImage.time * 1000); 
    return () => { if (timer) {  clearTimeout(timer); } };
  }, [testImage.time]);


  // disable context menu to "prevent" cheating
  useEffect(() => {
    const preventContextMenu = (e: Event) => {
      e.preventDefault();
    };

    document.addEventListener('contextmenu', preventContextMenu);
    return () => { document.removeEventListener('contextmenu', preventContextMenu); }; 
  }, []);


  function updateUI() {
    document.getElementById('study-test-user-input-' + testImage.id)?.classList.add('visible');
    document.getElementById('submit-btn')?.classList.add('visible');
    document.getElementById('study-test-image-' + testImage.id)?.classList.add('small');
  }

  return (
    <div className='study-test-container'>
      <img src={'/study_assets/' + testImage.filename} id={'study-test-image-' + testImage.id} />
      
      <div className='study-test-userinput' id={'study-test-user-input-' + testImage.id}>
        <h1>Regarding the image you just saw:</h1>

        <br />

        <h3>Was there anything manipulative on the website?</h3>
        <label>
          <input type='radio' name={testImage.id + '-radio'} value='yes' required={true} />Yes
        </label>
        
        <label>
          <input type='radio' name={testImage.id + '-radio'} value='no' required={true} />No
        </label>

        <h3><b>If yes</b>, please name the element and <b>briefly</b> explain why it is manipulative.</h3>
        <textarea name={testImage.id + '-text'} />
      </div>
    </div>
  )
}