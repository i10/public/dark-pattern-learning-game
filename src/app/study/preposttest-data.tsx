export type PrePostTestImage = {
  filename: string;
  id: string;
  time: number;
};

export const pretestImages: PrePostTestImage[] = [
  {
    filename: '1.png',
    id: 'pre_test_1',
    time: 20
  },
  {
    filename: '2.png',
    id: 'pre_test_2',
    time: 20
  },
  {
    filename: '3.png',
    id: 'pre_test_3',
    time: 20
  },
  {
    filename: '4.png',
    id: 'pre_test_4',
    time: 20
  },
  {
    filename: '5.png',
    id: 'pre_test_5',
    time: 20
  },
  {
    filename: '6.png',
    id: 'pre_test_6',
    time: 20
  },
  {
    filename: '7.png',
    id: 'pre_test_7',
    time: 20
  },
  {
    filename: '8.png',
    id: 'pre_test_8',
    time: 20
  },
  {
    filename: '9.png',
    id: 'pre_test_9',
    time: 20
  },
  {
    filename: '10.png',
    id: 'pre_test_10',
    time: 20
  },
]



export const posttestImages: PrePostTestImage[] = [
  {
    filename: '4.png',
    id: 'post_test_4',
    time: 20
  },
  {
    filename: '10.png',
    id: 'post_test_10',
    time: 20
  },
  {
    filename: '2.png',
    id: 'post_test_2',
    time: 20
  },
  {
    filename: '5.png',
    id: 'post_test_5',
    time: 20
  },
  {
    filename: '3.png',
    id: 'post_test_3',
    time: 20
  },
  {
    filename: '6.png',
    id: 'post_test_6',
    time: 20
  },
  {
    filename: '9.png',
    id: 'post_test_9',
    time: 20
  },
  {
    filename: '7.png',
    id: 'post_test_7',
    time: 20
  },
  {
    filename: '1.png',
    id: 'post_test_1',
    time: 20
  },
  {
    filename: '8.png',
    id: 'post_test_8',
    time: 20
  },
]