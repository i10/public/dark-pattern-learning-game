'use client'

import React from 'react';
import './study.css';
import { generateNewUserID, logMessage } from '../logger';
import { Prisma, User } from '@prisma/client';


export default function ProlificUserInfo() {

  // create new user in database and redirect to pretest-intro
  function submitAction(formData: FormData) {
    
    const newUserID = formData.get("prolificid")?.toString() ?? generateNewUserID();
    sessionStorage.setItem('user_id', newUserID);

    const email = `${newUserID}@example.com`;
    const age = formData.get("age")?.toString() ?? "-";
    const gender = formData.get("gender")?.toString() ?? "-";
    const highestdegree = formData.get("highestdegree")?.toString() ?? "-";
    const occupation = formData.get("occupation")?.toString() ?? "-";
    
    // only create database user in production
    if (process.env.NODE_ENV != 'production') { window.location.href = "/study/pretest-intro/"; return }

    const user: Prisma.UserCreateInput = {
      userID: newUserID,
      email: (email == "") ? `${newUserID}@example.com` : email,
      age: age,
      gender: gender,
      educationBackground: highestdegree,
      occupation: occupation,
    };
    
    console.log(user);

    // make api call to create new user.
    fetch('/api/user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
    .then((response) => response.json())
    .then((data) => {
      console.log('Success:', data);
    })
    .then(() => {
      window.location.href = "/study/pretest-intro/";
    })
    .catch((error) => {
      console.error('Error:', error);

      // check if user already exists:
      fetch(`/api/user/${newUserID}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then((response) => response.json())
      .then((data) => {
        console.log('Success:', data);
      })
      .then(() => {
        window.location.href = "/study/pretest-intro/";
      })
    });
  }

  

  return (
    <div className='user-wrapper'>
      <div className='user-wrapper-inner'>

        <h1>Demographics</h1>

        <br />
        
        <p>Welcome to this Prolific study about manipulative designs on websites.</p>
        <p>This study consists of three parts.</p>
        <p>To get started, please fill out the following form with some information about you.</p>

        <br />
        
        <form action={submitAction}>

        <label>
            Prolific ID:
            <input type='text' placeholder='######' name='prolificid' required={true} />
          </label>

          <label>
            Age:
            <input type='text' placeholder='20' name='age' required={true} />
          </label>

          <label>
            Gender:
            <input type='text' placeholder='M/F/D' name='gender' required={true} />
          </label>

          <label>
            Current Occupation / Field of Study:
            <input type='text' placeholder='CS, Economics, ...' name='occupation' required={true} />
          </label>

          <label>
            Last achieved academic degree:
            <input type='text' placeholder="High school, Bachelor's, ..." name='highestdegree' required={true} />
          </label>

          <br /><br />

          <button type="submit">Continue</button>

        </form>
      </div>
    </div>
  )
}


export function ProlificFinalPage() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Thank you for your participation in our study!</h1>
        
        <br />
        <p>Click <a href='https://app.prolific.com/submissions/complete?cc=C1BN0SEA' style={{cursor: 'pointer'}}><u>here</u></a> to complete the study or enter the following code in Prolific:</p>
        <br />
        <p><i>C1BN0SEA</i></p>
      </div>
    </div>
  )
}