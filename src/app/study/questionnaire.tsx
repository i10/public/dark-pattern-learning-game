'use client'

import React from 'react';
import './study.css';
import { logMessage, logMessageWithCompletion } from '../logger';
import { shuffleArray } from '../utils';



type GEQQuestion = {
  question: string;
  id: string;
};


const geqQuestions: GEQQuestion[] = [
  {
    question: 'I was interested in the game\'s story',
    id: 'geq-1'
  },
  {
    question: 'It was aesthetically pleasing',
    id: 'geq-2'
  },
  {
    question: 'It felt like a rich experience',
    id: 'geq-3'
  },
  {
    question: 'I found it impressive',
    id: 'geq-4'
  },
  {
    question: 'I was fully occupied with the game',
    id: 'geq-5'
  },
  {
    question: 'I forgot everything around me',
    id: 'geq-6'
  },
  {
    question: 'I lost track of time',
    id: 'geq-7'
  },
  {
    question: 'I was good at it',
    id: 'geq-8'
  },
  {
    question: 'I felt successful',
    id: 'geq-9'
  },
  {
    question: 'I was fast at reaching the game\'s targets',
    id: 'geq-10'
  },
  {
    question: 'I felt tense',
    id: 'geq-11'
  },
  {
    question: 'I felt restless',
    id: 'geq-12'
  },
  {
    question: 'I felt frustrated',
    id: 'geq-13'
  },
  {
    question: 'I felt that I was learning',
    id: 'geq-14'
  },
  {
    question: 'I felt challenged',
    id: 'geq-15'
  },
  {
    question: 'I had to put a lot of effort into it',
    id: 'geq-16'
  },
  {
    question: 'I thought it was cognitive demanding',
    id: 'geq-17'
  },
  {
    question: 'I felt good',
    id: 'geq-18'
  },
  {
    question: 'I enjoyed it',
    id: 'geq-19'
  },
  {
    question: 'I thought it was fun',
    id: 'geq-20'
  },
  {
    question: 'I thought about other things',
    id: 'geq-21'
  },
  {
    question: 'I found it tiresome',
    id: 'geq-22'
  },
  {
    question: 'I felt bored',
    id: 'geq-23'
  },
]




//===============================================================================================================================
//
// POST GAME QUESTIONNAIRE
//
//===============================================================================================================================



export default function PostTestGameExperienceQuestionnaire() {

  const qeustionList = shuffleArray(geqQuestions).map((val, _) => {
    return (
      <tr>
        <td suppressHydrationWarning>{val.question}</td>
        <td><input type='radio' name={val.id} value='0' required={true} /></td>
        <td><input type='radio' name={val.id} value='1' required={true} /></td>
        <td><input type='radio' name={val.id} value='2' required={true} /></td>
        <td><input type='radio' name={val.id} value='3' required={true} /></td>
        <td><input type='radio' name={val.id} value='4' required={true} /></td>
      </tr>
    )
  });

  function submitAction(formData: FormData) {
    const answers = geqQuestions.map((val) => {
      return formData.get(val.id);
    }).join(';');

    logMessageWithCompletion("posttest-geq", answers, 
      () => { 
        window.location.href = '/study/posttest-intro/'; 
    });
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Part 3</h1>

        <br />
        <h2>Game Experience</h2>
        
        <br />
        <p>Please indicate how you felt while playing the game for each of the items:</p>
        

        <form action={submitAction}>

          <table>
            <thead>
              <tr>
                <th></th>
                <th><small>Not at all</small></th>
                <th><small>Slightly</small></th>
                <th><small>Moderately</small></th>
                <th><small>Fairly</small></th>
                <th><small>Extremely</small></th>
              </tr>
            </thead>

            <tbody>
              {qeustionList}
            </tbody>
          </table>

          <button type="submit" id='submit-btn'>Continue</button>
        </form>
      </div>
    </div>
  )
}