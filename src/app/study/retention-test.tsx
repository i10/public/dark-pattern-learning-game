'use client'

import { useEffect } from 'react';
import React from 'react';
import { Suspense } from 'react'
import './study.css';
import { generateNewUserID, logMessage } from '../logger';
import { PrePostTestImage, pretestImages } from './preposttest-data';
import { useSearchParams } from 'next/navigation';
import { storeBool } from '../utils';


//===============================================================================================================================
//
// RETENTION TEST
//
//===============================================================================================================================

// loads the user id from the URL parameter or asks the user to enter it manually
export function RetentionTestWelcome() {
  return (
    <Suspense>
      <RetentionTestWelcomeInternal />
    </Suspense>
  )
}

function RetentionTestWelcomeInternal() {

  const searchParams = useSearchParams();
  const userID = searchParams.get('userID');

  useEffect(() => {
    if (userID) {
      console.log(userID);
      sessionStorage.setItem('user_id', userID);
      window.location.href = "/study/retentiontest-questionnaire/";
    }
  }, []);

  function submitAction(formData: FormData) {
    const manualUserID = formData.get("manual-user-id")?.toString() ?? generateNewUserID();

    console.log(manualUserID);
    sessionStorage.setItem('user_id', manualUserID);

    window.location.href = "/study/retentiontest-questionnaire/";
  }


  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        { !userID && <>
          <h1>Follow-up Study: A Dark Pattern Learning Game</h1>
          <br />
          <p>Please enter the <b>ID</b> that we send you with the email.</p>
          <form action={submitAction}>

            <label>
              ID:
              <input type='text' placeholder='user_XXXXXXXXXXXXXXXXXXXXXX' name='manual-user-id' required={true} />
            </label>
            <br />

            <button type="submit">Continue</button>
          </form>
        </> }
      </div>
    </div>
  )
}


export function RetentionTestQuestionnaire() {

  function submitAction(formData: FormData) {
    // if (Number(formData.get("timeonline")) == -1) { return }
    
    logMessage("retentiontest", "Talk: " + formData.get("retentiontest-talk-dp") + ";"
                                         + formData.get("retentiontest-talk-game") + ";"
                                         + formData.get("retentiontest-talk-learn") + ";");

    logMessage("retentiontest", "Answers: " + formData.get("retentiontest-1") + ";"
                                            + formData.get("retentiontest-2") + ";"
                                            + formData.get("retentiontest-3") + ";"
                                            + formData.get("retentiontest-4") + ";"
                                            + formData.get("retentiontest-5") + ";");

    logMessage("retentiontest", "Change Behavior: " + formData.get("retentiontest-change-behavior"));
    logMessage("retentiontest", "Changes: " + (formData.get('retentiontest-changes') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));

    logMessage("retentiontest", "Comments: " + (formData.get('retentiontest-further-comments') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));

    window.location.href = "/study/retentiontest-intro/";
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Follow-up Study: A Dark Pattern Learning Game</h1>
        <br />
        <h3><b>Thank you for taking the time to participate in this study.</b></h3>
        <br />
        
        <form action={submitAction} className='retention-test-questionnaire'>

          <p>Please answer the following questions:</p>

          <label>
            Did you to talk to other people about Dark Patterns since participating in the study?:
            <br />
            <input type='radio' name='retentiontest-talk-dp' value='0' required={true} /> No
            <br />
            <input type='radio' name='retentiontest-talk-dp' value='1' required={true} /> Yes
          </label>

          <br />

          <label>
          Did you talk to other people about the game?:
            <br />
            <input type='radio' name='retentiontest-talk-game' value='0' required={true} /> No
            <br />
            <input type='radio' name='retentiontest-talk-game' value='1' required={true} /> Yes
          </label>

          <br />

          <label>
          Did you talk to other people about what you have learned in the study?:
            <br />
            <input type='radio' name='retentiontest-talk-learn' value='0' required={true} /> No
            <br />
            <input type='radio' name='retentiontest-talk-learn' value='1' required={true} /> Yes
          </label>

          <br /><br /><br />

          <p>Please answer the following questions on a scale from -2 (totally disagree) to 2 (totally agree):</p>
          <table>
            <thead>
              <tr>
                <th></th>
                <th>-2<br/><small>totally disagree</small></th>
                <th>-1<br/><small>disagree</small></th>
                <th>0<br/><small>neutral</small></th>
                <th>1<br/><small>agree</small></th>
                <th>2<br/><small>totally agree</small></th>
              </tr>
            </thead>

            <tbody>
                 <tr>
                <td>I have been more aware of dark patterns since participating in the study.</td>
                <td><input type='radio' name='retentiontest-1' value='-2' required={true} /></td>
                <td><input type='radio' name='retentiontest-1' value='-1' required={true} /></td>
                <td><input type='radio' name='retentiontest-1' value='0' required={true} /></td>
                <td><input type='radio' name='retentiontest-1' value='1' required={true} /></td>
                <td><input type='radio' name='retentiontest-1' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>I have noticed more dark patterns while browsing since participating in the study.</td>
                <td><input type='radio' name='retentiontest-2' value='-2' required={true} /></td>
                <td><input type='radio' name='retentiontest-2' value='-1' required={true} /></td>
                <td><input type='radio' name='retentiontest-2' value='0' required={true} /></td>
                <td><input type='radio' name='retentiontest-2' value='1' required={true} /></td>
                <td><input type='radio' name='retentiontest-2' value='2' required={true} /></td>
              </tr> 
              
              <tr>
                <td> I have been more careful while browsing since participating in the study.</td>
                <td><input type='radio' name='retentiontest-3' value='-2' required={true} /></td>
                <td><input type='radio' name='retentiontest-3' value='-1' required={true} /></td>
                <td><input type='radio' name='retentiontest-3' value='0' required={true} /></td>
                <td><input type='radio' name='retentiontest-3' value='1' required={true} /></td>
                <td><input type='radio' name='retentiontest-3' value='2' required={true} /></td>
              </tr> 
              

              <tr>
                <td>I have been more frustrated by websites since participating in the study.</td>
                <td><input type='radio' name='retentiontest-4' value='-2' required={true} /></td>
                <td><input type='radio' name='retentiontest-4' value='-1' required={true} /></td>
                <td><input type='radio' name='retentiontest-4' value='0' required={true} /></td>
                <td><input type='radio' name='retentiontest-4' value='1' required={true} /></td>
                <td><input type='radio' name='retentiontest-4' value='2' required={true} /></td>
              </tr>

              <tr>
                <td>I feel joy when detecting a dark pattern since participating in the study.</td>
                <td><input type='radio' name='retentiontest-5' value='-2' required={true} /></td>
                <td><input type='radio' name='retentiontest-5' value='-1' required={true} /></td>
                <td><input type='radio' name='retentiontest-5' value='0' required={true} /></td>
                <td><input type='radio' name='retentiontest-5' value='1' required={true} /></td>
                <td><input type='radio' name='retentiontest-5' value='2' required={true} /></td>
              </tr>

            </tbody>
          </table>

          <br /><br /><br />

          <label>
            Did you change your behavior when interacting with websites since participating in the study?
            <br />
            <input type='radio' name='retentiontest-change-behavior' value='0' required={true} /> No
            <br />
            <input type='radio' name='retentiontest-change-behavior' value='1' required={true} /> Yes
          </label>

          <br />

          <p> If so, how?</p>
          <textarea name='retentiontest-changes' />

          <br /><br /><br />

          <p>Any further remarks?</p>
          <textarea name='retentiontest-further-comments' />

          <button type="submit">Continue</button>
        </form>
      </div>
    </div>
  )
}



// welcome view for the retention test
export function RetentionTestIntro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Thank you for your feedback.</h1>
        <br />
        <p>You will now be presented with a series of images of websites.</p>
        <p>Each website is only visible for a short period of time.</p>
        <p>Please look at the website and answer a question about it afterwards.</p>
        <br />
        <button onClick={() => window.location.href = "/study/retentiontest-1/"}>Continue</button>
      </div>
    </div>
  )
}


export function RetentionTest({ testImage, nextURL }: { testImage: PrePostTestImage, nextURL: string }) {

  function submitAction(formData: FormData) {
    logMessage("pretest-" + testImage.id, "Manipulative: " + formData.get(testImage.id + '-radio') + "; Reason: " + (formData.get(testImage.id + '-text') ?? '').toString().replaceAll(",", ";").replaceAll("\n", ";").replaceAll("\r", ";"));
    
    window.location.href = nextURL;
  }

  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <form action={submitAction}>

          <Test testImage={testImage} />

          <button className='preposttest-button' type="submit" id='submit-btn'>Continue</button>

        </form>
      </div>
    </div>
  )
}



export function RetentionTestOutro() {
  return (
    <div className='user-wrapper'>
      <div className='user-study-wrapper'>

        <h1>Thank you.</h1>
        <br />
        <p>You will now continue to the game.</p>
        <br />
        <button onClick={() => { storeBool("tutorial_completed", true); window.location.href = "/" }}>Continue to game</button>
      </div>
    </div>
  )
}






//===============================================================================================================================
//
// TEST HELPER VIEW
//
//===============================================================================================================================

function Test({ testImage }: { testImage: PrePostTestImage }) {

  useEffect(() => {
    let timer: NodeJS.Timeout;
    timer = setTimeout(() => updateUI(), testImage.time * 1000); 
    return () => { if (timer) {  clearTimeout(timer); } };
  }, [testImage.time]);


  // disable context menu to "prevent" cheating
  useEffect(() => {
    const preventContextMenu = (e: Event) => {
      e.preventDefault();
    };

    document.addEventListener('contextmenu', preventContextMenu);
    return () => { document.removeEventListener('contextmenu', preventContextMenu); }; 
  }, []);


  function updateUI() {
    document.getElementById('study-test-user-input-' + testImage.id)?.classList.add('visible');
    document.getElementById('submit-btn')?.classList.add('visible');
    document.getElementById('study-test-image-' + testImage.id)?.classList.add('small');
  }

  return (
    <div className='study-test-container'>
      <img src={'/study_assets/' + testImage.filename} id={'study-test-image-' + testImage.id} />
      
      <div className='study-test-userinput' id={'study-test-user-input-' + testImage.id}>
        <h3>Was there anything manipulative on the website?</h3>
        <label>
          <input type='radio' name={testImage.id + '-radio'} value='yes' required={true} />Yes
        </label>
        <label>
          <input type='radio' name={testImage.id + '-radio'} value='no' required={true} />No
        </label>

        <h3><b>If yes</b>, please name the element and <b>briefly</b> explain why it is manipulative.</h3>
        <textarea name={testImage.id + '-text'} />
      </div>
    </div>
  )
}