import TutorialIntro from '../tutorial-intro';
import TutorialClassifyGame, { ClassifyStoryIntro } from './../tutorial-classify';

export default function Home({ params }: { params: { variation: string } }) {
  switch (params.variation) {
    case "19424c90-efea-4ed0-a741-ad833b104d72":
      return (<TutorialIntro />)

    case "8e1f3930-77b5-41bd-8b31-447748325bb3":
      return (<ClassifyStoryIntro levelID={params.variation} />)

    case "b7806289-c668-4482-8c9c-5192802a2ff2":
      return (<TutorialClassifyGame levelID="8e1f3930-77b5-41bd-8b31-447748325bb3" />) // use the hard coded level id here!

    default:
      break;
  }
}

export function generateStaticParams() {
  return [
    { variation: '19424c90-efea-4ed0-a741-ad833b104d72' },
    { variation: '8e1f3930-77b5-41bd-8b31-447748325bb3' },
    { variation: 'b7806289-c668-4482-8c9c-5192802a2ff2' }
  ]
}