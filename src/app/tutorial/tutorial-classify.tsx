'use client'

import { useState, useEffect, useCallback } from 'react';
import React from 'react';
import '../globals.css';
import '../game-ui.css';
import './tutorial.css';
import '../cookiebanner.css';
import { DarkPatternCategory, applyClassifyScoringHighlight, getClassifyScoringResult, getDarkPatternTitle, getDarkPatternClassName } from '../scoring';
import { TimerView, PageControl, CategoryButtonInfo } from '../game-ui';
import * as explanations from '../dark-pattern-data';
import { applyLevelResult, setCareerValue, setHighscore, storeBool } from '../utils';
import { logMessage } from '../logger';
import { ClassifyTutorialElement, tutorialElements } from './tutorial-data';




//===============================================================================================================================
//
// STORY INTRO
//
//===============================================================================================================================

export function ClassifyStoryIntro({ levelID }: { levelID: string }) {
  const [currentStage, setCurrentStage] = useState(0);

  const [choice1, setChoice1] = useState(0);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    timer = setTimeout(() => setCurrentStage(s => s + 1), 500);

    const currentMessage = document.getElementById("msg-" + currentStage)
    if (currentMessage) {
      currentMessage.classList.remove("chat-msg-hidden");
      currentMessage.classList.add("chat-msg-visible");

      currentMessage.scrollIntoView();

      if (currentMessage.classList.contains('chat-p2')) {
        clearTimeout(timer);
      }
    }
    
    return () => {
      if (timer) { clearTimeout(timer); }
    };
  }, [currentStage]);


  const msgIntroChoice = (n: number) => {
    const otherButton = document.getElementById('btn-msg-intro-' + (n%2))
    if (otherButton) { otherButton.classList.add('chat-msg-hidden') }

    setChoice1(n);
    setCurrentStage(s => s + 1)
  };

  const continueAction = () => {
    setCurrentStage(s => s + 1)
  };

  
  function TypingDots() {
    return (
      <div className='chat-p1 typing-dots'><span></span><span></span><span></span></div>
    );
  }

  function Message({ typingStart, messageStart, currentStage, message }: { typingStart: number, messageStart: number, currentStage: number, message: React.ReactNode }) {
    if (currentStage < typingStart) {
      return <></>
    } else if ((currentStage >= typingStart) && (currentStage < messageStart)) {
      return <TypingDots />
    } else {
      return message
    }
  }


  const button1Choice1Class = (choice1 == 2) ? 'p2-button chat-msg-hidden' : 'p2-button'
  const button2Choice1Class = (choice1 == 1) ? 'p2-button chat-msg-hidden' : 'p2-button'


  return (
    <div className='main-wrapper'>
      <header className='main-header'>
        <div className='main-header-wrapper'> 
          <div className='header-logo' onClick={() => { storeBool(levelID + "-completed", true);; window.location.href = '/'; }}>
            <h1>D P D F</h1>
            <h3>Dark Pattern Defense Force</h3>
          </div>
        </div>
      </header>

      <main className='game-ui-main tutorial-intro-main' id='tutorial-intro-main'>
    
        <div className='story-chat-container'>

          <Message typingStart={0} messageStart={4} currentStage={currentStage} message={
            <div className='chat-p1' id='msg-2'>Ah, you're back!<br/>That wasn't too hard, was it?</div>
          } />

          <Message typingStart={5} messageStart={5} currentStage={currentStage} message={
            <div className='chat-p2' id='msg-5'>
              <button id='btn-msg-intro-0' className={button1Choice1Class} onClick={() => msgIntroChoice(1)}>It was easy!</button>
              <button id='btn-msg-intro-1' className={button2Choice1Class} onClick={() => msgIntroChoice(2)}>That was a lot to take in!</button>
            </div>
          } />

          <Message typingStart={6} messageStart={10} currentStage={currentStage} message={
            <div className='chat-p1' id='msg-10'>{(choice1 == 2) ? <>Well, tough luck! That was just the beginning!<br/>But don't worry, we added some tooltips to the category buttons that should help you remember the most important facts about each dark pattern.</> : <>Let's see if you still think so in a couple of minutes.<br/>Just as a precaution, we added some tooltips to the category buttons that should help you remember the most important facts about each dark pattern.</>}<br /></div>
          } />

          <Message typingStart={11} messageStart={11} currentStage={currentStage} message={
            <div className='chat-p2' id='msg-11'>
              <button className='p2-button' id='btn-msg-continue' onClick={continueAction}>That's very nice of you!</button>
            </div>  
          } /> 

          <Message typingStart={12} messageStart={16} currentStage={currentStage} message={
            <div className='chat-p1' id='msg-16'>You're welcome.<br/>Now it's time for you to prove your knowledge!</div>
          } />

          <Message typingStart={17} messageStart={21} currentStage={currentStage} message={
            <div className='chat-p1' id='msg-21'>Just forget everything you have just learned about our Dark Pattern Magic Wand&trade;.<br/>This is just rapid fire questions. We show you a couple of dark patterns and all you have to do is guess... uhm, I mean <i>»determine«</i> the correct category.</div>
          } />

          <Message typingStart={22} messageStart={23} currentStage={currentStage} message={
            <div className='chat-p1' id='msg-23'>Good Luck!</div>
          } />


          <Message typingStart={24} messageStart={24} currentStage={currentStage} message={
            <div className='chat-p2' id='msg-24'>
              <button className='p2-button' id='btn-msg-continue' onClick={() => window.location.href = "/tutorial/b7806289-c668-4482-8c9c-5192802a2ff2"}>Bring it on! I'm ready!</button>
            </div>  
          } /> 
        </div>
      </main>
    </div>
  )
}



//===============================================================================================================================
//
// GAME WRAPPER
//
//===============================================================================================================================

export default function TutorialClassifyGame({ levelID }: { levelID: string }) {
  const children = tutorialElements.map((element, index) => {
    return <ClassifyGameWrapper tutorialElement={element} index={index} key={index} />
  })

  return (
    <ClassifyGameUI children={children} levelID={levelID} />
  )
}




//===============================================================================================================================
//
// CLASSIFY GAME UI (modified main game ui)
//
//===============================================================================================================================

export function ClassifyGameUI({
  children,
  levelID
}: {
  children: React.ReactNode[],
  levelID: string
}) {
  const [currentPage, setCurrentPage] = useState(0);
  const [pageCount, setPageCount] = useState(1); // this will update ones the player uncovers additional pages

  const [isFinished, setIsFinished] = useState(false);
  const [currentScore, setCurrentScore] = useState(0);

  const [results, setResults] = useState<string[]>([]);


  //============================================================================
  //
  // LAUNCH TASKS
  //
  //============================================================================

  // Ask before leaving the page (if not finished)
  useEffect(() => {
    const handleWindowClose = (e: BeforeUnloadEvent) => {
      if (!isFinished) {
        e.preventDefault();
        return (e.returnValue = 'Are you sure you want to leave? Any progress will be lost!');
      }
    };

    window.addEventListener('beforeunload', handleWindowClose);
    return () => { window.removeEventListener('beforeunload', handleWindowClose); }; 
  }, [isFinished]);

  
  // disable context menu to "prevent" cheating
  useEffect(() => {
    const preventContextMenu = (e: Event) => {
      e.preventDefault();
    };

    document.addEventListener('contextmenu', preventContextMenu);
    return () => { window.removeEventListener('contextmenu', preventContextMenu); }; 
  }, [isFinished]);



  //============================================================================
  //
  // DARK PATTERN CATEGORIES BUTTONS
  //
  //============================================================================

  function handleDarkPatternCategorization(category: number) {
    const selectedElements = document.getElementsByClassName('dpClassifyContainerSelected-' + currentPage);
    if (selectedElements.length <= 0) { return /* only allow tagging once */ }
    const selectedElement = selectedElements[0];

    const selectedCategory: DarkPatternCategory = category

    handleDarkPatternScoring(selectedElement, selectedCategory)
  }


  function handleDarkPatternScoring(element: Element, selectedCategory: DarkPatternCategory) {
    
    // get the scoring result
    const scoreResult = getClassifyScoringResult(element, selectedCategory)
    if (!scoreResult) {  console.log(element); return }

    logMessage(levelID, "Categorized: " + getDarkPatternTitle(selectedCategory) + "; Score change: " +  JSON.stringify(scoreResult).replaceAll(",", ";"));

    // highlight the element accordingly
    applyClassifyScoringHighlight(element, scoreResult)

    // update the score
    setCurrentScore(sc => sc + scoreResult.value)

    const selectedElements = document.getElementsByClassName('dpClassifyContainerSelected-' + currentPage);
    if (selectedElements.length > 0) {
      const selectedElement = selectedElements[0];
      selectedElement.classList.remove('dpClassifyContainerSelected-' + currentPage)
    }

    document.getElementById('tutorial-classify-caption2-' + currentPage)!.innerHTML = "You selected: <b>" + getDarkPatternTitle(selectedCategory) + "</b>";
    document.getElementById('tutorial-classify-caption2-' + currentPage)?.classList.add('visible');
    if (scoreResult.value >= 0) {
      document.getElementById('tutorial-classify-caption3-' + currentPage)!.innerHTML = "That is <b>correct</b>."
    } else if (scoreResult.value >= -50) {
      document.getElementById('tutorial-classify-caption3-' + currentPage)!.innerHTML = ""
    }
    document.getElementById('tutorial-classify-caption-' + currentPage)?.classList.add('visible');
    document.getElementById('tutorial-classify-explanation-' + currentPage)?.classList.add('visible');
    document.getElementById('tutorial-classify-btn-' + currentPage)?.classList.add('visible');

    unlockPage(pageCount + 1);

    // finally update the pager
    setResults([...results, scoreResult.newClassName]);
  }


  function hideFinalResultOverlay() {
    // update the global highscore for this level
    setCareerValue(levelID, currentScore)
    setHighscore(levelID, currentScore);
  }


  //============================================================================
  //
  // FINISH ACTION
  //
  //============================================================================

  function handleFinishAction() {
    if (isFinished) {
      // Take the user back to home
      applyLevelResult(levelID, currentScore);
      storeBool(levelID + "-completed", true);
      storeBool("tutorial_completed", true);
      window.location.href = '/';
    }
  }


  //============================================================================
  //
  // KEYBOARD INPUT
  //
  //============================================================================


  const handleKeyPress = useCallback((event: KeyboardEvent) => {
    handleKeyboardEvent(event);
  }, [ // register the states that should be available for keyboard input
    pageCount,
    handleKeyboardEvent
  ]);

  useEffect(() => {
    window.addEventListener('keydown', handleKeyPress);
    return () => { window.removeEventListener('keydown', handleKeyPress); };
  }, [handleKeyPress]);

  function handleKeyboardEvent(e: KeyboardEvent) {
    switch (e.key) {
      case "n": nextPage(); e.preventDefault(); break;
      case "p": previousPage(); e.preventDefault(); break;
      case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': handleNumberInput(Number(e.key) - 1); e.preventDefault(); break;
      default: break;
    }
  }

  const nextPage = () => {
    setCurrentPage(c => {
      return ((c < children.length - 1) && (c < pageCount - 1)) ? c + 1 : c;
    });
  };

  const previousPage = () => {
    setCurrentPage(c => {
      return (c > 0) ? c - 1 : c;
    });
  };

  const handleNumberInput = (n: number) => {
    if (n < pageCount) {
      setCurrentPage(n);
    }
  };



  //============================================================================
  //
  // MULTI PAGE SUPPORT
  //
  //============================================================================

  function handlePageChange(page: number) {
    setCurrentPage(page);
  }

  useEffect(() => {
    let children = document.getElementById('classifyChildContainer')?.children
    if (children) {
      for (let i = 0; i < children.length; i++) {
        const child = children[i];
        if (i == currentPage) {
          child.classList.remove('game-ui-inactive-page');
          child.classList.add('game-ui-active-page');
        } else {
          child.classList.remove('game-ui-active-page');
          child.classList.add('game-ui-inactive-page');
        }
      }
    }

    document.getElementById('classifyChildContainer')?.parentElement?.scrollTo(0, 0);

    logMessage(levelID, "Changed to page: " + (currentPage + 1));
  }, [currentPage, levelID]);


  useEffect(() => {
    updatePagerColors();
  }, [pageCount, currentPage, results])


  function updatePagerColors() {
    for (let i = 0; i < results.length; i++) {
      const element = results[i];
      const pagerButton = document.getElementById('pager-button-' + i);
      if (pagerButton) {
        pagerButton.classList.add(element);
      }
    }
  }

  function unlockPage(page: number) {
    if ((pageCount < page) && (page <= children.length)) {
      setPageCount(page);
    } else {
      setIsFinished(true);
    }
  }

  




  //============================================================================
  //
  // UI
  //
  //============================================================================

  return (
    <div className='main-wrapper'>
      <header className='main-header'>
        <div className='main-header-wrapper'> 
          <div className='header-logo' onClick={() => { 
            if (isFinished) {
              handleFinishAction();
            }
            window.location.href = '/';
          }}>
            <h1>D P D F</h1>
            <h3>Dark Pattern Defense Force</h3>
          </div>
        </div>
      </header>

      <main className='game-ui-main'>
      <div className='game-ui-main-wrapper'>

        <div className='game-ui-level-container'>
          <h1 className='tutorial-classify-question'>Does this contain a dark pattern?</h1>
          <div className='game-ui-container-size-box' id='classifyChildContainer'>
            {children}
          </div>
        </div>

        <div className='game-ui-sidebar'>
          
          <TimerView finished={isFinished} />

          <div className='game-ui-score'>
            Score: <div className={(currentScore >= 0) ? 'game-ui-score-positive' : 'game-ui-score-negative'}>{currentScore}</div>
          </div>

          <div className='game-ui-dp-categories game-ui-dp-categories-active' id='gameUICategoriesList'>
            <h3>Categories of Dark Patterns</h3>

            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.Obstruction)}>Obstruction<CategoryButtonInfo description={explanations.obstructionExplanation} /></button>
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.Sneaking)}>Sneaking<CategoryButtonInfo description={explanations.sneakingExplanation} /></button>
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.InterfaceInterference)}>Interface Interference<CategoryButtonInfo description={explanations.interfaceInterferenceExplanation} /></button>
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.ForcedAction)}>Forced Action<CategoryButtonInfo description={explanations.forcedActionExplanation} /></button>
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.SocialEngineering)}>Social Engineering<CategoryButtonInfo description={explanations.socialEngineeringExplanation} /></button>
            
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.None)}>None</button>
            <button className='darkPatternCategoryButton' onClick={() => handleDarkPatternCategorization(DarkPatternCategory.UnsureClassify)}>I'm not sure</button>
          </div>
        </div>


        <PageControl pageCount={children.length} unlockCount={pageCount} currentPage={currentPage} action={handlePageChange} />

        {(isFinished) && <button className='finishedButton' onClick={() => handleFinishAction()}>Back To Home</button>}

      </div>
      </main>
    </div>
  )
}





//===============================================================================================================================
//
// PLAYING CANVAS
//
//===============================================================================================================================

function ClassifyGameWrapper({ tutorialElement, index }: { tutorialElement: ClassifyTutorialElement, index: number }) {
  const captionID = 'tutorial-classify-caption-' + index;
  const caption2ID = 'tutorial-classify-caption2-' + index;
  const caption3ID = 'tutorial-classify-caption3-' + index;
  const explanationID = 'tutorial-classify-explanation-' + index;
  const explanationButtonID = 'tutorial-classify-btn-' + index;

  const dpCategories = tutorialElement.category.map(c => getDarkPatternClassName(c)).join(" ")
  const classifyContainerClasses = 'tutorial-classify-element-wrapper dpClassifyContainerSelected-' + index + " " + dpCategories;

  const dpTitle = tutorialElement.category.map(c => getDarkPatternTitle(c)).join(" and ");

  const onExplanationButtonClick = () => {
    document.getElementById(explanationID)?.scrollIntoView();
  }

  return (
    <div className='tutorial-classify-wrapper game-ui-inactive-page'>
      <div className='tutorial-classify-wrapper-inner'>
        <h1 className='tutorial-classify-caption' id={caption2ID}>-</h1>
        <h1 className='tutorial-classify-caption' id={captionID}><span id={caption3ID}>That is <b>incorrect</b>.</span> It contains <b>{dpTitle}</b>.</h1>

        <button className='tutorial-classify-btn' id={explanationButtonID} onClick={onExplanationButtonClick}>⬇ Scroll to explanation ⬇</button>

        <div className={classifyContainerClasses} custom-score={100}>
         {tutorialElement.element}
        </div>
        
        <p className='tutorial-classify-explanation' id={explanationID}><span><b>Explanation:</b><br/></span>{tutorialElement.explanation}</p>
      </div>
    </div>
  )
}