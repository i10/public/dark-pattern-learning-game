import React from 'react';
import { DarkPatternCategory } from '../scoring';
import CookieBanner from '../cookiebanner';

export type ClassifyTutorialElement = {
  category: DarkPatternCategory[];
  element: React.JSX.Element;
  explanation: string;
}

export const tutorialElements: ClassifyTutorialElement[] = [
  {
    category: [DarkPatternCategory.ForcedAction],
    element: (
      <div className='classify-example-ad-blocker'>
        <h3>Ad blocker detected!</h3>
        <p>We have noticed that you have an ad blocker enabled which restricts ads served on this website.</p>
        <p>This website would not be possible without the revenue from advertisement.</p>
        <p>To continue to our website, disable your ad blocker and refresh the website.</p>
        <button>How can I disable my ad blocker?</button><button>↺ Refresh</button>
      </div>
    ),
    explanation: "This website restricts access to its content unless the visitor disables their ad blocker. Therefore, visitors are forced to give up on their privacy if they want to visit the website."
  },

  {
    category: [DarkPatternCategory.ForcedAction],
    element: (
      <div className='classify-example-nagging'>
        <h1>Subscribe to our newsletter?</h1>
        <img src='/tutorial/beach.jpg' />
        <p>Get the newest travel advice before anyone else!</p>
        <input placeholder='E-Mail' />
        <button>Subscribe</button>
        <button>Remind me later</button>
        
      </div>
    ),
    explanation: "There is no option to disable this message. The only options are subscribing or being nagged again."
  },

  {
    category: [DarkPatternCategory.SocialEngineering, DarkPatternCategory.InterfaceInterference],
    element: (
      <div className='classify-example-ad-blocker-2'>
        <img src='/tutorial/mr_snuffles.jpeg' />
        <h3>Please disable your ad blocker!</h3>
        <p>Without ads, we won't be able to feed our office dog "Mr. Snuffles".</p>
        <button>Turn off ad blocker</button>
        <button>Let Mr. Snuffles starve!</button>
        <br className='clearfix' />
      </div>
    ),
    explanation: "This is called confirmshaming. Visitors, who wish to use their ad blocker, have to confirm that they want the cute dog to starve which they probably don't! Furthermore, the inbalance between the two buttons is interface interference."
  },

  {
    category: [DarkPatternCategory.None],
    element: (
      <div className='classify-example-cookie-banner'>
        <CookieBanner seed={0}
          companyName='DPDF' 
          backgroundColor="#ddd" 
          foregroundColor='black' 
          buttonPrimaryColor='#001a76'
          buttonTextColor='#fff'
          buttonAccentColor='#001a76'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333' />
      </div>
    ),
    explanation: "While cookie banners are often used as an example of dark patterns, this one is actually not using one. It conforms to the GDPR requirements."
  },

  // {
  //   category: [DarkPatternCategory.ForcedAction],
  //   element: (
  //     <div>
  //       <img src='/tutorial/forcedregistration.png' />
  //     </div>
  //   ),
  //   explanation: "Visitors have to create an account to view the offers on this website. This is forced registration."
  // },
  
  {
    category: [DarkPatternCategory.Obstruction, DarkPatternCategory.InterfaceInterference],
    element: (
      <div className='classify-example-mobile-screenshot'>
        <img src='/tutorial/instagram_obstruction.png' />
      </div>
    ),
    explanation: "Opting out of receiving emails requires users to deactivate individual categories instead of one global toggle to disable them all. Furthermore, the default values are all against the users best interest making it also interface interference."
  },

  {
    category: [DarkPatternCategory.Sneaking],
    element: (
      <div className='classify-example-mobile-screenshot'>
        <img src='/tutorial/sneaking.jpeg' />
      </div>
    ),
    explanation: "The ads are almost indistinguishable from the content."
  },

  {
    category: [DarkPatternCategory.InterfaceInterference],
    element: (
      <div className='classify-example-cookie-banner'>
        <CookieBanner seed={2}
          companyName='DPDF' 
          backgroundColor="#ddd" 
          foregroundColor='black' 
          buttonPrimaryColor='001a76'
          buttonTextColor='#fff'
          buttonAccentColor='#001a76'
          buttonAccentHighlightColor='#fff'
          buttonSecondaryColor='#333' />
      </div>
    ),
    explanation: "This cookie banner puts an emphasis on the desired action (accepting all cookies). This false hierachy makes it interface interference."
  },

]