'use client'

import { useState, useEffect } from 'react';
import React from 'react';
import '../game-ui.css';
import './tutorial.css';
import '../globals.css';
import { logMessage } from '../logger';
import { storeBool } from '../utils';


const levelID = '19424c90-efea-4ed0-a741-ad833b104d72';


//===============================================================================================================================
//
// TEXTUAL TUTORIAL
//
//===============================================================================================================================

export default function TutorialIntro() {
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    document.getElementById('tutorial-intro-main')?.scroll({
      top: 0,
      left: 0,
      behavior: 'instant'
    });
    logMessage(levelID, "Changed tutorial page to: " + currentPage);
  }, [currentPage]);

  const nextPage = () => {
    setCurrentPage(p => {
      return (p < children.length - 1) ? p + 1 : p
    })
  };

  const backHome = () => {
    window.location.href = '/';
  };

  



  const children = [
    <StoryIntro dismissAction={nextPage} key='storyintro' />,

    <DarkPatternDefinition dismissAction={nextPage} key='dpDefinition' />,

    // <CategoryDefinition
    //   title='Nagging'
    //   definition='Nagging describes repeated requests or intrusions during normal interactions such as pop-ups.'
    //   examples={[
    //     <div key='n_1'>
    //       <h4>Example</h4>
    //       <img className='classify-tutorial-image-medium' src='/tutorial/nagging.png' />
    //     </div>,

    //     <div key='n_2'>
    //       <h4 className='tutorial-intro-example-question'>What is Nagging in this example?</h4>

    //       <p className='tutorial-intro-example-explanation-text'>The alert only offers a "Allow" or "Not now" option with no way to disable it.</p>
    //     </div>
    //   ]}
    //   dismissAction={nextPage}
    //   key='nagging'
    // />,

    <CategoryDefinition
      title='Obstruction'
      definition='Obstruction describes everything that makes a task harder to achieve to discourage users from taking that action.'
      examples={[
        <div key='ob_1'>
          <h4>Example</h4>
          <img className='tutorial-intro-example-fullsize-image' src='/tutorial/obstruction.jpeg' />
        </div>,

        <div key='ob_2'>
          <h4 className='tutorial-intro-example-question'>What is Obstruction in this example?</h4>

          <p className='tutorial-intro-example-explanation-text'>Disabling emails requires users to manually disable multiple categories instead of one global toggle making it unnecessarily hard.</p>

          <h5>Obstruction also includes:</h5>
          <ul>
            <li>Hard or impossible to delete accounts</li>
            <li>Additional steps</li>
            <li>Preventing price comparisons</li>
            <li>Intermediate currency (e.g., buying gold coins with real money to buy in-game items thus masquerading the real price of the items)</li>
          </ul>
        </div>
      ]}
      dismissAction={nextPage}
      key='obstruction'
    />,

    <CategoryDefinition
      title='Sneaking'
      definition='Sneaking describes dark patterns that hide or delay information and often include undisclosed costs.'
      examples={[
        <div key='s_1'>
          <h4>Example</h4>
          <img className='tutorial-intro-example-fullsize-image' src='/tutorial/disguised_ad.jpeg' />
        </div>,

        <div key='s_2'>
          <h4 className='tutorial-intro-example-question'>What is Sneaking in this example?</h4>

          <p className='tutorial-intro-example-explanation-text'>The ads are barely distinguishable from the content.</p>
          
          <img className='classify-tutorial-image-small' src='/tutorial/disguised_ad_detail.jpeg' />
          <p>There is only a small icon indicating that this is an ad which is why users might mistake it for real content.</p>

          <h5>Sneaking also includes:</h5>
          <ul>
            <li>Sneaking items into the basket</li>
            <li>Undisclosed costs or additional costs disclosed very late</li>
            <li>Hidden subscriptions</li>
          </ul>
        </div>
      ]}
      dismissAction={nextPage}
      key='sneaking'
    />,

    <CategoryDefinition
      title='Interface Interference'
      definition='Interface Interference describes manipulations of the user interface to favor certain actions over others.'
      examples={[
        <div key='ii_1'>
          <h4>Example</h4>
          <img className='tutorial-intro-example-fullsize-image' src='/tutorial/interface_interference.png' />
        </div>,

        <div key='ii_2'>
          <h4 className='tutorial-intro-example-question'>What is Interface Interference in this example?</h4>

          <p className='tutorial-intro-example-explanation-text'>The preferred action (subscribing) is way more prominent than the undesired action (continuing without subscribing).</p>

          <h5>Preferred Action</h5>
          <img className='classify-tutorial-image-small' src='/tutorial/interface_interference_detail1.png' />
          <p>The subscribe button is very prominent.</p>

          <h5>Undesired Action</h5>
          <img className='classify-tutorial-image-small' src='/tutorial/interface_interference_detail2.png' />
          <p>The close button is hard to find.</p>

          <h5>Interface Interference also includes:</h5>
          <ul>
            <li>Bad defaults or preselections</li>
            <li>Trick questions</li>
            <li>Complex or confusing language</li>
          </ul>
        </div>
      ]}
      dismissAction={nextPage}
      key='interfaceInterference'
    />,

    <CategoryDefinition
      title='Forced Action'
      definition='Forced Action describe dark patterns where users have to perform some action to access certain functionalities.'
      examples={[
        <div key='fa_1'>
          <h4>Example</h4>
          <img className='tutorial-intro-example-fullsize-image' src='/tutorial/forced_registration.jpeg' />
        </div>,

        <div key='fa_2'>
          <h4 className='tutorial-intro-example-question'>What is Forced Action in this example?</h4>

          <p className='tutorial-intro-example-explanation-text'>Users are forced to create an account to view the content.</p>

          <h5>Forced Action also includes:</h5>
          <ul>
            <li>Nagging</li>
            <li>Disclosing more personal information as intented</li>
            <li>Pay-to-play or grinding</li>
          </ul>
        </div>
      ]}
      dismissAction={nextPage}
      key='forcedAction'
    />,

    <CategoryDefinition
      title='Social Engineering'
      definition='Social Engineering describes all dark patterns that exploit social psychology or behavioral economics.'
      examples={[
        <div key='se_1'>
          <h4>Example</h4>
          <img className='tutorial-intro-example-fullsize-image' src='/tutorial/urgency_scarcity.png' />
        </div>,

        <div key='se_2'>
          <h4 className='tutorial-intro-example-question'>What is Social Engineering in this example?</h4>

          <p className='tutorial-intro-example-explanation-text'>These limited time offers exploits the users fear of missing out.</p>

          <h5>Urgency</h5>
          <img className='classify-tutorial-image-small' src='/tutorial/urgency_detail.png' />
          <p>You better hurry or this offer will be gone!</p>

          <h5>Scarcity</h5>
          <img className='classify-tutorial-image-small' src='/tutorial/scarcity_detail.png' />
          <p>There are also limited quantities remaining. Hurry even more!</p>

          <h5>Social Engineering also includes:</h5>
          <ul>
            <li>Low stock messages</li>
            <li>High demand messages</li>
            <li>Fake reviews</li>
            <li>Fake expert testimonials</li>
            <li>Confirmshaming (i.e., emotional blackmail shaming the user for their choices)</li>
          </ul>
        </div>
      ]}
      dismissAction={nextPage}
      key='socialEngineering'
    />,

    <GameExplanation key='gameexplanation' />
  ]


  const pageNavigationButtons = children.map((child, index) => {
    const classList = (index == currentPage) ? 'active' : '';
    if (child.type == CategoryDefinition) {
      return <button key={index} className={classList} onClick={() => setCurrentPage(index)}>{child.props.title}</button>
    } else {
      if (index == 0) {
        return <button key={index} className={classList} onClick={() => setCurrentPage(index)}>Story Intro</button>
      } else if (index == 1) {
        return <button key={index} className={classList} onClick={() => setCurrentPage(index)}>Dark Pattern Definition</button>
      } else {
        return <button key={index} className={classList} onClick={() => setCurrentPage(index)}>Game Explanation</button>
      }
    }
  })

  return (
    <div className='main-wrapper'>
      <header className='main-header'>
        <div className='main-header-wrapper'> 
          <div className='header-logo' onClick={() => { storeBool(levelID + "-completed", true);; window.location.href = '/'; }}>
            <h1>D P D F</h1>
            <h3>Dark Pattern Defense Force</h3>
          </div>
        </div>
      </header>

      <main className='game-ui-main tutorial-intro-main' id='tutorial-intro-main'>
        {children[currentPage]}

        <div className='tutorial-pages'>
          {pageNavigationButtons}
        </div>
      </main>
    </div>
  )
}




//===============================================================================================================================
//
// STORY INTRO
//
//===============================================================================================================================

function StoryIntro({ dismissAction }: { dismissAction: () => void}) {
  const [currentStage, setCurrentStage] = useState(0);

  const [choice1, setChoice1] = useState(0);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    timer = setTimeout(() => setCurrentStage(s => s + 1), 500);

    const currentMessage = document.getElementById("msg-" + currentStage)
    if (currentMessage) {
      currentMessage.classList.remove("chat-msg-hidden");
      currentMessage.classList.add("chat-msg-visible");

      currentMessage.scrollIntoView();

      if (currentMessage.classList.contains('chat-p2')) {
        clearTimeout(timer);
      }
    }
    
    return () => {
      if (timer) { clearTimeout(timer); }
    };
  }, [currentStage]);


  const msgIntroChoice = (n: number) => {
    const otherButton = document.getElementById('btn-msg-intro-' + (n%2))
    if (otherButton) { otherButton.classList.add('chat-msg-hidden') }

    setChoice1(n);
    setCurrentStage(s => s + 1)
  };

  
  function TypingDots() {
    return (
      <div className='chat-p1 typing-dots'><span></span><span></span><span></span></div>
    );
  }

  function Message({ typingStart, messageStart, currentStage, message }: { typingStart: number, messageStart: number, currentStage: number, message: React.ReactNode }) {
    if (currentStage < typingStart) {
      return <></>
    } else if ((currentStage >= typingStart) && (currentStage < messageStart)) {
      return <TypingDots />
    } else {
      return message
    }
  }


  const button1Choice1Class = (choice1 == 2) ? 'p2-button chat-msg-hidden' : 'p2-button'
  const button2Choice1Class = (choice1 == 1) ? 'p2-button chat-msg-hidden' : 'p2-button'


  return (
    <div className='story-chat-container'>

      <Message typingStart={0} messageStart={2} currentStage={currentStage} message={
        <div className='chat-p1' id='msg-2'>Ah, hallo there! You must be the newest recruit.</div>
      } />

      <Message typingStart={3} messageStart={5} currentStage={currentStage} message={
        <div className='chat-p1' id='msg-5'>Welcome to the <strong>DPDF</strong>! The <i>"Dark Pattern Defense Force"</i>.</div>
      } />

      <Message typingStart={6} messageStart={10} currentStage={currentStage} message={
        <div className='chat-p1' id='msg-10'>We are the last line of defense to protect the internet from the evil that is <strong>dark patterns</strong>.<br />I'm sure you know what dark patterns are?</div>
      } />


      <Message typingStart={12} messageStart={12} currentStage={currentStage} message={
        <div className='chat-p2' id='msg-12'>
          <button id='btn-msg-intro-0' className={button1Choice1Class} onClick={() => msgIntroChoice(1)}>Of course I do!</button>
          <button id='btn-msg-intro-1' className={button2Choice1Class} onClick={() => msgIntroChoice(2)}>Could you refresh my memory?</button>
        </div>
      } />


      <Message typingStart={13} messageStart={17} currentStage={currentStage} message={
        <div className='chat-p1' id='msg-17'>{(choice1 == 2) ? "Certainly!" : "Haha, nice try!"}<br />We have actually prepared a quick introduction to get you started. Once you have completed it, you will know everything you need to know to start fighting back against those evil dark patterns!</div>
      } />

      <Message typingStart={18} messageStart={19} currentStage={currentStage} message={
        <div className='chat-p1' id='msg-19'>Good Luck!</div>
      } />

      <Message typingStart={20} messageStart={20} currentStage={currentStage} message={
        <div className='chat-p2' id='msg-20'>
          <button className='p2-button' id='btn-msg-continue' onClick={dismissAction}>Start the introduction!</button>
        </div>  
      } />
    </div>
  )
}




//===============================================================================================================================
//
// DARK PATTERN DEFINITIONS
//
//===============================================================================================================================

function DarkPatternDefinition({ dismissAction }: { dismissAction: () => void}) {
  return (
    <div className='tutorial-intro-definitions'>
      <h1>What are Dark Patterns?</h1>
      <h2>Dark patterns are tricks used in websites and apps that make you do things that you didn't mean to, like buying or signing up for something.</h2>
      <div className='anim-fast anim-3'><a href="https://www.deceptive.design" target="_blank" rel="noopener noreferrer">- Definition by Harry Brignull <small style={{opacity: '0.6'}}>[Learn more; external content]</small></a></div>

      <h2 className='anim-5'>There are many different types of dark patterns that all try to exploit the user in one way or another.</h2>
      <h2 className='anim-8'>We will use broader categories that the different dark patterns fall into and present you with some examples for each of them!</h2>

      <button className='tutorial-intro-button-next anim-fast anim-10' onClick={dismissAction}>Continue</button>
    </div>
  )
}




//===============================================================================================================================
//
// TAXONOMY CATEGORY DEFINITIONS
//
//===============================================================================================================================

function CategoryDefinition({ title, definition, examples, dismissAction }: { title: string, definition: string, examples: React.ReactNode[], dismissAction: () => void }) {
  return (
    <div className='tutorial-intro-definitions tutorial-intro-definitions-fullwidth'>
      <h1>{title}</h1>
      <h2>{definition}</h2>

      <div className='tutorial-intro-example-wrapper anim-fast anim-2'>
        <div className='tutorial-intro-example-wrapper-inner'>
        {examples}
        </div>
      </div>

      <br className='clearfix' />

      <button className='tutorial-intro-button-next anim-fast anim-3' onClick={dismissAction}>Continue</button>
    </div>
  );
}




//===============================================================================================================================
//
// GAMEPLAY EXPLANATION
//
//===============================================================================================================================

export function GameExplanation() {
  return (
    <div className='tutorial-intro-definitions tutorial-intro-game-explanation'>
      <h1>How does the game work?</h1>

      <div className='tutorial-intro-game-explanation-wrapper'>
        <h3>1. The UI</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_0.jpg' />
        <p>You will see a website in the center of the screen that may contain dark patterns and that you can interact with. Note that not all elements are interactive.</p>
        <p>On the right, the sidebar contains your time, score, the Dark Pattern Magic Wand&trade; and the categories of dark patterns.</p>
      </div>

      <div className='tutorial-intro-game-explanation-wrapper anim-1'>
        <h3>2. Activate the Dark Pattern Magic Wand&trade; in the sidebar</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_1.jpg' />
        <p>The Dark Pattern Magic Wand&trade; allows you to highlight elements in the website on the left that are potential dark patterns. They will be highlighted when you hover over them with the Dark Pattern Magic Wand&trade; enabled.</p>
        <p>You can also activate or deactivate your Dark Pattern Magic Wand&trade; by pressing "M".</p>
      </div>
   
      <div className='tutorial-intro-game-explanation-wrapper anim-2'>
        <h3>3. Select potential dark pattern elements on the website</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_2.jpg' />
        <p>Click on a potential dark pattern element to select it. The categories will appear in the sidebar. Try to be as precise as possible with your selection (i.e., select the smallest possible container).</p>
        <p>You can clear your selection by deactivating the Dark Pattern Magic Wand&trade; or by selecting another element instead.</p>
      </div>

      <div className='tutorial-intro-game-explanation-wrapper anim-3'>
        <h3>4. Select the correct category of dark pattern in the sidebar</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_3.jpg' />
        <p>You will get instant feedback if your choice was correct and why. You can also choose the "I'm not sure" option which will give you less points than the correct answer but more points than a wrong answer.</p>
        <p>If multiple dark pattern categories are correct, one of them is enough.</p>
      </div>

      <div className='tutorial-intro-game-explanation-wrapper anim-4'>
      <h3>5. Unlock additional pages</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_4.jpg' />
        <p>Sometimes dark patterns are distributed across multiple pages. You can unlock additional pages by clicking on any element displaying 🔓 as cursor. Once unlocked, you can switch between pages at the bottom of the screen.</p>
        <p>You can also switch to a page by pressing the corresponding number key.</p>
      </div>

      <div className='tutorial-intro-game-explanation-wrapper anim-5'>
        <h3>6. Complete the level</h3>
        <img className='tutorial-intro-game-explanation-img' src='/tutorial/gameui_5.jpg' />
        <p>When you think that you found all dark patterns, deactivate your Dark Pattern Magic Wand&trade; and click the "Finish" button in the bottom right corner.</p>
        <p>You will get a summary of any dark patterns that you may have missed.</p>
      </div>
      
      <button className='tutorial-intro-button-next anim-5' onClick={() => { storeBool(levelID + "-completed", true); window.location.href = '/'; }}>Back to Home</button>
    </div>
  )
}