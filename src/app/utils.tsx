export function randomNumberInRange(min = 0, max = 10) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


export function shuffleArray(array: any[]) {
  return array.map(value => ({ value, sort: Math.random() }))
              .sort((a, b) => a.sort - b.sort)
              .map(({ value }) => value)
}


export function addLineBreaks(str: string) {
  return str.split('\n').map((s, i) => <p key={i}>{s}</p>);
}

// use async or with .sleep(x).then(() => {})
export function sleep(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}


  //============================================================================
  //
  // SESSION STORAGE
  //
  //============================================================================

export function storeBool(key: string, b: boolean) {
  if (typeof window == 'undefined') { return }
  window.sessionStorage.setItem(key, JSON.stringify(b));
}

export function loadBool(key: string, fallback: boolean = false): boolean {
  if (typeof window == 'undefined') { return fallback }
  const val = window.sessionStorage.getItem(key);
  if (!val) { return fallback }
  return JSON.parse(val);
}


export function storeNumber(key: string, n: number) {
  if (typeof window == 'undefined') { return }
  window.sessionStorage.setItem(key, JSON.stringify(n));
}

export function loadNumber(key: string, fallback: number = 0): number {
  if (typeof window == 'undefined') { return fallback }
  const val = window.sessionStorage.getItem(key);
  if (!val) { return fallback }
  return JSON.parse(val);
}




//============================================================================
//
// HIGHSCORE
//
//============================================================================


export function applyLevelResult(levelID: string, highscore: number) {
  setCareerValue(levelID, highscore);
  setHighscore(levelID, highscore);
}

export function getHighscore(levelID: string): number {
  return loadNumber(levelID, 0);
}


export function setHighscore(levelID: string, highscore: number) {
  const existingHighscore = loadNumber(levelID, -100000);
  if (highscore > existingHighscore) {
    storeNumber(levelID, highscore);
  }
}



export function getCareerValue() {
  return loadNumber('career_points', 0);
}

export function setCareerValue(levelID: string, highscore: number) {
  const existingHighscore = loadNumber(levelID, -100000);
  console.log(existingHighscore)
  if (existingHighscore == -100000) { // only update career value on first attempt
    const careerValue = getCareerValue();
    console.log(careerValue)
    storeNumber('career_points', careerValue + highscore);
  }
}




//============================================================================
//
// HELPER TYPES
//
//============================================================================

export type BonusPoints = {
  title: string;
  value: number;
}


export type ButtonAction = {
  title: string;
  action: (params: any | null) => void;
}

export type Warning = {
  title: string;
  prompt: string;
  buttons: ButtonAction[];
  dismissAction: () => void;
}