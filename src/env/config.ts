import { z } from "zod";


export const PublicEnvConfig = z.object({
    NEXT_PUBLIC_MARIADB_SERVER: z.string(),
    // NEXT_PUBLIC_MARIADB_PORT: z.string(),
    // NEXT_PUBLIC_MARIADB_DATABASE: z.string(),
    // NEXT_PUBLIC_MARIADB_USER: z.string(),
});

export const EnvConfig = z.object({
    NODE_ENV: z.enum(["development", "production", "test"]),
    PORT: z.string().optional(),
    ADMINER_PORT: z.string().optional(),
    DATABASE_URL: z.string().optional(),
    SHADOW_DATABASE_URL: z.string().optional(),
}).merge(PublicEnvConfig);

export type PublicEnv = z.infer<typeof PublicEnvConfig>

declare global {
    namespace NodeJS {
        interface ProcessEnv extends z.infer<typeof EnvConfig> {}
    }
}