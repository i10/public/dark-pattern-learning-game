"use server";
import { PublicEnvConfig } from "./config";

export const getEnv = async () => PublicEnvConfig.strip().parse(process.env);
