"use client";
import { createContext, useContext, useEffect, useState } from "react";
import { getEnv } from "./env";
import { PublicEnv } from "./config";

export const EnvContext = createContext<Partial<PublicEnv>>({});

export function EnvProvider({ children }: Readonly<{ children: React.ReactNode }>) {
    const [env,setEnv] = useState<Partial<PublicEnv>>({});

    useEffect(() => {
        getEnv().then((env) =>{
            // console.debug('env',env);
            setEnv(env);
        });
    }, []);


    return <EnvContext.Provider value={env}>{children}</EnvContext.Provider>;
}

export const useEnv = () => {
    return useContext(EnvContext);
}