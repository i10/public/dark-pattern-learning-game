#!/bin/bash

# Note: this script is for testing purpose only

# Create enviroment
mkdir -p .db
cd .db > /dev/null
echo 'dev' > password.txt
cd ../

echo 'NEXT_PUBLIC_MARIADB_SERVER=db' > app.prod.env
echo 'DATABASE_URL=mysql://dplg:dev@dplg_db:3306/dplg' >> app.prod.env


# install
npm install
#npm install next@latest react@latest react-dom@latest


# docker build
docker compose -f "compose.prod.yml" up -d --build


# init / update database
npx prisma migrate deploy